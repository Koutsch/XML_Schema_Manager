#include "xml_schema_manager.h"

XML_Obj *create_template(char *schema_path, char *encoding)
{
	pthread_t					thread;
	Schema_Obj				*schema;
	XML_Obj						*xml;
	Communicator			*com;
	Node_Description	*desc;
	
	// get schema
  if (!(schema = get_schema(BAD_CAST schema_path, encoding)))
  {
  	ERROR("Could not create schema from file '%s'.", schema_path);
  	return NULL;
  }
  
  printf("Schema loaded from '%s'.\n", schema_path);
  
  // create the empty all possible XML
  if (!(xml = create_xml(schema, "1.0")))
  {
  	ERROR("Could not create XML object.", true);
  	return NULL;
  }
  
  printf("XML object created.\n");
  
  // create communicator for interthread communication
  if (!(com = create_communicator(schema, xml)))
  {
  	ERROR("Could not create communicator for interthread communication.", true);
  	return NULL;
  }
  
  printf("Communicator created. Ready to create the 'all possible' XML.\n");
  
  // start the worker thread
  start_creation(com, TEMPLATE);

  printf("Worker thread created.\n");

	// wait for requests from the worker thread
	while (!(has_request(com)))
	{
	
	}
	
	if (com->request->type == COMPLETE_CONF)
	{
		printf("Creation completed.\n");
	}
	else
	{
		ERROR("Creation not completed.", true);
	}
	
	return xml;
}

int main(int argc, char *argv[])
{   
  if (argc != 2 && argc != 4 && argc != 6)
  {
  	printf("Wrong number of arguments.\nYou must supply a path to an XML schema file.\nOptions:\n-o: specify output file path, otherwise to stdout\n-e: file encoding, default is UTF-8\n");
  	return 1;
  }

  char *schema_path = NULL;
  char *xml_path = NULL;  
  char *encoding = NULL;
  
  int i;
  for (i = 1; i < argc; i++)
  {
  	if (argv[i][0] == '-')
  	{
  		switch (argv[i][1])
  		{
  			case 'o':
  				xml_path = argv[++i];
  				break;
  			case 'e':
  				encoding = argv[++i];
  				break;
  		}
  	}
  	else
  	{
  		schema_path = argv[i];
  	}
  }
  
  if (!(encoding)) encoding = "UTF-8";
  
  if (!(schema_path))
  {
  	ERROR("No XML schema file path given.", true);
  	return 2;
  }
  
  XML_Obj *xml = create_template(schema_path, encoding);
  
  if (xml_path)
  {
  	save_to_file(xml, xml_path, encoding, 1);
  }
  else
  {
  	xmlChar *doc_txt_ptr;
  	int doc_txt_len;
  	
  	xmlDocDumpFormatMemoryEnc(xml->doc, &doc_txt_ptr, &doc_txt_len, encoding, 1);
  	
  	printf("%s", doc_txt_ptr);
  	
  	xmlFree(doc_txt_ptr);
  }
	
  return 0;
}

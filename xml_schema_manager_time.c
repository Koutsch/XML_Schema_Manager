#include "xml_schema_manager_time.h"

#define PART_NR 9

static long long int get_time_part(Time time, int i)
{
	switch (i) 
	{
		case 0: return time.year;
		case 1: return time.month;
		case 2: return time.day;
		case 3: return time.hour;
		case 4: return time.minute;
		case 5: return time.second;
		case 6: return time.nth_second;
		case 7: return time.tz_hour;
		case 8: return time.tz_minute;
	}
}

static bool is_timezone(char *timezone_str)
{	
	// return false if Z = UTC time representation
	if (*timezone_str == 'Z' && *(timezone_str + 1) == '\0') return true;
	
	// return false if not + or -
	if (*timezone_str != '+' && *timezone_str != '-') return false;
	
	// go to time definition
	timezone_str++;
	
	int		status,
				hour,
				hour_10,
				hour_1,
				minute,
				minute_10,
				minute_1,
				consumed;
	
	// scan timezone string
	status = sscanf(timezone_str, "%1u%1u:%1u%1u%n", &hour_10, &hour_1, &minute_10, &minute_1, &consumed);
	
	// return false if wrong number of numbers read
	if (status != 4) return false;
	
	// check hour
	hour = (hour_10 * 10) + hour_1;
	
	// return false if not between 0 and 14
	if (hour < 0 || hour > 14) return false;
	
	// check minute
	minute = (minute_10 * 10) + minute_1;
	
	// return false if not between 0 and 59
	if (minute < 0 || minute > 59) return false;
	
	// check if end is reached
	if (*(timezone_str + consumed) != '\0') return false;
	
	return true;
}

bool is_date(char *date_str)
{
	int 					status,
								month,
								month_10,
								month_1,
								day,
								day_10,
								day_1,
								consumed;
	
	long long int	year;
	
	// scan date string
	status = sscanf(date_str, "%lld-%1u%1u-%1u%1u%n", &year, &month_10, &month_1, &day_10, &day_1, &consumed);
	
	// return false if wrong number of numbers read
	if (status != 5) return false;
	
	// check month
	month = (month_10 * 10) + month_1;

	// return false if wrong month number
	if (month < 1 || month > 12) return false;
	
	// check day
	day = (day_10 * 10) + day_1;
	
	// check day of month
	switch (month)
	{
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			if (day < 1 || day > 31) return false;
			break;
		case 4:
		case 6:
		case 9:
		case 11:
			if (day < 1 || day > 30) return false;
			break;
		case 2: // february
			if (day < 1 || day > 29) return false;
			
			if (day == 29 && !(
						((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0) // leap year check
       	 )) return false;
      break;
    default:
    	return false;
	}
	
	// go to last char
	date_str += consumed;
	
	// check if end is reached
	if (*date_str == '\0') return true;
	
	// check time zone
	return is_timezone(date_str);
}

bool is_time(char *time_str)
{
	int						status,
								hour,
								hour_10,
								hour_1,
								minute,
								minute_10,
								minute_1,
								second,
								second_10,
								second_1,
								consumed;
	
	long long int	second_nth;
	
	// scan time string until full seconds
	status = sscanf(time_str, "%1u%1u:%1u%1u:%1u%1u%n", &hour_10, &hour_1, &minute_10, &minute_1, &second_10, &second_1, &consumed);
	
	// return false if wrong number of numbers read
	if (status != 6) return false;
	
	// check hour
	hour = (hour_10 * 10) + hour_1;
	
	// return false if not between 0 and 14
	if (hour < 0 || hour > 24) return false;
	
	// check minute
	minute = (minute_10 * 10) + minute_1;
	
	// return false if not between 0 and 59
	if ((hour == 24 && minute != 0) || minute < 0 || minute > 59) return false;
	
	// check second
	second = (second_10 * 10) + second_1;
	
	// return false if not between 0 and 59
	if ((second == 24 && second != 0) || second < 0 || second > 59) return false;
	
	// go to last char
	time_str += consumed;
	
	// return true if end is reached
	if (*time_str == '\0') return true;
	
	// check nth seconds
	if (*time_str == '.')
	{
		// go to nth seconds
		time_str++;
		
		// scan nth seconds
		status = sscanf(time_str, "%lld%n", &second_nth, &consumed);
		
		// return false if wrong number of numbers read
		if (status != 1) return false;
		
		// go to last char
		time_str += consumed;		
	}
	
	// check if end is reached
	if (*time_str == '\0') return true;
	
	// check time zone
	return is_timezone(time_str);	
}

bool is_datetime(char *datetime_str)
{
	char 	*t;
	
	int		date_len;
	
	// get position of T
	t = strchr(datetime_str, 'T');
	
	// return false if T not found
	if (t == NULL) return false;
	
	// extract date
	date_len = t - datetime_str;
	char date_str[date_len + 1];
	strncpy(date_str, datetime_str, date_len);
	date_str[date_len] = '\0';
	
	// check date part
	if (!(is_date(date_str))) return false;
	
	// go to time part
	t++;
	
	return is_time(t);
}

static int get_nth_second(Time *t, char *nth_second_str)
{
	int		consumed = 0;
	
	// scan the nth second
	if (*nth_second_str == '.')
	{
		sscanf(nth_second_str, ".%lld%n", &(t->nth_second), &consumed);
	}
	
	return consumed;
}

static void get_timezone(Time *t, char *timezone_str)
{
	int		hour,
				minute;
	
	// scan timezone str
	if (2 != sscanf(timezone_str, "%d:%d", &hour, &minute))
	{
		t->tz_hour = 0;
		t->tz_minute = 0;
		return;
	}
	
	// assign hour
	t->tz_hour = hour;
	t->tz_minute = minute;
	
	if (*timezone_str == '-') t->tz_minute = -(t->tz_minute);
}

Time get_date(char *date_str)
{
	Time	t;
	
	int		consumed;
	
	// scan the date string
	sscanf(date_str, "%lld-%d-%d%n", &(t.year), &(t.month), &(t.day), &consumed);
	
	// set other values to zero
	t.hour = 0;
	t.minute = 0;
	t.second = 0;
	t.nth_second = 0;
	
	// check time zone
	get_timezone(&t, date_str + consumed);
	
	return t;
}

Time get_time(char *time_str)
{
	Time	t;
	
	int		consumed;
	
	// scan the time string
	sscanf(time_str, "%d:%d:%d%n", &(t.hour), &(t.minute), &(t.second), &consumed);
	
	// set other values to zero
	t.year = 0;
	t.month = 0;
	t.day = 0;
	
	// go to last char
	time_str += consumed;
	
	// scan the nth second
	consumed = get_nth_second(&t, time_str);
	
	// check time zone
	get_timezone(&t, time_str + consumed);
	
	return t;	
}

Time get_datetime(char *datetime_str)
{
	Time	t;
	
	int		consumed;
	
	// scan the datetime string
	sscanf(datetime_str, "%lld-%d-%dT%d:%d:%d%n", &(t.year), &(t.month), &(t.day), &(t.hour), &(t.minute), &(t.second), &consumed);
	
	// go to last char
	datetime_str += consumed;
	
	// scan the nth second
	consumed = get_nth_second(&t, datetime_str);
	
	// check time zone
	get_timezone(&t, datetime_str + consumed);
	
	return t;
}

int compare_time(Time time1, Time time2)
{
	int						i;
	
	long long int	part1,
								part2;
	
	for (i = 0; i < PART_NR; i++)
	{
		part1 = get_time_part(time1, i);
		part2 = get_time_part(time2, i);
		
		if (part1 < part2) return -1;
		else if (part1 > part2) return 1;
	}
	
	return 0;
}

/** 
 * @file xml_schema_manager.h 
 *
 * @brief The XML Schema Manager contains utilities to create and edit XML files according to an XML Schema file.
 *
 * @author Jakob Wiedner
 */

#ifndef XML_SCHEMA_MAN
#define XML_SCHEMA_MAN

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <ctype.h>
#include <time.h>

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xpath.h>
#include <libxml/xpathInternals.h>
#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>
#include <libxml/xmlschemastypes.h>
#include <libxml/xmlmemory.h>

#include "list.h"
#include "occ_finder.h"
#include "xml_schema_manager_time.h"
#include "validator.h"

#define EQUALS(str1, str2) (strcmp(str1, str2) == 0)

#define NEQUALS(str1, str2) (strcmp(str1, str2) != 0)

#ifdef D
	#define DEBUG(fmt, ...) fprintf(stderr, "DEBUG %s:%d:%s(): " fmt "\n", __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); 
#else
	#define DEBUG(fmt, ...)
#endif

#ifdef E
	#define ERROR(fmt, ...) fprintf(stderr, "ERROR %s:%d:%s(): " fmt "\n", __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); 
#else
	#define ERROR(fmt, ...)
#endif

#ifdef W
	#define WARNING(fmt, ...) fprintf(stderr, "WARNING %s:%d:%s(): " fmt "\n", __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); 
#else
	#define WARNING(fmt, ...)
#endif

/**
 * A struct containing pointers to an XML schema object and its Xpath context.
 */
typedef struct _Schema_Obj {
	xmlDocPtr doc; /**< a pointer to the schema as XML document */
	xmlXPathContextPtr xpath_ctxt; /**< a pointer to the Xpath context */
	xmlChar *schema_path; /**< the path to the schema file */
} Schema_Obj;

/**
 * A struct containing pointers to an XML object and its Xpath context.
 */
typedef struct _XML_Obj {
	xmlDocPtr doc; /**< a pointer to the XML object */
	xmlXPathContextPtr xpath_ctxt; /**< a pointer to the Xpath context */
} XML_Obj;

/**
 * A collection of different request types.
 */
typedef enum
{
	NO_REQUEST = 0,
	CONFIRM_REQ, /**< request that user accepts or refuses something */
	STRING_REQ, /**< character data is requested */
	CHOOSE_REQ, /**< request that chooses from a list of possibilities */
	SEARCH_REQ, /**< request the user to transmit a Xpath expression and deliver results if exist */
	ERROR_CONF, /**< confirm that an error has been detected while opening or validating a schema or XML file */
	CREATE_CONF, /**< confirm the creation of an element */
	EDIT_CONF, /**< confirm the editing of the XML document */
	COMPLETE_CONF /**< confirm that the XML creation is completed */	
} Request_Type;

/**
 * A collection of reasons why user input has been rejected and must be repeated
 */
typedef enum
{
	NO_REFUSE = 0,
	NOT_UNIQUE,
	NO_MATCH,
	INCORR,
	OUT_OF_BOUNDS
} Refuse_Type;

/**
 * A collection of different creation actions informing the user about the creation/editing/deletion of a node
 */
typedef enum
{
	NONE = 0,	
	CREATE_NODE, /**< node is created */
	REPLACE_NODE, /**< node is replaced */
	DEFAULT, /**< use of default value must be decided */
	STRING, /**< string is requested */
	PATTERN, /**< string complying a certain regex pattern is required */
	FILE_PATH, /**< file path is requested */
	DECIMAL, /**< decimal number is requested */
	INTEGER, /**< integer is requested */
	DATE, /**< date is requested */
	TIME, /**< time is requested */
	DATETIME, /**< dateTime is requested */
	CHOOSE_NODE, /**< node is to be chosen */
	CHOOSE_ENUMERATION, /**< value from enumeration is to be chosen */
	
	// FOR SEARCH FUNCTIONS
	
	SEARCH_RESULT, /**< search is offered and results are available */
	SEARCH_EMPTY, /**< search is offered, no results */
	
	// CONFIRMATIONS
	
	ATTRIBUTE, /**< attribute is created */
	ELEMENT, /**< element is created */
	SEQUENCE, /**< sequence is created */
	CHOICE, /**< choice is created */
	GROUP, /**< group is created */
	EXTENSION, /**< extension is created */
	ENUMERATION, /**< enumeration is created */
	COMPLEX_CONTENT, /**< complex content is created */
	SIMPLE_CONTENT, /**< simple content is created */
	COMPLEX_TYPE, /**< complex type is created */
	SIMPLE_TYPE, /**< simple type is created */
	CONTENT, /**< content is created */
	PATTERN_CONTENT, /**< content matching regex pattern is created */
	BOUNDS_CONTENT, /**< content value within bounds is created */
	
	// DELETE FUNCTIONS
	
	DELETE_SUCCESS, /**< deletion was successful */
	DELETE_ERROR, /**< error while deleting */
	DELETE_FORBIDDEN, /**< deletion not allowed */
	
	// EDIT FUNCTIONS
	
	EDIT_INTERRUPT, /**< editing was interrupt */
	EDIT_SUCCESS, /**< editing was successful */
	EDIT_ERROR, /**< error while editing */
	EDIT_FORBIDDEN, /**< editing not allowed */
	
	// COMPLETION FUNCTIONS
	
	CREATE_SUCCESS, /**< creation was successful */
	CREATE_FINISHED, /**< creation was terminated */
} Action_Type;

/**
 * A struct holding request information.
 */
typedef struct _Request
{
	Request_Type type; /**< the type of request (see #Request_Type) */
	Action_Type action; /**< an integer indicating which action has been performed (see #Action_Type) */
	Refuse_Type refuse; /**< an integer indicating why a value has not been accepted (see #Refuse_Type) */
	void *data; /**< pointer to additional data: node name, choices or results */
} Request;

/**
 * A struct holding the description of a node to be created.
 */
typedef struct _Node_Description
{
	xmlChar *name; /**< the name of the node */
	xmlChar *type; /**< the type of the node as string */
	xmlChar	*content; /**< possible text content of a node */
} Node_Description;

/**
 * A struct holding choice information for client.
 */
typedef struct _Choice_Collection
{
	xmlChar *name; /**< the name of the parent node */
	xmlChar **choices; /**< a pointer to pointers to strings holding a choice, to be used as an array for choices */
	size_t count; /**< the number of possible choices */
} Choice_Collection;

/**
 * A struct holding xpath search results */
typedef struct _Result
{
	xmlNodePtr *nodes; /**< a pointer to an array of the XML nodes found */
	size_t count; /**< the number of XML nodes found */
} Result;

/**
 * A collection of possible answers from the calling thread.
 */
typedef enum
{
	NO_ANSWER = 0,
	DATA_ANSWER, /**< answer contains data for creation */
	SEARCH_ANSWER, /**< answer contains xpath expression */
	UNDO_ANSWER, /**< answer contains an undo request */
	QUIT_ANSWER, /**< complete and quit creation */
	DELETE_ANSWER, /**< answer contains pointer to node that should be deleted */
	EDIT_ANSWER /**< answer contains pointer to text node and new text */
} Answer_Type;

/**
 * A collection of possible acceptance answers from the calling thread.
 */
typedef enum
{
	ACCEPT = 0, /**< calling thread accepts */
	ACCEPT_ONCE, /**< calling thread accepts once (for testing purposes) */
	REFUSE /**< calling thread refuses */ 
} Accept_Type;

/**
 * A struct holding the answer from the calling thread.
 */
typedef struct _Answer
{
	Answer_Type type; /**< the type of the answer (see #Answer_Type) */
	void *data; /**< the answer data */
	int chosen_index; /**< answer chosen index */
	Accept_Type acceptance; /**< boolean indicating if the user accepts or not */ 
} Answer;

typedef enum
{
	COMMUNICATE = 0, /**< communicate with calling thread to create or edit XML */
	TEMPLATE /**< do not communicate with calling thread and create a default, annotated template XML */
} Creation_Type;

/**
 * A struct containing a request and an answer for interthread communication.
 */
typedef struct _Communicator
{
	Request *request; /**< a pointer to the request */
	Answer *answer; /**< a pointer to the answer */
	bool interrupt; /**< whether or not to interrupt the creation process */
	int cur_depth; /**< the depth of the node currently processed */
	Schema_Obj *schema; /**< a pointer to the schema object used for creation or editing */
	XML_Obj *xml; /**< a pointer to the XML to be created or edited */
	Creation_Type type; /**< whether to communicate or not with calling thread, see also #Creation_Type (only for XML creation) */
} Communicator;

/**
 * A struct containing pointers to an XML unique element from a schema document, the corresponding XML object and the XML node the unique rule applies to.
 */
typedef struct _Unique_Obj
{
	Schema_Obj *schema; /**< a pointer to the schema object */
	xmlNodePtr schema_unique; /**< a pointer to the schema unique element */
	XML_Obj *xml; /**< a pointer to the XML object */
	xmlNodePtr xml_node; /**< a pointer to the XML element */
} Unique_Obj;

/**
 * A struct containing pointers to the first node in the undo list and the current valid XML node.
 */
typedef struct _Undo_Collection
{
	List_Node *undo_head; /**< a pointer to the first node in the undo list */
	List_Node *unlinked_head; /**< a pointer to a list of unlinked xml nodes */
} Undo_Collection;

/**
 * A pointer to a create function.
 */
typedef void (*Create_function)(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_node, xmlNodePtr xml_node);

/**
 * A pointer to a check function.
 */
typedef bool (*Check_function)(char *str);

/**
 * A struct containing pointers to the function, schema object, XML object, schema node and XML node needed to undo a creation of XML content.
 */
typedef struct _Undo_Obj
{
	List_Node **unique_head; /**< a pointer to the pointer of the unique list head */
	Create_function create_function; /**< a pointer to the create function used to create the node to undo */
	xmlNodePtr schema_node; /**< a pointer to the schema node do undo */
	xmlNodePtr parent_node; /**< a pointer to the parent XML node */
	xmlNodePtr new_node; /**< a pointer to the newly created XML node to be deleted, no deletion if NULL */
} Undo_Obj;

/**
 * An enumeration of bounds types.
 */
typedef enum {
	INCLUSIVE = 0,
	EXCLUSIVE,
	MINIMUM,
	MAXIMUM,
	EMPTY
} Bounds_Type;

/**
 * An struct representing a bounds facet.
 */
typedef	struct _Bounds {
	Bounds_Type limit;
	Bounds_Type type;
	xmlChar *value;
} Bounds;

/* FUNCTIONS */

/**
 * Create a new undo collection holding undo objects and pointers to unlinked (undone) nodes.
 * @return a newly created undo collection or NULL in case of error
 */
static Undo_Collection *create_undo_collection(void);

/**
 * Undo the last action stored in the undo list, unlink the deleted nodes and repeat the creation of the node in which undo was called.
 * @param undo_coll the undo collection holding the undo list and the unlinked list
 * @param unique_head a pointer to a pointer to the unique list
 * @param com the communicator object used for interthread communication
 * @param cur_create_function the current function in which undo was called
 * @param cur_schema_node the currently processed schema node
 * @param cur_xml_node the currently processed XML node
 */
static void process_undo(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, Create_function cur_create_function, xmlNodePtr cur_schema_node, xmlNodePtr cur_xml_node);

/**
 * Add a new undo node to the undo list.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the communicator object used for interthread communication
 * @param create_function a pointer to the function used to create the XML node
 * @param schema_node the current schema node to undo
 * @param parent_node the parent XML node of the node to be deleted
 * @param new_node the XML node to delete
 */
static void add_undo(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, Create_function create_function, xmlNodePtr schema_node, xmlNodePtr parent_node, xmlNodePtr new_node);

/**
 * Free an undo collection object.
 * @param the undo collection to be freed
 */
static void free_undo_coll(Undo_Collection *undo_coll);

/**
 * Check if the current node with the value specified is unique in the XML document.
 * @param unique_head a pointer to the unique list head
 * @param value the value to check as string
 * @param cur_node the current node to be checked for uniqueness
 * @return true if the node is unique, false otherwise
 */
static bool is_unique(List_Node *unique_head, xmlChar *value, xmlNodePtr cur_node);

/**
 * Get the unique node and add it to the unique list.
 * @param unique_head a pointer to the pointer of the the unique list head
 * @param schema a pointer to the schema object
 * @param schema_element a pointer to the schema element holding the unique node
 * @param xml a pointer to the corresponding XML object
 * @param xml_node a pointer to the xml node representing the scope of the unique check 
 */
static void get_unique(List_Node **unique_head, Schema_Obj *schema, xmlNodePtr schema_element, XML_Obj *xml, xmlNodePtr xml_node);

/**
 * Free a unique object. Since the members are pointers to external objects they are not freed.
 * @param data a pointer to the unique object
 */
static void free_unique(void *data);

/**
 * Wrapper function for freeing libxml XML nodes in a generic list.
 * @param data the xmlNodePtr to be freed
 */
static void xmlFreeNode_wrapper(void *data);

/**
 * Wrapper function for freeing libxml strings in a generic list.
 * @param data the xmlChar pointer to be freed
 */
static void xmlFreeString_wrapper(void *data);

/**
 * Allocate memory for a string and fill it with the parameters given.
 * @param format the format string
 * @return the newly created string, it is up to the user to free it
 */
xmlChar *create_string(xmlChar *format, ...);

/**
 * Reallocate memory for an existing string concatenated by a new string.
 * @param old_str the string to be extended
 * @param new_str the string to be concatenated
 * @return a pointer to the concatenated string
 */
xmlChar *concat_string(xmlChar *old_str, const xmlChar *new_str);

/**
 * Utility function to check if string represents a decimal number.
 * @param str the xmlChar array to check
 * @return true if a decimal number, false otherwise
 */
bool is_decimal(char *str);

/**
 * Utility function to check if string represents an integer.
 * @param str the xmlChar array to check
 * @return true if an integer, false otherwise
 */
bool is_integer(char *str);

/**
 * Utility function to obtain a number before a delimiter character.
 * @param str the xmlChar array to check
 * @param delimiters the xmlChar array containing the delimiters to look for
 * @param delim_len the number of delimiters
 * @param digit a pointer to an integer to store the number in
 * @param digit_len the number of digits the number string must contain
 * @return a pointer to the delimiter character found, NULL if delimiter has not been found or characters other than digits have been found 
 */
xmlChar *get_number_before(xmlChar *str, xmlChar *delimiters, int delim_len, int *digit, int digit_len);

/**
 * Get the Xpath context pointer from a XML document.
 * @param xml_doc a pointer to the XML document struct
 * @return a Xpath context pointer
 */
static xmlXPathContextPtr get_xpath_context_ptr(xmlDocPtr xml_doc);

/**
 * Evaluate XPath exprssion, check result and change context if given.
 * @param xpath_exp the XPath expression to evaluate
 * @param xpath_ctxt the XPath context to use
 * @param ctxt_node the node to set the context to if given, skipped if NULL is given
 * @return a XML XPath object containing the result(s)
 */
xmlXPathObjectPtr get_xpath_result(const xmlChar *xpath_exp, xmlXPathContextPtr xpath_ctxt, xmlNodePtr ctxt_node);

/**
 * Get file opening and validation errors and redirect them to the 'errors' variable.
 * @param ctx the validation context, used by libxml internally
 * @param msg the format char, used by libxml internally
 */
static void catch_errors(void *ctx, const char *msg, ...);

/**
 * Get the last error message(s).
 * @return a pointer to the error message string 
 */
xmlChar *get_error_message(void);

/**
 * Save an XML document to a file.
 * @param xml the XML object
 * @param file_path the path to save the file to
 * @param encoding the encoding to be used as string
 * @param format whether to add foramtting to output XML
 */
void save_to_file(XML_Obj *xml, xmlChar *file_path, xmlChar *encoding, bool format);

/**
 * Check whether execution is to be continued or interrupted by determining if the XML node is unlinked and if the user requests interruption of execution.
 * @param com the communicator object used for interthread communication
 * @param undo_coll the undo collection used
 * @param xml_node the XML node to be checked
 * @return true, if the threshold is reached and the xml node is not freed, false otherwise
 */
static bool check_continue(Communicator *com, Undo_Collection *undo_coll, xmlNodePtr xml_node);

/**
 * Unlink and remove all child nodes of a given name.
 * @param xml_node a pointer to the XML node to be cleaned
 */
static void remove_all_child_nodes(xmlNodePtr xml_node);

/**
 * Add template namespace to a node and add namespace attribute to the root node if not exists.
 * @param com the Communicator the communicator object used for interthread communication
 * @param node the current XML node
 */
static void create_template_ns(Communicator *com, xmlNodePtr node);

/**
 * Create action type, check function and description text for builtin type.
 * This function expects pointers to the variables to store the values in.
 * @param builtin_type the built-in type string
 * @param action_type the action type describing the type
 * @param check_function the function checking if value is correct
 * @param desc_text a text describing what type is requested
 */
static void prepare_builtin_type(xmlChar *builtin_type, Action_Type *action_type, Check_function *check_function, xmlChar **desc_text);

/**
 * Check if a value is within bounds.
 * @param to_comp the string containing the value to compare
 * @param min_bounds the minimum bounds object (see #Bounds)
 * @param max_bounds the maximum bounds object (see #Bounds)
 * @param action the Action type defining the built-in type
 * @return true if within bounds, false otherwise
 */
static bool check_bounds(xmlChar *to_comp, Bounds *min_bounds, Bounds *max_bounds, Action_Type action_type);

/**
 * Read a schema file, check its validity, get its xpath context and register namespaces. In case a validation error occurs, the error description is saved to the 'errors' string.
 * @param schema_path the path to the schema file as string
 * @param encoding the encoding to be used for interpreting the schema
 * @return a Schema_Obj containing the schema and its xpath context or NULL in case of error
 */
Schema_Obj *get_schema(xmlChar *schema_path, xmlChar *encoding);

/**
 * Free the schema and its xpath context.
 * @param schema the schema object to free
 */
void free_schema(void *schema);

/**
 * Read an existing XML file into tree.  In case a validation error occurs, the error description is saved to the 'errors' string.
 * @param schema a pointer to the schema object for validation
 * @param xml_path the path to the XML file as string
 * @param the encoding to use when parsing the XML file
 * @return an XML_Obj containing the XML document and its xpath context or NULL in case of error
 */
XML_Obj *get_xml(Schema_Obj *schema, xmlChar *xml_path, xmlChar *encoding);

/**
 * Create a new XML file according to a schema
 * @param schema a pointer to the schema object for creation
 * @param version a string indicating the XML version (1.0 or 1.1)
 * @return an XML_Obj containing the XML document and its xpath context
 */
XML_Obj *create_xml(Schema_Obj *schema, xmlChar *version);

/**
 * Create an XML object with document and its xpath context.
 * @param doc a pointer to an XML document
 * @return an XML_Obj containing the XML document and its xpath context
 */
static XML_Obj *create_xml_obj(xmlDocPtr doc);

/**
 * Free an XML object.
 * @param xml the XML object
 */
void free_xml(void *xml);

/**
 * Get the root element from the schema.
 * @param schema pointer to a schema object
 * @return a pointer to the root element
 */
static xmlNodePtr get_schema_root_element(Schema_Obj *schema);

/**
 * Get the schema node corresponding to the XML node given.
 * @param schema pointer to a schema object
 * @param node the XML node the schema node of which is to be found
 * @return a pointer to the corresponding schema node
 */
static xmlNodePtr get_schema_node(Schema_Obj *schema, xmlNodePtr node);

/**
 * Get an external schema-level type definition.
 * @param schema the schema object
 * @param name the name of the type
 * @return a pointer to the schema type node
 */
static xmlNodePtr get_external_schema_type(Schema_Obj *schema, xmlChar *name);

/**
 * Get the type of an element in the schema document.
 * @param schema the schema object
 * @param schema_element the element from which to search the type
 * @return a pointer to the schema type node
 */
static xmlNodePtr get_schema_type(Schema_Obj *schema, xmlNodePtr schema_element);

/**
 * Get a group schema node node by reference.
 * @param schema_group the group element holding the reference to the group description
 * @param schema the schema object
 * @return a pointer to the group description element
 */
static xmlNodePtr get_group(xmlNodePtr schema_group, Schema_Obj *schema);

/**
 * Get the minimum and maximum bounds from a schema node.
 * @param min a pointer to an integer that will hold the minimum value
 * @param max a pointer to an integer that will hold the maximum value
 * @param schema_node a pointer to the node to get the values from
 */
static void get_bounds(int *min, int *max, xmlNodePtr schema_node);

/**
 * Get all nodes created by a schema choice.
 * @param com the Communicator object used for interthread communication
 * @param schema_order_indicator the schema order indicator node
 * @param parent_node the parent node of the node(s) created by choice
 * @return a result object holding all nodes found
 */
static Result *get_order_indicator_nodes(Communicator *com, xmlNodePtr schema_order_indicator, xmlNodePtr parent_node);

/**
 * Get a referenced attribute schema node.
 * @param ref the name of the referenced attribute
 * @param schema the schema object
 * @return the referenced attribute node
 */
static xmlNodePtr get_referenced_node(xmlChar *ref, Schema_Obj *schema);

/**
 * Get annotations of a schema element and add them as template elements to the corresponding XML nodes.
 * @param schema_element the schema element holding the annotation(s)
 * @param xml_node the XML node adding the annotation to, checked for name correspondence in case of an schema element definition
 * @param com the Communicator object used for interthread communication
 */
static void get_annotations(xmlNodePtr schema_element, xmlNodePtr xml_node, Communicator *com);

/**
 * Get the built-in type of a simple or built-in type schema node. Recursively searches through the simple type child nodes until the basic built-in type is found.
 * @param schema_restriction the schema restriction to get the built-in type from
 * @param schema the schema object
 * @return the built-in type as string
 */
static xmlChar *get_builtin_type(xmlNodePtr schema_restriction, Schema_Obj *schema);

/**
 * Default creation function make continue check and adding annotations before executing the create function given.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_attribute the schema node defining the attribute
 * @param xml_element the XML element to add the attribute to
 * @param create_function the create function to execute 
 */
static void create(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_node, Create_function create_function);

/**
 * Create an attribute specified by the schema.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_attribute the schema node defining the attribute
 * @param xml_element the XML element to add the attribute to
 */
static void create_fixed_default(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_node, xmlNodePtr xml_element);

/**
 * Create an attribute specified by the schema.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_attribute the schema node defining the attribute
 * @param xml_element the XML element to add the attribute to
 */
static void create_attribute(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_attribute, xmlNodePtr xml_element);

/**
 * Create a new element according to the bounds specified by the schema.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_element the current element to process
 * @param xml_element the XML element that will contain the complex content
 */
static void create_element(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_element);

/**
 * Create a child element or indicator of an order indicator schema element (sequence, choice, all)
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_indicator_child the schema indicator child node to process
 * @param xml_element the XML element that will contain the complex content
 */
static void create_order_indicator_child(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_indicator_child, xmlNodePtr xml_element);

/**
 * Create a sequence of child nodes according to the schema.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_sequence the sequence to process
 * @param xml_element the XML element that will contain the child nodes
 */
static void create_sequence(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_sequence, xmlNodePtr xml_element);

/**
 * Let the user choose one child nodes according to the schema.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_choice the choice to process
 * @param xml_element the XML element that will contain the child nodes
 */
static void create_choice(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_choice, xmlNodePtr xml_element);

/**
 * Create the element collection given in a schema group.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_group the group tag referencing the group description
 * @param xml_element the XML element that will contain the child nodes 
 */
static void create_group(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_group, xmlNodePtr xml_element);

/**
 * Extend a given type with additional elements.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_extension the extension to process
 * @param xml_element the XML element that will contain the child nodes 
 */
static void create_extension(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_extension, xmlNodePtr xml_element);

/**
 * Let the user choose from an enumeration.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_restriction the restriction to process
 * @param xml_node the XML node that will contain the child nodes 
 */
static void create_enumeration_facet(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_restriction, xmlNodePtr xml_node);

/**
 * Check user input for a regex pattern.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_restriction the restriction to process
 * @param xml_node the XML node that will contain the child nodes 
 */
static void create_pattern_facet(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_restriction, xmlNodePtr xml_node);

/**
 * Restrict a given type.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_restriction the restriction to process
 * @param xml_element the XML element that will contain the child nodes 
 */
static void create_restriction(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_restriction, xmlNodePtr xml_element);

/**
 * Create a complex content.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_content the content to process
 * @param xml_element the XML element that will contain the child nodes 
 */
static void create_complex_content(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_content, xmlNodePtr xml_element);

/**
 * Create a simple content.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_content the content to process
 * @param xml_element the XML element that will contain the child nodes 
 */
static void create_simple_content(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_content, xmlNodePtr xml_element);

/**
 * Create complex type element content according to the schema.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_type the current type to process
 * @param xml_element the XML element that will contain the complex content
 */
static void create_complex_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_type, xmlNodePtr xml_element);

/**
 * Create simple type element content according to the schema.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_type the current type to process
 * @param xml_element the XML element that will contain the simple type
 */
static void create_simple_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_type, xmlNodePtr xml_element);

/**
 * Create build in types according to type attribute.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_element the element to process the type of
 * @param xml_element the XML element that will contain the simple type
 */
static void create_builtin_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_element);

/**
 * Check the type and redirect to the according create function.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication
 * @param schema_type the current type to process
 * @param xml_element the XML element that will contain the complex content
 */
static void create_schema_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_type, xmlNodePtr xml_element);

/**
 * Create node content by looking at the type and the unique tags in schema XML.
 * @param undo_coll a pointer to the undo collection used to manage undo operations
 * @param unique_head a pointer to the pointer of the unique list head
 * @param com the Communicator object used for interthread communication and holding the schema and XML objects
 * @param schema_element the current element to process
 * @param xml_element the current corresponding xml element
 */
static void create_content(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_element);

/**
 * Create new XML by going through the schema recursively. It is up to the user to free the communicator and its members.
 * @param com the Communicator object used for interthread communication and holding the schema and XML objects
 */
static void create_by_schema(Communicator *com);

/**
 * Edit existing XML by allowing to search for certain nodes via Xpath and then offer editing.
 * @param com the Communicator object used for interthread communication and holding the schema and XML objects
 */
static void edit_by_schema(Communicator *com);

/**
 * Check if there is currently a request.
 * @param com the Communicator object to be checked
 * @return true if there is, otherwise false
 */
bool has_request(Communicator *com);

/**
 * Create a new empty request object with its members initialized.
 * @param type the type of the request (see #Request_Type)
 * @param action a description of which create/edit/delete action has been performed or its status (see #Action_Type)
 * @return a pointer to the new request object
 */
static Request *create_request(Request_Type type, Action_Type action);

/**
 * Create a new request object used for requesting information from the user.
 * @param type whether it is a confirm or string request (see #Request_Type)
 * @param action specification which data is requested (see #Action_Type)
 * @param refuse specification why data has been refused (see #Refuse_Type)
 * @param node_name the name of the node the creation of which is to be communicated
 * @param node_type the node type written out as a string
 * @param content the possible string content
 * @return a pointer to the new request object
 */
static Request *create_data_request(Request_Type type, Action_Type action, Refuse_Type refuse, xmlChar *node_name, xmlChar *node_type, xmlChar *content);

/**
 * Create a new empty choice request object with its members initialized.
 * @param action an integer representing the action performed (see #Action_Type)
 * @param refuse specification why data has been refused (see #Refuse_Type)
 * @param node_name the name of the parent node the chosen node will be attached to
 * @return a pointer to the new request object
 */
static Request *create_choice_request(Action_Type action, Refuse_Type refuse, xmlChar *node_name);

/**
 * Create a new request holding the search results.
 * @param a pointer to the nodeset holding the results 
 * @return a pointer to the new request object
 */
static Request *create_search_request(xmlNodeSetPtr nodeset);

/**
 * Create a new request indicating that the creation of the XML has been completed.
 * @return a pointer to the request created
 */
static Request *create_complete_request(void);

/**
 * Add a choice to the choice list.
 * @param request the request object to add the choice to
 * @param choice the choice to be added
 */
static void add_choice(Request *request, xmlChar *choice);

/**
 * Create and make a request confirming the execution of an action.
 * Prompts the calling thread to confirm that the reception of this request.
 * com the Communicator object used for interthread communication
 * @param type the type of the request (see #Request_Type)
 * @param action the action performed (see #Action_Type)
 * @param node a pointer to a possible newly created node
 */
void confirm_execution(Communicator *com, Request_Type type, Action_Type action, xmlNodePtr xml_node);

/**
 * Create a request object confirming an action.
 * @param action the action performed
 * @param node a pointer to a possible newly created node
 * @return a pointer to the new request object
 */
static Request *create_confirmation(Action_Type action, xmlNodePtr node);

/**
 * Free a request object and its members.
 * @param request a pointer to the request object to be freed
 */
static void free_request(Request *request);

/**
 * Make an answer object and fill out with defaults for templates
 * @param request a pointer to the request object
 * @param request a pointer to the answer object to populate
 * @return the answer object, it is up to the user to free it
 */
static Answer *make_template_answer(Request *request, Answer *answer);

/**
 * Send create request to main thread and wait for the answer and check if to continue.
 * @param com the Communicator object used
 * @param request a pointer to the request object
 * @param undo_coll a pointer to the undo collection used to manage undo operations (not needed in case of confirmation)
 * @param unique_head a pointer to the pointer of the unique list head (not needed in case of confirmation)
 * @param create_function the current function in which undo was called (not needed in case of confirmation)
 * @param schema_element the current schema element to process (not needed in case of confirmation)
 * @param xml_node the current corresponding xml node (not needed in case of confirmation)
 * @return the answer object, it is up to the user to free it, or null to indicate that further creating or editing is to be interrupted
 */
static Answer *make_create_request_with_check(Communicator *com, Request *request, Undo_Collection *undo_coll, List_Node **unique_head, Create_function create_function, xmlNodePtr schema_element, xmlNodePtr xml_node);

/**
 * Send completion request to main thread and wait for the answer.
 * Answer can be either undo or quit
 * @param com the Communicator object used
 * @param request a pointer to the request object
 * @param undo_coll a pointer to the undo collection used to manage undo operations (not needed in case of confirmation)
 * @param unique_head a pointer to the pointer of the unique list head (not needed in case of confirmation)
 * @return the answer object, it is up to the user to free it, or null to indicate that further creating or editing is to be interrupted
 */
static Answer *make_completion_request(Communicator *com, Undo_Collection *undo_coll, List_Node **unique_head);

/**
 * Send search request to main thread and wait for the answer.
 * @param com the Communicator object used
 * @param request a pointer to the request object
 */
static Answer *make_search_request(Communicator *com, Request *request);

/**
 * Confirm that a request has been received. Used if no answer is expected.
 * @param com the communicator object used for interthread communication
 */
void confirm_received(Communicator *com);

/**
 * Create a new empty answer object.
 * @param type the type of the answer (see #Answer_Type)
 * @return a newly created answer object with its values initialized
 */
static Answer *create_answer(Answer_Type type);

/**
 * Request the cancellation of the creation process and the freeing of the data.
 * @param com the Communicator object used
 * @return true if execution of subthread finished, false in case of error
 */
bool quit(Communicator *com);

/**
 * Signal that the user accepts to worker thread.
 * Used when a confirmation is requested for creation (see #Request_Type, #Answer_Type).
 * @param com the Communicator object used
 */
void accept(Communicator *com);

/**
 * Signal that the user accepts once to worker thread. Used for testing purposes.
 * Used when a confirmation is requested for creation (see #Request_Type, #Answer_Type).
 * @param com the Communicator object used
 */
void accept_once(Communicator *com);

/**
 * Signal that the user refuses to worker thread.
 * Used when a confirmation is requested for creation (see #Request_Type, #Answer_Type).
 * @param com the Communicator object used
 */
void refuse(Communicator *com);

/**
 * Send string as answer to worker thread.
 * Used when a string is requested for creation (see #Request_Type).
 * @param com the Communicator object used
 * @param data a pointer to the answer string
 */
void answer(Communicator *com, xmlChar *data);

/**
 * Send xpath expression as answer to worker thread.
 * Used when a search request is sent to user (see #Request_Type).
 * @param com the Communicator object used
 * @param data a pointer to the answer string
 */
void send_xpath(Communicator *com, xmlChar *xpath_exp);

/**
 * Send the user's choice as the index of the chosen string.
 * Used when a choice is requested for creation (see #Request_Type).
 * @param com the Communicator object used
 * @param chosen_index the index of the chosen string starting from 0
 */
void choose(Communicator *com, int chosen_index);

/**
 * Request editing of XML node given.
 * @param com the Communicator object used
 * @param node a pointer to the XML node to be edited
 */
void request_editing(Communicator *com, xmlNodePtr node);

/**
 * Request deletion of XML node given.
 * @param com the Communicator object used
 * @param node a pointer to the XML node to be deleted
 */
void request_deletion(Communicator *com, xmlNodePtr node);

/**
 * Request undo from sub thread and reset request.
 * @param com the Communicator object used
 */
void request_undo(Communicator *com);

/**
 * Check the type of the answer and redirect accordingly.
 * @param answer the answer object to check
 * @param undo_coll the undo collection holding the undo list and the unlinked list
 * @param unique_head a pointer to a pointer to the unique list
 * @param com the communicator object used for interthread communication
 * @param create_function the current function in which undo was called
 * @param cur_schema_node the currently processed schema node
 * @param cur_xml_node the currently processed XML node
 */
static bool check_answer(Answer *answer, Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, Create_function create_function, xmlNodePtr schema_node, xmlNodePtr xml_node);

/**
 * Free an answer object and its members.
 * @param answer a pointer to the answer object to be freed
 */
static void free_answer(Answer *answer);

/**
 * Create a communicator used for interthread communication.
 * Allocate memory for a new communicator object and set its members to NULL or zero.
 * @param schema the schema object used for creation
 * @param xml the XML object containing the XML to be created or edited
 */
Communicator *create_communicator(Schema_Obj *schema, XML_Obj *xml);

/**
 * Start the creation of a new XML file in a subthread. The communication between calling and subthread is made via the communicator.
 * @param com the Communicator object used for interthread communication.
 * @param type whether the subthread should communicate with the calling thread or not to create the XML file, see also #Creation_Type
 */
void start_creation(Communicator *com, Creation_Type type);

/**
 * Start an XML creation.
 * @param thread_val a pointer to the communicator used to communicate with the calling main thread
 * @return returns a NULL pointer
 */
static void *start_internal_creation(void *thread_val);

/**
 * Start the editing of a new XML file in a subthread. The communication between calling and subthread is made via the communicator.
 * @param com the Communicator object used for interthread communication.
 */
void start_editing(Communicator *com);

/**
 * Start an XML editing dialog with user.
 * @param thread_val a pointer to the communicator used to communicate with the calling main thread
 * @return returns a NULL pointer
 */
static void *start_internal_editing(void *thread_val);

#endif

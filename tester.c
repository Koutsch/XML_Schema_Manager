#include <stdlib.h>
#include "validator.h"
 
int main(const int argc, const char **argv)
{
	void *temp_validator = get_validator();

 	char *err_str = validate_xsd(temp_validator, "/home/koutsch/XML_Schemas/test.xsd");
 	
 	printf("Validator says: %s\n", err_str);
 	
 	free(err_str);
	
	return( EXIT_SUCCESS );
}

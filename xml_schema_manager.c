/**
 * To compile this file using gcc you can type
 * gcc `xml2-config --cflags --libs` `pkg-config --cflags --libs` -o PROG_NAME FILE_NAME.c -lpthread -export-dynamic -lpcre
 */

#include "xml_schema_manager.h"

#if defined(LIBXML_TREE_ENABLED) && defined(LIBXML_XPATH_ENABLED) && defined(LIBXML_OUTPUT_ENABLED) && defined(LIBXML_SCHEMAS_ENABLED)

#define MAX_BUFFER 2048

#define XPATH_ALL_CHILDREN ".//*"
#define XPATH_ATTR "xs:attribute"
#define XPATH_ATTR_BY_NAME "/xs:schema/xs:attribute[@name='%s']"
#define XPATH_TYPE_BY_NAME "/xs:schema/xs:complexType[@name='%s'] | /xs:schema/xs:simpleType[@name='%s']"
#define XPATH_GROUP_BY_REF "/xs:schema/xs:group[@name='%s']"
#define XPATH_TYPE "xs:complexType | xs:simpleType"
#define XPATH_TYPE_CHILD "xs:restriction|xs:list"
#define XPATH_ELEM "/xs:schema/xs:element"
#define XPATH_ELEM_CHILDREN ".//xs:element"
#define XPATH_SCHEMA_NODE ".//xs:element[@name='%s'] | .//xs:attribute[@name='%s'] | .//xs:attribute[@ref='%s']"
#define XPATH_CHOICE "xs:choice"
#define XPATH_CHOICES "xs:element | xs:sequence | xs:choice | xs:group"
#define XPATH_SELECTOR "xs:selector"
#define XPATH_UNIQUE "xs:unique"
#define XPATH_ENUM "xs:enumeration"
#define XPATH_PATTERN "xs:pattern"
#define XPATH_BOUNDS "xs:minExclusive | xs:minInclusive | xs:maxExclusive | xs:maxInclusive"
#define XPATH_SEQ "xs:sequence"
#define XPATH_COMP_CONT "xs:complexContent"
#define XPATH_SIMPLE_CONT "xs:simpleContent"
#define XPATH_REST "xs:restriction"
#define XPATH_GROUP "xs:group"
#define XPATH_GROUP_CHILDREN ".//xs:group"
#define XPATH_ANNOTATION "xs:annotation"
#define XPATH_ANYURI "xs:anyURI"
#define XPATH_DECIMAL "xs:decimal"
#define XPATH_INTEGER "xs:integer"
#define XPATH_DATE "xs:date"
#define XPATH_TIME "xs:time"
#define XPATH_DATETIME "xs:dateTime"

#define TEMPLATE_NAMESPACE_URI "xml_schema_manager/template"
#define TEMPLATE_NAMESPACE_PREFIX "t"

/**
 * A string for possible schema or XML opening and validation errors.
 */
static char *errors;

/* UNDO */

static Undo_Collection *create_undo_collection(void)
{
	Undo_Collection *undo_coll;
	
	// allocate memory for collection
	if (!(undo_coll = malloc(sizeof(Undo_Collection))))
	{
		ERROR("Could not allocate memory for undo collection", true);
		return NULL;
	}
	
	// set members to NULL
	undo_coll->undo_head = NULL;
	
	undo_coll->unlinked_head = NULL;
	
	return undo_coll;
}

static void process_undo(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, Create_function cur_create_function, xmlNodePtr cur_schema_node, xmlNodePtr cur_xml_node)
{
	Undo_Obj					*undo_obj = NULL;
	
	List_Node					*undo_head = NULL,
										*tmp_undo = NULL;
	
	Result						*choice_nodes = NULL;

	int								i;
	
	Create_function		create_function;
	
	// check if undo head exists
	if (undo_coll->undo_head)
	{		
		undo_head = undo_coll->undo_head;
		
		// get the undo object
		undo_obj = undo_head->data;
		
		// store create function of undo object
		create_function = undo_obj->create_function;
		
		// remove first (recent) node from list
		tmp_undo = undo_head;
		undo_coll->undo_head = undo_head->next;
		free(tmp_undo);
		
		// check if there is a node to remove
		if (undo_obj->new_node) 
		{		
			xmlUnlinkNode(undo_obj->new_node);
			push_list(&(undo_coll->unlinked_head), undo_obj->new_node);
		}
		
		// if a choice is undone, unlink all child elements being part of the choice
		if (create_function == create_choice)
		{
			choice_nodes = get_order_indicator_nodes(com, undo_obj->schema_node, undo_obj->parent_node);
			
			for (i = 0; i < choice_nodes->count; i++)
			{
				xmlUnlinkNode(choice_nodes->nodes[i]);
				push_list(&(undo_coll->unlinked_head), choice_nodes->nodes[i]);
			}
			
			free(choice_nodes->nodes);
			free(choice_nodes);
		}
		
		// repeat the undone node creation		
		create(undo_coll, undo_obj->unique_head, com, undo_obj->schema_node, undo_obj->parent_node, undo_obj->create_function);	
	
		free(undo_obj);
	}
	
	// repeat the function in which undo was called if given, and check if not text only
	if (cur_create_function && cur_schema_node && cur_xml_node && !(cur_xml_node->children != NULL && cur_xml_node->children->type == 3 && cur_xml_node->children->next == NULL))
	{	
		create(undo_coll, unique_head, com, cur_schema_node, cur_xml_node, cur_create_function);
	}
}

static void add_undo(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, Create_function create_function, xmlNodePtr schema_node, xmlNodePtr parent_node, xmlNodePtr new_node)
{
	Undo_Obj	*undo;
	
	// allocate and check a new undo object
	if (!(undo = malloc(sizeof(Undo_Obj))))
	{
		ERROR("Could not allocate memory for undo object", true);
		return;
	}
	
	// add the members of the undo object
	undo->unique_head = unique_head;
	undo->create_function = create_function;
	undo->schema_node = schema_node;
	undo->parent_node = parent_node;
	undo->new_node = new_node;
	
	// add the new undo object to the undo list
	push_list(&undo_coll->undo_head, undo);
}

static void free_undo_coll(Undo_Collection *undo_coll)
{	
	// free the undo object list
	if (undo_coll->undo_head)
	{
		free_list(&(undo_coll->undo_head), free);
	}
	
	// free all unlinked nodes
	if (undo_coll->unlinked_head)
	{
		free_list(&(undo_coll->undo_head), xmlFreeNode_wrapper);
	}
	
	// and free the collection itself
	free(undo_coll);
}

/* UNDO END */

/* UNIQUE */

static bool is_unique(List_Node *unique_head, xmlChar *value, xmlNodePtr cur_node)
{	
	bool							value_equals,
										node_equals;
	
	List_Node					*cur;

	Unique_Obj				*unique;

	Schema_Obj				*schema;	

	XML_Obj						*xml;

	xmlNodePtr				schema_unique,
										xml_node,
										child;
	
	xmlChar						*selector = NULL,
										*field_value = NULL;
							
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	int								i;
	
	// initialize the equality booleans to false
	value_equals = false;
	node_equals = false;
	
	// loop through the unique object list	
	for (cur = unique_head; cur; cur = cur->next)
	{
		// get the current unique object
		unique = cur->data;
		
		// get its members
		schema = unique->schema;
		schema_unique = unique->schema_unique;
		xml = unique->xml;
		xml_node = unique->xml_node;
		
		// get the selector node
		if (!(xpath_obj = get_xpath_result(XPATH_SELECTOR, schema->xpath_ctxt, schema_unique)) || xpath_obj->nodesetval->nodeNr != 1)
		{
			ERROR("Could not get selector via xpath", true);
			goto cleanup;
		}
		
		// get the selector xpath
		if (!(selector = xmlGetProp(xpath_obj->nodesetval->nodeTab[0], "xpath")))
		{
			ERROR("Could not get xpath attribute from selector", true);
			goto cleanup;
		}
		
		// free the selector xpath object
		xmlXPathFreeObject(xpath_obj);
		
		// get the nodes defined by selector
		if (!(xpath_obj = get_xpath_result(selector, xml->xpath_ctxt, xml_node)))
		{
			ERROR("Could not get nodes for selector '%s'", selector);
			goto cleanup;
		}
		
		// loop selected nodes
		for (i = 0; i < xpath_obj->nodesetval->nodeNr; i++)
		{
			// loop fields
			for (child = schema_unique->children; child; child = child->next)
			{
				if (EQUALS(child->name, "field"))
				{
					xmlXPathObjectPtr	xpath_field = NULL;
					
					xmlChar						*field = NULL;
					
					// get xpath attribute
					if (!(field = xmlGetProp(child, "xpath")))
					{
						ERROR("Could not get xpath attribute from field", true);
						goto cleanup;
					}
					
					// get the node defined by field
					if (!(xpath_field = get_xpath_result(field, xml->xpath_ctxt, xpath_obj->nodesetval->nodeTab[i])))
					{
						ERROR("Could not get xpath object for field '%s'", field);
						xmlFree(field);
						goto cleanup;
					}
					
					// check if there is a field node
					if (xpath_field->nodesetval->nodeNr > 0)
					{
						// check value equality
						if (field_value = xmlNodeListGetString(xml->doc, xpath_field->nodesetval->nodeTab[0]->xmlChildrenNode, 1))
						{
							if (EQUALS(field_value, value))
							{
								value_equals = true;
							}
							
							xmlFree(field_value);
						}
					
						// check node equality
						if (xpath_field->nodesetval->nodeTab[0] == cur_node)
						{
							node_equals = true;
						}
					}
					
					// free the field xpath object
					xmlXPathFreeObject(xpath_field);
					
					// free the field xpath
					xmlFree(field);
				}
			}	
		}
		
		// free the selector xpath object
		xmlXPathFreeObject(xpath_obj);
		xpath_obj = NULL;
		
		// free the selector xpath
		xmlFree(selector);
		
		// return that node is not unique
		if (value_equals && node_equals)
		{
			return false;
		}
	}
	
	// after checking everything, return that node is unique
	return true;
	
	cleanup:
		if (xpath_obj) xmlXPathFreeObject(xpath_obj);
		if (selector) xmlFree(selector);
		return false;
}

static void get_unique(List_Node **unique_head, Schema_Obj *schema, xmlNodePtr schema_element, XML_Obj *xml, xmlNodePtr xml_node)
{
	xmlXPathObjectPtr	xpath_obj;
	
	Unique_Obj				*unique;
	
	int								i;
	
	// get the unique node from the element
	if (!(xpath_obj = get_xpath_result(XPATH_UNIQUE, schema->xpath_ctxt, schema_element)))
	{
		ERROR("Could not get unique node", true);
		return;
	}
	
	// loop unique nodes
	for (i = 0; i < xpath_obj->nodesetval->nodeNr; i++)
	{
		// create new unique object
		if (!(unique = malloc(sizeof(Unique_Obj))))
		{
			ERROR("Could not allocate memory for unique", true);
			xmlXPathFreeObject(xpath_obj);
			return;
		}
		
		// and add the data if successfully allocated
		unique->schema = schema;
		unique->schema_unique = xpath_obj->nodesetval->nodeTab[i];
		unique->xml = xml;
		unique->xml_node = xml_node;
		push_list(unique_head, unique);
	}
	
	xmlXPathFreeObject(xpath_obj);
}

static void free_unique(void *data)
{
	Unique_Obj	*unique;
	
	unique = data;
	
	free(unique);
}

/* UNIQUE END */

/* UTILITIES */

static void xmlFreeNode_wrapper(void *data)
{
	xmlNodePtr	node = data;
	
	xmlFreeNode(node);
}

static void xmlFreeString_wrapper(void *data)
{
	xmlChar	*string = data;
	
	xmlFree(string);
}

xmlChar *create_string(xmlChar *format, ...)
{
	va_list	argp;
	
	int			len, 
					size = sizeof(xmlChar) * MAX_BUFFER;
	
	xmlChar	*new_str = NULL;
	
	// start the var args
	va_start(argp, format);
	
	// allocate memory for the new string
	if (!(new_str = malloc(size)))
	{
		ERROR("Could not allocate memory for new string", true);
		goto cleanup;
	}
	
	// assign new string and get string length for check
	if ((len = vsnprintf(new_str, size, format, argp)) < 0)
	{
		ERROR("Could not add string content, errcode: %i", len);
		goto cleanup;
	}
	
	// if the string is too long
	if ((len + 1) > size)
	{
		// start the var args anew
		va_start(argp, format);
		
		// reallocate memory to fit the string
		if (!(new_str = realloc(new_str, sizeof(xmlChar) * (len + 1))))
		{
			ERROR("Could not reallocate memory for new string", true);
			goto cleanup;
		}
		
		// assign the string to an extended empty string
		if ((len = vsprintf(new_str, format, argp)) < 0)
		{
			ERROR("Could not add string content after reallocation, errcode: %i", len);
			goto cleanup;
		}
	}
	
	// clean up var args
	va_end(argp);
	
	return new_str;
	
	cleanup:
		if (new_str) xmlFree(new_str);
		va_end(argp);
		return NULL;
}

xmlChar *concat_string(xmlChar *old_str, const xmlChar *new_str)
{
	size_t	old_str_len = 0,
					new_str_len;
	
	// check if new string is not null
	if (new_str)
	{
		// check if old string exists and get get size
		if (old_str) old_str_len = xmlStrlen(old_str);
		
		// add length of new string
		new_str_len = xmlStrlen(new_str) + 1; // initialize to 1 for \0 char

		if (!(old_str = realloc(old_str, (old_str_len + new_str_len))))
		{
			ERROR("Could not reallocate memory for extended string", true);
			return NULL;
		}
		
		memcpy(old_str + old_str_len, new_str, new_str_len);
	}
	
	return old_str;
}

bool is_decimal(char *str)
{
	if (!(isdigit(*str)) && *str != 0x2D) return FALSE;
	
	str++;
	
	while (*str != '\0')
	{
		if (!(isdigit(*str)) && *str != 0x2E) return FALSE;
		
		str++;
	}

	return TRUE;	
}

bool is_integer(char *str)
{
	if (!(isdigit(*str)) && *str != '-' && *str != '+') return FALSE;
	
	str++;
	
	while (*str != '\0')
	{
		if (!(isdigit(*str))) return FALSE;
		
		str++;
	}

	return TRUE;
}

static xmlXPathContextPtr get_xpath_context_ptr(xmlDocPtr xml_doc)
{
	xmlXPathContextPtr	context_ptr = NULL;
	
	// check if successful and issue err msg if not
	if (!(context_ptr = xmlXPathNewContext(xml_doc))) ERROR("Cannot get Xpath context pointer for %s", xml_doc->name);
	
	return context_ptr;
}

xmlXPathObjectPtr get_xpath_result(const xmlChar *xpath_exp, xmlXPathContextPtr xpath_ctxt, xmlNodePtr ctxt_node)
{
	xmlXPathObjectPtr	xpath_obj;
	
	xmlNodePtr				tmp;
	
	// set context node if given
	if (ctxt_node)
	{
		tmp = xpath_ctxt->node;
		xmlXPathSetContextNode(ctxt_node, xpath_ctxt);
	}
	
	// evaluate the xpath expression
	if (!(xpath_obj = xmlXPathEvalExpression(BAD_CAST xpath_exp, xpath_ctxt))) ERROR("No xpath object returned for '%s'", xpath_exp);
	
	// reset the default xpath context
	if (ctxt_node) xmlXPathSetContextNode(tmp, xpath_ctxt);
	
	return xpath_obj;
}

static void catch_errors(void *ctx, const char *msg, ...)
{
	va_list	argp;
	
	char		*msg_buf;
	
	if (!(msg_buf = malloc(sizeof(char) * MAX_BUFFER)))
	{
		ERROR("Could not allocate memory for error message buffer", true);
		return;
	}
	
	// start the var args
	va_start(argp, msg);
	
	vsnprintf(msg_buf, MAX_BUFFER, msg, argp);
	
	msg_buf[MAX_BUFFER - 1] = '\0';
	
	// add to the errors
	errors = concat_string(errors, msg_buf);
	
	// get rid of buffer
	free(msg_buf);
	
	// clean up var args
	va_end(argp);
}

xmlChar *get_error_message(void)
{
	return errors;
}

void save_to_file(XML_Obj *xml, xmlChar *file_path, xmlChar *encoding, bool format)
{
	if (format) format = 1;
	else format = 0;
	
	xmlSaveFormatFileEnc(file_path, xml->doc, encoding, format);
}

static bool check_continue(Communicator *com, Undo_Collection *undo_coll, xmlNodePtr xml_node)
{	
	List_Node		*cur;
	
	xmlNodePtr	unlinked_node,
							tmp;
	
	if (com->interrupt) return false;
	
	// loop the unlinked list
	for (cur = undo_coll->unlinked_head; cur; cur = cur->next)
	{
		unlinked_node = cur->data;
		
		// check if xml node or a parent are in the unlinked list
		for (tmp = xml_node; tmp; tmp = tmp->parent)
		{
			if (tmp == unlinked_node)
			{
				// confirm that node is unlinked and return confirmation
				return false;
			}
		}
	}
	
	// if neither XML node nor one of its parents is unlinked, confirm that it's still linked
	return true;
}

static void remove_all_child_nodes(xmlNodePtr xml_node)
{
	xmlNodePtr	tmp,
							child;
	
	for (child = xml_node->children; child; )
	{
		// save child reference for deletion
		tmp = child;
		
		// assign next child
		child = child->next;
		
		// unlink and free (=remove) node
		xmlUnlinkNode(tmp);
		xmlFreeNode(tmp);		
	}
}

static void create_template_ns(Communicator *com, xmlNodePtr node)
{
	xmlNsPtr *nsp =	xmlGetNsList(com->xml->doc, xmlDocGetRootElement(com->xml->doc));
	
	xmlNsPtr ns = NULL;
	
	if (nsp != NULL)
	{
		xmlNsPtr *cur = nsp;
		while (*cur != NULL)
		{
			if (EQUALS((*cur)->prefix, TEMPLATE_NAMESPACE_PREFIX))
			{
				ns = *cur;
			}
			cur++;
		}
	}
	
	if (ns == NULL) ns = xmlNewNs(xmlDocGetRootElement(com->xml->doc), TEMPLATE_NAMESPACE_URI, TEMPLATE_NAMESPACE_PREFIX);
	
	xmlSetNs(node, ns);
	
	xmlFree(nsp);
}

static void prepare_builtin_type(xmlChar *builtin_type, Action_Type *action_type, Check_function *check_function, xmlChar **desc_text)
{
	// create and asssign values
	if (EQUALS(builtin_type, XPATH_DECIMAL))
	{
		*action_type = DECIMAL;
		*check_function = &is_decimal;
		*desc_text = create_string("decimal number");
	}
	else if (EQUALS(builtin_type, XPATH_INTEGER))
	{
		*action_type = INTEGER;
		*check_function = &is_integer;
		*desc_text = create_string("integer");
	}
	else if (EQUALS(builtin_type, XPATH_DATE))
	{
		*action_type = DATE;
		*check_function = &is_date;
		*desc_text = create_string("date [YYYY-MM-DD(Z+-|HH:MM)]");
	}
	else if (EQUALS(builtin_type, XPATH_TIME))
	{
		*action_type = TIME;
		*check_function = &is_time;
		*desc_text = create_string("time [hh:mm:ss.s(Z+-|HH:MM)]");
	}
	else if (EQUALS(builtin_type, XPATH_DATETIME))
	{
		*action_type = DATETIME;
		*check_function = &is_datetime;
		*desc_text = create_string("date time [YYYY-MM-DDThh:mm:ss.s(Z+-|HH:MM)]");
	}	
	else if (EQUALS(builtin_type, XPATH_ANYURI))
	{
		*action_type = FILE_PATH;
		*check_function = NULL;
		*desc_text = create_string("file path/URI");		
	}
	else // if not found, string is assumed
	{
		*action_type = STRING;
		*check_function = NULL;
		*desc_text = create_string("string content");
	}
	
	// TODO add other data types too
}

static bool check_bounds(xmlChar *to_comp, Bounds *min_bounds, Bounds *max_bounds, Action_Type action_type)
{
	union	_Value {
					long long int i;
					long double d;
					Time t;
				} comp, min, max;
	
	int		min_diff,
				max_diff;
	
	// switch according to action type (= built-in type)
	switch (action_type)
	{
		case INTEGER:
			comp.i = atoll(to_comp);
			min.i = atoll(min_bounds->value);
			max.i = atoll(max_bounds->value);
			
			if (comp.i == min.i) min_diff = 0;
			else if (comp.i < min.i) min_diff = -1;
			else if (comp.i > min.i) min_diff = 1;
			
			if (comp.i == max.i) max_diff = 0;
			else if (comp.i < max.i) max_diff = -1;
			else if (comp.i > max.i) max_diff = 1;			
			
			break;
		case DECIMAL:
			comp.d = atof(to_comp);
			min.d = atof(min_bounds->value);
			max.d = atof(max_bounds->value);

			if (comp.d == min.d) min_diff = 0;
			else if (comp.d < min.d) min_diff = -1;
			else if (comp.d > min.d) min_diff = 1;
			
			if (comp.d == max.d) max_diff = 0;
			else if (comp.d < max.d) max_diff = -1;
			else if (comp.d > max.d) max_diff = 1;

			break;
		case DATE:
			comp.t = get_date(to_comp);
			min.t = get_date(min_bounds->value);
			max.t = get_date(max_bounds->value);
			
			min_diff = compare_time(comp.t, min.t);
			max_diff = compare_time(comp.t, max.t);
			
			break;
		case TIME:
			comp.t = get_time(to_comp);
			min.t = get_time(min_bounds->value);
			max.t = get_time(max_bounds->value);

			min_diff = compare_time(comp.t, min.t);
			max_diff = compare_time(comp.t, max.t);

			break;
		case DATETIME:
			comp.t = get_datetime(to_comp);
			min.t = get_datetime(min_bounds->value);
			max.t = get_datetime(max_bounds->value);

			min_diff = compare_time(comp.t, min.t);
			max_diff = compare_time(comp.t, max.t);

			break;		
	}
	
	// check if within bounds
	if (!(min_bounds->type != EMPTY && ((min_bounds->type == INCLUSIVE && min_diff >= 0) || (min_bounds->type == EXCLUSIVE && min_diff > 0))))
	{
		return false;
	}
	
	if (!(max_bounds->type != EMPTY && ((max_bounds->type == INCLUSIVE && max_diff <= 0) || (max_bounds->type == EXCLUSIVE && max_diff < 0))))
	{
		return false;
	}
	
	return true;
}

/* UTILITIES END */

/* SCHEMA HANDLING */

Schema_Obj *get_schema(xmlChar *schema_path, xmlChar *encoding)
{
	xmlSchemaParserCtxtPtr	sp_ctxt = NULL;
	
	xmlDocPtr								doc = NULL;
	
	xmlNodePtr							schema_root_element = NULL;
	
	xmlErrorPtr							err = NULL;

	xmlXPathContextPtr			schema_xpath_ctxt = NULL;

	xmlNs										*namespace;

	Schema_Obj							*schema_obj;
	
	char										*err_str;
	
	// validation with XERCES C++
	if ((err_str = validate_xsd(schema_path)))
	{
		ERROR("%s", err_str);
		catch_errors(NULL, err_str);
		
		free(err_str);
		
		goto cleanup;
	}
	
	// get the schema parser context from file
	if (!(sp_ctxt = xmlSchemaNewParserCtxt(schema_path)))
	{
		ERROR("Could not load schema parser context from %s", schema_path);
		goto cleanup;
	}
	
	// clear old error messages
	free(errors);
	errors = NULL;
	
	// redirect error messages
	xmlSetGenericErrorFunc(sp_ctxt, catch_errors);
	
	// parse the schema
	if (!(xmlSchemaParse(sp_ctxt)))
	{
		ERROR("Schema for file '%s' could not be parsed", schema_path);
		goto cleanup;
	}
	
	// get XML from file
	doc = xmlReadFile(schema_path, encoding, 0);

	// check if an error occurred while loading the file
	err = xmlGetLastError();
	if (err)
	{
		ERROR("%s", err->message);
		catch_errors(NULL, err->message);
		
		// reset the error
		xmlResetLastError();
		
		goto cleanup;
	}
	
	// free the parser context as the schema is successfully parsed
	xmlSchemaFreeParserCtxt(sp_ctxt);
  
  // get the Xpath context pointer of the schema document
  if (!(schema_xpath_ctxt = get_xpath_context_ptr(doc)))
  {
		ERROR("Could not get Xpath context pointer for %s", doc->name);
  	goto cleanup;
  }
  
	// get the root element
  if (!(schema_root_element = xmlDocGetRootElement(doc)))
  {
  	ERROR("Could not get schema root element for %s", doc->name);
  	goto cleanup;
  }
  
	// register namespaces at xpath context
	for (namespace = schema_root_element->ns; namespace; namespace = namespace->next)
	{
		if (xmlXPathRegisterNs(schema_xpath_ctxt, namespace->prefix, namespace->href) != 0)
		{
			ERROR("Cannot register namespace [prefix: %s, href: %s]", namespace->prefix, namespace->href);
			goto cleanup;
		}
	}
	
	// allocate memory for the new Schema_Obj
	if (!(schema_obj = malloc(sizeof(Schema_Obj))))
	{
		ERROR("Cannot allocate memory for schema object '%s'", doc->name);
		goto cleanup;
	}
	
	// assign newly created members 
	schema_obj->doc = doc;
	schema_obj->xpath_ctxt = schema_xpath_ctxt;
	schema_obj->schema_path = xmlStrdup(schema_path);
  
  return schema_obj;
	
	cleanup:
		if (sp_ctxt) xmlSchemaFreeParserCtxt(sp_ctxt);
		if (doc) xmlFreeDoc(doc);
		if (schema_xpath_ctxt) xmlXPathFreeContext(schema_xpath_ctxt);
		xmlResetLastError();	
		return NULL;
}

void free_schema(void *schema)
{	
	Schema_Obj	*schema_obj;
	
	schema_obj = schema;

	// free xpath context
	if (schema_obj->xpath_ctxt) xmlXPathFreeContext(schema_obj->xpath_ctxt);

	// free the schema
	if (schema_obj->doc) xmlFreeDoc(schema_obj->doc);
	
	// free the validation context
	if (schema_obj->schema_path) xmlFree(schema_obj->schema_path);
	
	// finally, free the schema object itself
	free(schema_obj);
	
	// make cleanup
	xmlSchemaCleanupTypes();
}

/* SCHEMA HANDLING END */

/* XML HANDLING */

XML_Obj *get_xml(Schema_Obj *schema, xmlChar *xml_path, xmlChar *encoding)
{	
	xmlDocPtr							doc = NULL;

	xmlErrorPtr						err = NULL;

	int 									v_val = 0;
	
	char									*err_str = NULL;
	
	// get XML from file
	doc = xmlReadFile(xml_path, encoding, 0);
	
	// check if an error occurred while loading the file
	err = xmlGetLastError();
	if (err)
	{
		ERROR("%s", err->message);
		catch_errors(NULL, err->message);

		// reset the error
		xmlResetLastError();
	} 
	
	// check if exists and properly loaded
	if (doc && !(err))
	{	
		// return a newly created XML object in case document is valid
		if ((err_str = validate_xml(schema->schema_path, xml_path)))
		{
			return create_xml_obj(doc);
		}
		else
		{
			ERROR("%s", err_str);
			catch_errors(NULL, err_str);
			free(err_str);
		}
	}
	
	if (doc) xmlFreeDoc(doc);
	xmlResetLastError();
	return NULL;
}

XML_Obj *create_xml(Schema_Obj *schema, xmlChar* version)
{
	xmlDocPtr	doc = NULL;
	
	XML_Obj		*xml = NULL;
	
	// create new doc
	if (!(doc = xmlNewDoc(version)))
	{
		ERROR("Cannot create new XML document", true);
		return NULL;
	}
	
	// create the XML object
	if (!(xml = create_xml_obj(doc)))
	{
		ERROR("Cannot create xml object", true);
		if (doc) xmlFreeDoc(doc);
		return NULL;
	}
	
	return xml;	
}

static XML_Obj *create_xml_obj(xmlDocPtr doc)
{
	xmlXPathContextPtr	xpath_ctxt;

	XML_Obj							*xml;
	
	// get the xpath context for the document
	if (!(xpath_ctxt = get_xpath_context_ptr(doc)))
	{
		ERROR("Cannot get context pointer for %s", doc->name);
		return NULL;
	}
	
	// allocate memory for the object
	if (!(xml = malloc(sizeof(XML_Obj))))
	{
		ERROR("Cannot allocate memory for XML object '%s'", doc->name);
		xmlXPathFreeContext(xpath_ctxt);
		return NULL;
	}

	// assign the document and xpath context pointer
	xml->doc = doc;
	xml->xpath_ctxt = xpath_ctxt;
	
	return xml;
}

void free_xml(void *xml)
{	
	XML_Obj *xml_obj = xml;

	// free context pointer if exists
	if (xml_obj->xpath_ctxt) xmlXPathFreeContext(xml_obj->xpath_ctxt);
	
	// free the xmlDoc if exists
	if (xml_obj->doc) xmlFreeDoc(xml_obj->doc);
	
	// finally, free the struct itself
	free(xml_obj);
}

/* XML HANDLING END */

/* GET FUNCTIONS */

static xmlNodePtr get_schema_root_element(Schema_Obj *schema)
{
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	xmlNodePtr				schema_root_element = NULL;
	
	// get root element via xpath
	if (!(xpath_obj = get_xpath_result(XPATH_ELEM, schema->xpath_ctxt, NULL)) || xpath_obj->nodesetval->nodeNr < 1)
	{
		ERROR("Could not schema root element for %s with xpath", schema->doc->name);
		return NULL;
	}
	
	schema_root_element = xpath_obj->nodesetval->nodeTab[0];
	
	// free xpath object
	xmlXPathFreeObject(xpath_obj);
	
	return schema_root_element;
}

static xmlNodePtr get_schema_node(Schema_Obj *schema, xmlNodePtr node)
{
	xmlNodePtr				cur = NULL,
										cur_schema_node = NULL,
										group_node = NULL;
	
	List_Node					*ancestry = NULL,
										*cur_family = NULL;
	
	xmlXPathObjectPtr	xpath_obj = NULL,
										xpath_obj_group = NULL;
	
	xmlChar						*xpath_exp = NULL;
	
	int								i;
	
	// get all nodes until the root node
	for (cur = node; cur->parent->parent; cur = cur->parent)
	{
		push_list(&ancestry, xmlStrdup(cur->name));
	}
	
	// start by getting the schema root node
	cur_schema_node = get_schema_root_element(schema);
	
	// loop through the list from root node on
	for (cur_family = ancestry; cur_family; cur_family = cur_family->next)
	{		
		// get the schema type
		cur_schema_node = get_schema_type(schema, cur_schema_node);
		
		// create xpath string
		if (!(xpath_exp = create_string(XPATH_SCHEMA_NODE, cur_family->data, cur_family->data, cur_family->data)))
		{
			ERROR("Could not create string for searching element and attribute nodes", true);
			free_list(&ancestry, xmlFreeString_wrapper);
			return NULL;
		}
		
		if (!(xpath_obj = get_xpath_result(xpath_exp, schema->xpath_ctxt, cur_schema_node)))
		{
			ERROR("Could not get schema node with xpath", true);
			free_list(&ancestry, xmlFreeString_wrapper);
			xmlFree(xpath_exp);
			return NULL;
		}
		
		// expression no longer needed
		xmlFree(xpath_exp);
		
		// check if found
		if (xpath_obj->nodesetval->nodeNr > 0)
		{
			cur_schema_node = xpath_obj->nodesetval->nodeTab[0];
			xmlXPathFreeObject(xpath_obj);
		}
		else // if not, check group
		{
			// free object since no result
			xmlXPathFreeObject(xpath_obj);
			
			// search for groups
			if (!(xpath_exp = create_string(XPATH_GROUP_CHILDREN)))
			{
				ERROR("Could not create string for search groups", true);
				free_list(&ancestry, xmlFreeString_wrapper);
				return NULL;
			}
			
			if (!(xpath_obj = get_xpath_result(xpath_exp, schema->xpath_ctxt, cur_schema_node)))
			{
				ERROR("Could not get group node with xpath", true);
				free_list(&ancestry, xmlFreeString_wrapper);
				xmlFree(xpath_exp);
				return NULL;
			}
			
			// expression no longer needed
			xmlFree(xpath_exp);
			
			// check if there are results
			if (xpath_obj->nodesetval->nodeNr < 1)
			{
				ERROR("Could not find any groups", true);
				free_list(&ancestry, xmlFreeString_wrapper);
				xmlFree(xpath_exp);
				xmlXPathFreeObject(xpath_obj);
				return NULL;				
			}
			
			// loop through the groups
			for (i = 0; i < xpath_obj->nodesetval->nodeNr; i++)
			{
				if (!(group_node = get_group(xpath_obj->nodesetval->nodeTab[i], schema)))
				{
					ERROR("Could not get group by reference", true);
					free_list(&ancestry, xmlFreeString_wrapper);
					xmlFree(xpath_exp);
					xmlXPathFreeObject(xpath_obj);
					return NULL;
				}
				
				// get the schema type from group
				cur_schema_node = get_schema_type(schema, group_node);
				
				// create xpath string
				if (!(xpath_exp = create_string(XPATH_SCHEMA_NODE, cur_family->data, cur_family->data, cur_family->data)))
				{
					ERROR("Could not create string for searching element and attribute nodes", true);
					free_list(&ancestry, xmlFreeString_wrapper);
					xmlXPathFreeObject(xpath_obj);
					return NULL;				
				}
				
				// get nodes within type within group
				if(!(xpath_obj_group = get_xpath_result(xpath_exp, schema->xpath_ctxt, cur_schema_node)))
				{
					ERROR("Could not get schema node with xpath in group", true);
					free_list(&ancestry, xmlFreeString_wrapper);
					xmlFree(xpath_exp);
					xmlXPathFreeObject(xpath_obj);
					return NULL;					
				}
				
				// expression no longer needed
				xmlFree(xpath_exp);
				
				// add the node found and interrupt loop
				if (xpath_obj_group->nodesetval->nodeNr > 0)
				{
					cur_schema_node = xpath_obj_group->nodesetval->nodeTab[0];
					xmlXPathFreeObject(xpath_obj);
					xmlXPathFreeObject(xpath_obj_group);
					break;
				}
				else
				{
					// free object for reuse
					xmlXPathFreeObject(xpath_obj_group);
				}
			}
			
			// in case the node has not been found, send error message and clean up
			if (i == xpath_obj->nodesetval->nodeNr)
			{
				ERROR("There is no node with the name '%s'", cur_family->data);
				xmlXPathFreeObject(xpath_obj);
				free_list(&ancestry, xmlFreeString_wrapper);
				return NULL;
			}
		}
	}
	
	free_list(&ancestry, xmlFreeString_wrapper);
	
	return cur_schema_node;
}

static xmlNodePtr get_external_schema_type(Schema_Obj *schema, xmlChar *name)
{
	xmlChar						*xpath_exp = NULL;

	xmlXPathObjectPtr	xpath_obj = NULL;

	xmlNodePtr				schema_type = NULL;
	
	// create xpath string
	if (!(xpath_exp = create_string(XPATH_TYPE_BY_NAME, name, name)))
	{
		ERROR("Could not create string", true);
		return NULL;
	}
	
	// get type nodes
	if (!(xpath_obj = get_xpath_result(xpath_exp, schema->xpath_ctxt, NULL)))
	{
		ERROR("Could not find schema type element '%s' with xpath", name);
	}
	else if (xpath_obj->nodesetval->nodeNr > 0) // and add if found
	{
		schema_type = xpath_obj->nodesetval->nodeTab[0];
	}
	
	// free xpath expression and object
	xmlFree(xpath_exp);
	xmlXPathFreeObject(xpath_obj);
	
	return schema_type;
}

static xmlNodePtr get_schema_type(Schema_Obj *schema, xmlNodePtr schema_element)
{
	xmlChar						*type_attr = NULL,
										*ref = NULL;
	
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	xmlNodePtr				schema_type = NULL;
	
	// check type and base attributes
	if ((type_attr = xmlGetProp(schema_element, "type")) || (type_attr = xmlGetProp(schema_element, "base")) || (type_attr = xmlGetProp(schema_element, "itemType")))
	{
		// check if it's an in-built type
		if (strncmp(type_attr, "xs:", 3) == 0)
		{
			schema_type = schema_element;
		}
		else // or a separate type definition
		{
			schema_type = get_external_schema_type(schema, type_attr);
		}		
	}
	else
	{
		// if no type attribute try to get type child node
		if (!(xpath_obj = get_xpath_result(XPATH_TYPE, schema->xpath_ctxt, schema_element)))
		{
			ERROR("Could not get type child node", true);
			goto cleanup;
		}
		
		// check if there is a child element and if yes, assign the schema type node found
		if (xpath_obj->nodesetval->nodeNr > 0) schema_type = xpath_obj->nodesetval->nodeTab[0];		
	}

	cleanup:
		if (type_attr) xmlFree(type_attr);
		if (xpath_obj) xmlXPathFreeObject(xpath_obj);
		return schema_type;
}

static xmlNodePtr get_group(xmlNodePtr schema_group, Schema_Obj *schema)
{
	xmlNodePtr				group_node = NULL;
	
	xmlChar						*ref = NULL,
										*xpath_exp = NULL;
	
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	// find the referenced group description
	if (!(ref = xmlGetProp(schema_group, "ref")))
	{
		ERROR("Could not get reference to group description", true);
		goto cleanup;
	}
	
	if (!(xpath_exp = create_string(XPATH_GROUP_BY_REF, ref)))
	{
		ERROR("Could not create string", true);
		goto cleanup;
	}

	if (!(xpath_obj = get_xpath_result(xpath_exp, schema->xpath_ctxt, NULL)) && (xpath_obj->nodesetval->nodeNr < 1))
	{
		ERROR("Could not get attribute reference '%s'", ref);
		goto cleanup;
	}
	
	if (xpath_obj->nodesetval->nodeNr < 1)
	{
		ERROR("Group '%s' could not be found", ref);
		goto cleanup;
	}
	
	group_node = xpath_obj->nodesetval->nodeTab[0];
	
	cleanup:
		xmlFree(ref);
		xmlFree(xpath_exp);
		xmlXPathFreeObject(xpath_obj);
		return group_node;
}

static void get_bounds(int *min, int *max, xmlNodePtr schema_node)
{
	xmlChar	*min_char,
					*max_char;
	
	// get the minimum occurrences
	if (!(min_char = xmlGetProp(schema_node, BAD_CAST "minOccurs")))
	{
		*min = 1;
	}
	else
	{
		*min = atoi(min_char);
		xmlFree(min_char);
	}
	
	// get the maximum occurrences
	if (!(max_char = xmlGetProp(schema_node, BAD_CAST "maxOccurs")))
	{
		*max = 1;
	}
	else 
	{
		if (EQUALS(max_char, "unbounded"))
		{
			*max = -1;
		}
		else
		{
			*max = atoi(max_char);
		}
		xmlFree(max_char);
	}
}

static Result *get_order_indicator_nodes(Communicator *com, xmlNodePtr schema_order_indicator, xmlNodePtr xml_parent_node)
{
	xmlXPathObjectPtr	xpath_schema = NULL,
										xpath_xml = NULL;

	xmlChar						*name = NULL;

	int								i,
										j;

	Result						*result = NULL;
	
	// get all child schema elements
	if (!(xpath_schema = get_xpath_result(XPATH_ELEM_CHILDREN, com->schema->xpath_ctxt, schema_order_indicator)))
	{
		ERROR("Could not get schema elements via xpath", true);
		return NULL;
	}
	
	// create the result object
	if (!(result = malloc(sizeof(Result))))
	{
		ERROR("Could not allocate memory for result", true);
		xmlXPathFreeObject(xpath_schema);
		return NULL;
	}
	
	// initialize members
	result->nodes = NULL;
	result->count = 0;
	
	// loop the choice nodes
	for (i = 0; i < xpath_schema->nodesetval->nodeNr; i++)
	{
		// get the name of the element
		if (!(name = xmlGetProp(xpath_schema->nodesetval->nodeTab[i], "name")))
		{
			ERROR("Could not get name attribute", true);
			xmlXPathFreeObject(xpath_schema);
			free(result->nodes);
			free(result);
			return NULL;
		}

		// search existing nodes of that name in the XML
		if (!(xpath_xml = get_xpath_result(name, com->xml->xpath_ctxt, xml_parent_node)))
		{
			ERROR("Could not get XML nodes", true);
			xmlXPathFreeObject(xpath_schema);
			xmlFree(name);
			free(result->nodes);
			free(result);
			return NULL;
		}
		
		for (j = 0; j < xpath_xml->nodesetval->nodeNr; j++)
		{			
			// make more space for the next node
			if (!(result->nodes = realloc(result->nodes, (sizeof(xmlNodePtr) * (result->count + 1)))))
			{
				ERROR("Could not allocate memory for node pointer", true);
				xmlXPathFreeObject(xpath_schema);
				xmlFree(name);
				free(result->nodes);
				free(result);
				return NULL;
			}
			
			// add the node found
			result->nodes[result->count] = xpath_xml->nodesetval->nodeTab[j];
			
			// increment counter
			result->count++;
		}
		
		// free XML xpath and name
		xmlXPathFreeObject(xpath_xml);
		xmlFree(name);
	}
	
	// free schema xpath
	xmlXPathFreeObject(xpath_schema);
	
	return result;
}

static xmlNodePtr get_referenced_node(xmlChar *ref, Schema_Obj *schema)
{
	xmlNodePtr				schema_attribute = NULL;
	
	xmlChar						*xpath_exp = NULL;
	
	xmlXPathObjectPtr xpath_obj = NULL;
	
	if (!(xpath_exp = create_string(XPATH_ATTR_BY_NAME, ref)))
	{
		ERROR("Could not create string", true);
		return NULL;
	}
	
	if (!(xpath_obj = get_xpath_result(xpath_exp, schema->xpath_ctxt, NULL)) && (xpath_obj->nodesetval->nodeNr < 1))
	{
		ERROR("Could not get attribute reference '%s'", ref);
		xmlFree(xpath_exp);
		return NULL;
	}
	
	schema_attribute = xpath_obj->nodesetval->nodeTab[0];
	
	xmlFree(xpath_exp);	
	xmlXPathFreeObject(xpath_obj);
	
	return schema_attribute;
}

static void get_annotations(xmlNodePtr schema_element, xmlNodePtr xml_node, Communicator *com)
{
	xmlChar 					*name = NULL,
										*content = NULL,
										*lang = NULL;
	
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	xmlNodePtr				annotation,
										cur,
										text_node,
										child_node;
	
	// get name property of schema element 
	name = xmlGetProp(schema_element, BAD_CAST "name");
	
	if ((xpath_obj = get_xpath_result(XPATH_ANNOTATION, com->schema->xpath_ctxt, schema_element)) && xpath_obj->nodesetval && xpath_obj->nodesetval->nodeNr > 0)
	{
		if (xpath_obj->nodesetval->nodeNr == 1)
		{
			// create annotation node
			annotation = xmlNewNode(NULL, "annotation");
			
			// assign template namespace
			create_template_ns(com, annotation);

			// add schema parent node name as attribute
			xmlSetProp(annotation, BAD_CAST "schema", schema_element->name);
			
			// add schema parent node name attribute as attribute
			if (name) xmlSetProp(annotation, BAD_CAST "name", name);
			
			// add annotation
			xmlAddChild(xml_node, annotation);
			
			for (cur = xpath_obj->nodesetval->nodeTab[0]->children; cur; cur = cur->next)
			{
				if (cur->type == XML_ELEMENT_NODE)
				{
					// get annotation child content
					content = xmlNodeGetContent(cur);
					
					// create new text node with content
					text_node = xmlNewText(content);
					
					xmlFree(content);
					
					// create new annotation child node
					child_node = xmlNewNode(NULL, cur->name);
					
					// add language attribute
					if (EQUALS(cur->name,"documentation"))
					{
						lang = xmlGetProp(cur, "lang");
						if (lang)
						{
							xmlSetProp(child_node, BAD_CAST "lang", lang);
							xmlFree(lang);
						}
					}
					
					// assign template namespace
					create_template_ns(com, child_node);
					
					// add text node
					xmlAddChild(child_node, text_node);
					
					// add annotation child node
					xmlAddChild(annotation, child_node);
				}
			}
		}
		else
		{
			ERROR("%i annotations found. Not valid, check your XSD!", xpath_obj->nodesetval->nodeNr);
		}
		xmlXPathFreeObject(xpath_obj);		
	}
	xmlFree(name);
}

static xmlChar *get_builtin_type(xmlNodePtr schema_restriction, Schema_Obj *schema)
{
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	xmlNodePtr				type_child,
										schema_node;
	
	xmlChar						*builtin_type = NULL;
	
	// get type
	schema_node = get_schema_type(schema, schema_restriction);
	
	// loop while still simple type
	while (EQUALS(schema_node->name, "simpleType"))
	{
		if (!(xpath_obj = get_xpath_result(XPATH_TYPE_CHILD, schema->xpath_ctxt, schema_node)) && xpath_obj->nodesetval->nodeNr > 0)
		{
			ERROR("Could not get type attribute", true);
			return NULL;
		}
		
		type_child = xpath_obj->nodesetval->nodeTab[0];
		
		// reassign type
		schema_node = get_schema_type(schema, type_child);
		
		// free xpath object
		xmlXPathFreeObject(xpath_obj);
	}
	
	// check for built-in type string of simple type child element (either restriction or list)
	if (!(builtin_type = xmlGetProp(schema_node, "base")) && !(builtin_type = xmlGetProp(schema_node, "itemType")))
	{
		ERROR("Could not get built-in type", true);
		return NULL;
	}
	
	return builtin_type;
}

/* GET FUNCTIONS END */

/* CREATE FUNCTIONS */

static void create(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_node, Create_function create_function)
{
	// check if node is not deleted in undo
	if (!(check_continue(com, undo_coll, xml_node))) return;
	
	create_function(undo_coll, unique_head, com, schema_element, xml_node);	
}

static void create_fixed_default(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_node, xmlNodePtr xml_element)
{
	xmlChar				*text = NULL,
								*desc_text = NULL;
	
	xmlNodePtr		text_node = NULL;
	
	Request				*default_request = NULL;
	
	Answer				*default_answer = NULL;
	
	// delete all old text
	remove_all_child_nodes(xml_element);
	
	// check if fixed value
	if (text = xmlGetProp(schema_node, "fixed"))
	{
		// create the text node
		text_node = xmlNewText(text);
		xmlAddChild(xml_element, text_node);

		// confirm creation
		confirm_execution(com, CREATE_CONF, SIMPLE_TYPE, text_node);
	}
	else if (text = xmlGetProp(schema_node, "default")) // or if default
	{
		// get right node type string
		switch (xml_element->type)
		{
			case XML_ELEMENT_NODE:
			default:
				desc_text = "element";
				break;
			case XML_ATTRIBUTE_NODE:
				desc_text = "attribute";
				break; 
		}
		
		// create request
		if (!(default_request = create_data_request(CONFIRM_REQ, DEFAULT, NO_REFUSE, xmlStrdup(xml_element->name), xmlStrdup(desc_text), xmlStrdup(text)))) goto cleanup;
		
		// get user data
		if (!(default_answer = make_create_request_with_check(com, default_request, undo_coll, unique_head, &create_fixed_default, schema_node, xml_element))) goto cleanup;	
		
		if (default_answer->acceptance == ACCEPT)
		{
			// create the text node
			text_node = xmlNewText(text);
			xmlAddChild(xml_element, text_node);
			
			// save an undo indicating that user decided whether or not to accept the dafault value
			add_undo(undo_coll, unique_head, com, &create_fixed_default, schema_node, xml_element, text_node);
			
			// confirm creation
			confirm_execution(com, CREATE_CONF, SIMPLE_TYPE, text_node);
		}
		else // if no accept proceed and go to content
		{			
			// save an undo indicating that user decided whether or not to accept the dafault value
			add_undo(undo_coll, unique_head, com, &create_fixed_default, schema_node, xml_element, NULL);
			
			// create the content
			create(undo_coll, unique_head, com, schema_node, xml_element, &create_content);
		}
	}
	else
	{
		// create the content
		create(undo_coll, unique_head, com, schema_node, xml_element, &create_content);		
	}

	cleanup:
		if (text) xmlFree(text);
		if (default_request) free_request(default_request);
		if (default_answer) free_answer(default_answer);
}

static void create_attribute(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_attribute, xmlNodePtr xml_element)
{
	xmlChar						*name = NULL,
										*ref = NULL,
										*use = NULL,
										*old_attr = NULL;
	
	Request						*request = NULL;
	
	Answer						*answer = NULL;
	
	xmlNodePtr				attribute = NULL,
										text_node = NULL;

	// check name and reference
	name = xmlGetProp(schema_attribute, "name");
	ref = xmlGetProp(schema_attribute, "ref");
	
	// make the reference the name if exists
	if (ref)
	{
		name = xmlStrdup(ref);
	}
	
	// check the existence and ask user if to be replaced
	if (old_attr = xmlGetProp(xml_element, name))
	{
		if (!(request = create_data_request(CONFIRM_REQ, REPLACE_NODE, NO_REFUSE, xmlStrdup(name), create_string("attribute"), NULL))) goto cleanup;
		
		if (!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_attribute, schema_attribute, xml_element))) goto cleanup;
		
		if (answer->acceptance == REFUSE)
		{
			// save an undo indicating that user decided not to create the attribute
			add_undo(undo_coll, unique_head, com, &create_attribute, schema_attribute, xml_element, NULL);
			
			goto cleanup;
		}						
	}
	
	// get use
	use = xmlGetProp(schema_attribute, "use");
	
	// interrupt if use is prohibited
	if (use && EQUALS(use, "prohibited")) goto cleanup;
	
	// if no use is specified or use is optional
	if ((!use) || EQUALS(use, "optional"))
	{		
		if (!(request = create_data_request(CONFIRM_REQ, CREATE_NODE, NO_REFUSE, xmlStrdup(name), create_string("attribute"), NULL))) goto cleanup;
		
		if (!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_attribute, schema_attribute, xml_element))) goto cleanup;
		
		if (answer->acceptance == REFUSE)
		{
			// save an undo indicating that user decided not to create the attribute
			add_undo(undo_coll, unique_head, com, &create_attribute, schema_attribute, xml_element, NULL);
			
			goto cleanup;
		}
	}
	
	// create the attribute node
	if (!(attribute = xmlNewNode(NULL, name)))
	{
		ERROR("Cannot create attribute '%s'", name);
		goto cleanup;
	}
	
	// set node to be an attribute
	attribute->type = 2;
	
	// add the attribute
	xmlAddChild(xml_element, attribute);
	
	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_attribute, xml_element, com);
	
	// add to undo list if wanted by user
	if (answer) add_undo(undo_coll, unique_head, com, &create_attribute, schema_attribute, xml_element, attribute);
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, ATTRIBUTE, attribute);

	// create fixed and default content
	create(undo_coll, unique_head, com, schema_attribute, attribute, &create_fixed_default);
	
	cleanup:
		if (name) xmlFree(name);
		if (ref) xmlFree(ref);
		if (use) xmlFree(use);
		if (old_attr) xmlFree(old_attr);
		if (answer) free_answer(answer);
}

typedef struct _Element_Block {
	xmlNodePtr *nodes;
	int length;
} Element_Block; // HIER WEITER

static int get_element_occurrences(Communicator *com, xmlNodePtr schema_node, xmlNodePtr xml_element)
{
	/*
		TODO: überall dort, wo ein früherer sich nach einem späteren befindet, ist ein neuer block...
		1) go to next (or first) XML element
		2) compare with following one until end is reached
		3) if following is same: add to bounds and go to 1)
		4) if following is later: check bounds and choice or sequence bounds
		5) if within bounds, go to 1)
		6) if out of bounds, count += 1, go to 1)
		7) if following is earlier: check if part of same occurrence indicator
		8) if part of choice or sequence, add to choice or sequence bounds, go to 1)
		9) if not part of choice or sequence, count += 1, go to 1)
		
		TODO: bei element checks die übergeordneten occurs miteinbeziehen, bis hin zum nächsthöheren complexType
		
		HIER WEITER 1) check if element or order indicator definition, 2) needs to return not only block number, but blocks, 3) needs to get block as parameter
	*/
	
	xmlXPathObjectPtr	schema_elements_obj = NULL,
										xml_element_children = NULL;
	
	int								i,
										j,
										number = 0,
										cur_position,
										next_position,
										e_occurs = 1,
										oi_occurs = 1,
										min,
										max,
										e_len;
	
	xmlChar						*element_name = NULL;
	
	xmlNodePtr				cur_element,
										next_element,
										cur_parent,
										next_parent;

	// get all child elements of the xml element given
	if (!(xml_element_children = get_xpath_result(XPATH_ALL_CHILDREN, com->xml->xpath_ctxt, xml_element)))
	{
		ERROR("Could not query child elements of '%s'", xml_element->name);
		goto cleanup;
	}

	// get element definitions of schema node given
	if (!(schema_elements_obj = get_xpath_result(XPATH_ELEM_CHILDREN, com->schema->xpath_ctxt, schema_node)))
	{
		ERROR("Could not query schema element definitions", true);
		goto cleanup;
	}
	
	// assign number of XML child elements found
	e_len = xml_element_children->nodesetval->nodeNr;
	
	// increment if not 0 since at least 1 occurrence of a "block" found
	if (e_len > 0) number++; 
	
	// loop XML child elements
	for (i = 0; i < e_len; i++)
	{
		// reset positions
		cur_position = -1;
		next_position = -1;
		
		// get current element 1)
		cur_element = xml_element_children->nodesetval->nodeTab[i];
		
		// skip if is last
		if (i == (e_len - 1)) continue;
		
		// get next element
		next_element = xml_element_children->nodesetval->nodeTab[i + 1];
		
		// get schema element definition position number of current and next elements
		for (j = 0; j < schema_elements_obj->nodesetval->nodeNr; j++)
		{
			// get name from definition
			element_name = xmlGetProp(schema_elements_obj->nodesetval->nodeTab[j], "name");
			
			// assign
			if (EQUALS(cur_element->name, element_name)) cur_position = j;
			if (EQUALS(next_element->name, element_name)) next_position = j;
			
			// free it again
			xmlFree(element_name);
		}
		
		// check if assigned and skip rest if not
		if (cur_position == -1 || next_position == -1) continue;
		
		// get parent nodes
		cur_parent = schema_elements_obj->nodesetval->nodeTab[cur_position]->parent;
		next_parent = schema_elements_obj->nodesetval->nodeTab[next_position]->parent;
		
		// compare the definitions 2)
		if (cur_position == next_position) // if same element definition
		{
			// increment element occurs 3)
			e_occurs++;
		}
		else if (cur_position < next_position) // if current definition before next
		{
			// get bounds of current element
			get_bounds(&min, &max, schema_elements_obj->nodesetval->nodeTab[cur_position]);
			
			// if out of bounds 4, 5, 6)
			if (max != -1 && e_occurs > max)
			{
				number++;	// increment "block" number by 1
			}

			// get bounds of current order indicator parent
			get_bounds(&min, &max, cur_parent);
			
			// otherwise check parent order indicator occurs
			if (max != -1 && oi_occurs > max)
			{
				number++;	// increment "block" number by 1
			}

			// reset counters
			e_occurs = 1;
			oi_occurs = 1;
		}
		else if (cur_position > next_position) // if current definition after next
		{
			if (cur_parent == next_parent) // 7)
			{
				oi_occurs++; // increment parent order indicator occurs
			}
			else
			{
				number++; // increment "block" number by 1
			}
		}
	}

	cleanup:
		xmlXPathFreeObject(schema_elements_obj);
		xmlXPathFreeObject(xml_element_children);		
		return number;
}

static void create_element(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_element) // TODO: ref
{
	int								min,
										max,
										element_count = 0;
										
	xmlNodePtr				tmp = NULL,
										new_node = NULL;
										
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	xmlChar						*element_name = NULL;
										
	Request						*request = NULL;
	
	Answer 						*answer = NULL;
	
	bool							once_created = false;
	
	if (!(element_name = xmlGetProp(schema_element, "name")))
	{
		ERROR("Could not get element name", true);
		return;
	}
	
	// get bounds
	get_bounds(&min, &max, schema_element);
	
	// create within bounds	and if not undone
	while ((tmp == NULL || check_continue(com, undo_coll, tmp)) && !(once_created))
	{
		// get existing elements (having same name)
		if (!(xpath_obj = get_xpath_result(element_name, com->xml->xpath_ctxt, xml_element)))
		{
			ERROR("Could not query elements", true);
			goto cleanup;
		}	
		
		// get element count
		element_count = xpath_obj->nodesetval->nodeNr;
		
		// reset XPath object
		xmlXPathFreeObject(xpath_obj);
		xpath_obj = NULL;
		
		// interrupt if maximum is reached
		if (max != -1 && element_count >= max)
		{
			goto cleanup;
		}
		
		// in case creation is optional, ask user
		if (element_count >= min)
		{
			if (!(request = create_data_request(CONFIRM_REQ, CREATE_NODE, NO_REFUSE, xmlStrdup(element_name), create_string("element"), NULL))) goto cleanup;
			
			// get user data
			if (!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_element, schema_element, xml_element))) goto cleanup;
			
			// check if only one element is demanded irrespective of bounds for testing purposes
			if (answer->acceptance == ACCEPT_ONCE)
			{
				once_created = true;
			}
			else if (answer->acceptance == REFUSE)
			{
				// save an undo indicating that user decided not to create the element
				add_undo(undo_coll, unique_head, com, &create_element, schema_element, xml_element, NULL);
				
				goto cleanup;
			}
		}
		
		// create the new element
		if (!(new_node = xmlNewNode(NULL, element_name)))
		{
			ERROR("Cannot create element '%s'", element_name);
			goto cleanup;
		}
		
		// add new element to parent element
		xmlAddChild(xml_element, new_node);
		
		// save new node temporarily for later undo check
		tmp = new_node;
		
		// add to undo list if wanted by user
		if (answer) add_undo(undo_coll, unique_head, com, &create_element, schema_element, xml_element, new_node);
		
		// confirm creation
		confirm_execution(com, CREATE_CONF, ELEMENT, new_node);
		
		// reset answer for loop
		xmlFree(answer);
		answer = NULL;
		
		// create fixed and default content
		create(undo_coll, unique_head, com, schema_element, new_node, &create_fixed_default); // and start the recursion
		
		// add annotations in case of template
		if (com->type == TEMPLATE) get_annotations(schema_element, new_node, com);
	}
	
	cleanup:
		if (element_name) xmlFree(element_name);
		if (xpath_obj) xmlXPathFreeObject(xpath_obj);
		if (answer) free_answer(answer);
}

static void create_order_indicator_child(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_indicator_child, xmlNodePtr xml_element)
{
	if (EQUALS(schema_indicator_child->name, "element"))
	{
		create(undo_coll, unique_head, com, schema_indicator_child, xml_element, &create_element);
	}
	
	if (EQUALS(schema_indicator_child->name, "sequence"))
	{
		create(undo_coll, unique_head, com, schema_indicator_child, xml_element, create_sequence);
	}
	
	if (EQUALS(schema_indicator_child->name, "choice"))
	{
		create(undo_coll, unique_head, com, schema_indicator_child, xml_element, create_choice);
	}
	
	if (EQUALS(schema_indicator_child->name, "group"))
	{
		create(undo_coll, unique_head, com, schema_indicator_child, xml_element, create_group);
	}
	
	// TODO: any
}

static void create_sequence(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_sequence, xmlNodePtr xml_element) // TODO implement Occurs
{
	xmlNodePtr				child,
										previous_node = NULL;
	
	Request						*request = NULL;
	
	xmlXPathObjectPtr	xpath_schema = NULL,
										xpath_xml = NULL;
	
	int								i,
										j;
	
	xmlChar						*name = NULL;
	
	get_element_occurrences(com, schema_sequence, xml_element); // HIER WEITER for testing
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, SEQUENCE, NULL);
	
	// go through all child elements
	for (child = schema_sequence->children; child; child = child->next)
	{
		create(undo_coll, unique_head, com, child, xml_element, create_order_indicator_child);
	}
	
	// get elements added later into correct order
	
	// get all child schema elements
	if (!(xpath_schema = get_xpath_result(XPATH_ELEM_CHILDREN, com->schema->xpath_ctxt, schema_sequence)))
	{
		ERROR("Could not get schema elements via xpath", true);
		return;
	}
	
	// loop child element description of sequence
	for (i = 0; i < xpath_schema->nodesetval->nodeNr; i++)
	{		
		
		if (!(name = xmlGetProp(xpath_schema->nodesetval->nodeTab[i], "name")))
		{
			ERROR("Could not get element name via xpath", true);
			xmlXPathFreeObject(xpath_schema);
			return;
		}
		
		// find children in XML with that name
		if (!(xpath_xml = get_xpath_result(name, com->xml->xpath_ctxt, xml_element)))
		{
			ERROR("Could not get xml elements via xpath", true);
			xmlXPathFreeObject(xpath_schema);
			xmlFree(name);
			return;
		}
		
		// add same name node after each other
		for (j = 0; j < xpath_xml->nodesetval->nodeNr; j++)
		{
			if (previous_node == NULL)
			{
				previous_node = xpath_xml->nodesetval->nodeTab[j];
			}
			else
			{
				previous_node = xmlAddNextSibling(previous_node, xpath_xml->nodesetval->nodeTab[j]);
			}
		}
		
		xmlXPathFreeObject(xpath_xml);
		xmlFree(name);
	}
	
	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_sequence, xml_element, com);
	
	xmlXPathFreeObject(xpath_schema);
}

static void create_choice(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_choice, xmlNodePtr xml_element) // TODO implement Occurs
{
	xmlNodePtr				chosen_node = NULL,
										next_sibling = NULL;
	
	xmlChar						*name = NULL,
										*choice_text = NULL,
										*chosen_node_name = NULL,
										*old_node_names = NULL;										
										
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	int								choice_nr,
										i,
										chosen_index;
										
	Request						*request = NULL,
										*confirm_request = NULL;
	
	Answer						*answer = NULL;

	Result						*choice_nodes = NULL;
	
	get_element_occurrences(com, schema_choice, xml_element); // HIER WEITER for testing
	
	// before creating the choice, look for existing nodes
	choice_nodes = get_order_indicator_nodes(com, schema_choice, xml_element);
	
	if (choice_nodes && choice_nodes->count > 0)
	{
		for (i = 0; i < choice_nodes->count; i++)
		{
			old_node_names = concat_string(old_node_names, choice_nodes->nodes[i]->name);
			
			if (i < choice_nodes->count - 1)
			{
				old_node_names = concat_string(old_node_names, ", ");
			}
		}
		
		// ask if to delete
		if (!(request = create_data_request(CONFIRM_REQ, REPLACE_NODE, NO_REFUSE, old_node_names, create_string("choice"), NULL))) goto cleanup;
		
		// get user data
		if (!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_choice, schema_choice, xml_element))) goto cleanup;
		
		if (answer->acceptance == REFUSE)
		{
			// save an undo indicating that user decided not to create the nodes
			add_undo(undo_coll, unique_head, com, &create_choice, schema_choice, xml_element, NULL);
			
			goto cleanup;
		}
		else // if user accepts, delete the nodes
		{
			for (i = 0; i < choice_nodes->count; i++)
			{
				// remember the next sibling of the last unlinked node
				if (i == (choice_nodes->count - 1))
				{
					next_sibling = choice_nodes->nodes[i]->next;
				}
				
				// unlink and remember in undo collection
				xmlUnlinkNode(choice_nodes->nodes[i]);
				push_list(&(undo_coll->unlinked_head), choice_nodes->nodes[i]);
			}
		}
		
		free(choice_nodes->nodes);
		free(choice_nodes);
	}
	
	// get all choices 
	if (!(xpath_obj = get_xpath_result(XPATH_CHOICES, com->schema->xpath_ctxt, schema_choice))) // TODO: any
	{
		ERROR("Could not get child elements via xpath", true);
		goto cleanup;
	}
	
	// get the number of choices and check that there is at least one
	if ((choice_nr = xpath_obj->nodesetval->nodeNr) > 0)
	{
		// prepare a choice request object
		if (!(request = create_choice_request(CHOOSE_NODE, NO_REFUSE, xmlStrdup(xml_element->name)))) goto cleanup;
		
		// add the choices
		for (i = 0; i < choice_nr; i++)
		{
			// check if the choosable node has a name attribute, add it if yes
			if ((name = xmlGetProp(xpath_obj->nodesetval->nodeTab[i], "name")) || (name = xmlGetProp(xpath_obj->nodesetval->nodeTab[i], "id")))
			{
				add_choice(request, name);
			}
			else
			{
				add_choice(request, xmlStrdup(xpath_obj->nodesetval->nodeTab[i]->name));
			}
		}
		
		// get user data
		if (!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_choice, schema_choice, xml_element))) goto cleanup;
		
		// add to undo list
		add_undo(undo_coll, unique_head, com, &create_choice, schema_choice, xml_element, NULL);

		// get chosen index
		chosen_index = answer->chosen_index;

		// check if index is valid
		if (chosen_index < -1 || chosen_index >= choice_nr)
		{
			ERROR("Chosen node is out of bound", true);
			goto cleanup;
		}
		
		if (chosen_index == -1)
		{
			WARNING("Choice is -1: All choosable elements are created. The resulting XML file will NOT be valid!", true);
			
			// create choice node
			xmlNodePtr choice_node = xmlNewNode(NULL, "choice");
			
			// add template namespace
			create_template_ns(com, choice_node);
			
			// add to current element
			xmlAddChild(xml_element, choice_node);
			
			// loop options
			for (i = 0; i < choice_nr; i++)
			{
				// get the chosen node by index
				chosen_node = xpath_obj->nodesetval->nodeTab[i];
				
				// confirm creation
				confirm_execution(com, CREATE_CONF, CHOICE, NULL);
				
				// create the child node
				create(undo_coll, unique_head, com, chosen_node, choice_node, create_order_indicator_child);
			}
		}
		else
		{
			// get the chosen node by index
			chosen_node = xpath_obj->nodesetval->nodeTab[chosen_index];	
	
			// confirm creation
			confirm_execution(com, CREATE_CONF, CHOICE, NULL);
			
			// create the child node
			create(undo_coll, unique_head, com, chosen_node, xml_element, create_order_indicator_child);
		}
		
		// replace if there was a next sibling
		if (next_sibling)
		{
			choice_nodes = get_order_indicator_nodes(com, schema_choice, xml_element);
			
			if (choice_nodes && choice_nodes->count > 0)
			{
				for (i = 0; i < choice_nodes->count; i++)
				{
					xmlAddPrevSibling(next_sibling, choice_nodes->nodes[i]);
				}
			}
		}
	}
	
	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_choice, xml_element, com);
	
	cleanup:
		xmlXPathFreeObject(xpath_obj);
		free_answer(answer);
}

static void create_group(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_group, xmlNodePtr xml_element)
{
	xmlNodePtr				group_node,
										child;
	
	Request						*request = NULL;
	
	// find by reference
	group_node = get_group(schema_group, com->schema);
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, GROUP, NULL);
	
	// stop if no group
	if (group_node == NULL)
	{
		return;
	}
	
	// go through all child elements
	for (child = group_node->children; child; child = child->next)
	{		
		if (EQUALS(child->name, "sequence"))
		{
			create(undo_coll, unique_head, com, child, xml_element, create_sequence);
		}
		
		if (EQUALS(child->name, "choice"))
		{
			create(undo_coll, unique_head, com, child, xml_element, create_choice);
		}	
		
		// TODO: all
	}

	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_group, xml_element, com);

	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(group_node, xml_element, com);
}

static void create_extension(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_extension, xmlNodePtr xml_element)
{
	xmlNodePtr	base_type;
	
	Request			*request = NULL;
	
	// get the base type	
	if (!(base_type = get_schema_type(com->schema, schema_extension)))
	{
		ERROR("Could not get base type", true);
		return;
	}
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, EXTENSION, NULL);
	
	// process the base
	create(undo_coll, unique_head, com, base_type, xml_element, create_schema_type); // HIER WEITER geht das???
	
	// process the extension
	create(undo_coll, unique_head, com, schema_extension, xml_element, create_complex_type);

	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_extension, xml_element, com);
}

static void create_enumeration_facet(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_restriction, xmlNodePtr xml_node)
{
	xmlXPathObjectPtr	xpath_obj = NULL;

	if (!(xpath_obj = get_xpath_result(XPATH_ENUM, com->schema->xpath_ctxt, schema_restriction)))
	{
		return;
	}
	
	int								enum_number = 0,
										chosen_index = 0,
										i;
	
	Request						*request = NULL;
	
	Answer						*answer = NULL;
	
	xmlChar						*value = NULL,
										*chosen_value = NULL;
	
	xmlNodePtr				text_node = NULL;
	
	Refuse_Type				refuse = NO_REFUSE;
	
	if ((enum_number = xpath_obj->nodesetval->nodeNr) == 0)
	{
		goto cleanup;
	}
	
	do
	{
		// set empty
		free_request(request);
		request = NULL;
		free_answer(answer);
		answer = NULL;
		
		// make choice request
		if (!(request = create_choice_request(CHOOSE_ENUMERATION, refuse, xmlStrdup(xml_node->name))))
		{
			ERROR("Could not create request", true);
			goto cleanup;
		}
		
		// add the choices
		for (i = 0; i < enum_number; i++)
		{
			if (!(value = xmlGetProp(xpath_obj->nodesetval->nodeTab[i], "value")))
			{
				ERROR("Could not get value attribute", true);
				goto cleanup;
			}
		
			// add each choice to the choices array
			add_choice(request, value);
		}
		
		// get user data
		if(!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_restriction, schema_restriction, xml_node))) goto cleanup;
		
		// get user choice index
		chosen_index = answer->chosen_index;
		
		// check if user choice is out of bound
		if (chosen_index < -1 || chosen_index > enum_number)
		{
			ERROR("Chosen enumeration is out of bound", true);
			goto cleanup;
		}
		
		// choose first if -1 is given
		if (chosen_index == -1)
		{
			xmlNodePtr	enum_node,
									enum_item;
			
			WARNING("Choice is -1: All texts from the enumeration are chosen. The resulting XML file will NOT be valid!", true);
			
			// create enumeration node
			enum_node = xmlNewNode(NULL, "enumeration");

			// add template namespace
			create_template_ns(com, enum_node);
			
			// check if it's an element
			if (xml_node->type == XML_ELEMENT_NODE)
			{
				// add type attribute
				xmlSetProp(enum_node, BAD_CAST "type", BAD_CAST "element");

				// add to current node
				xmlAddChild(xml_node, enum_node);
			}
			else if (xml_node->type == XML_ATTRIBUTE_NODE) // check if it's an attribute
			{
				// add type attribute
				xmlSetProp(enum_node, BAD_CAST "type", BAD_CAST "attribute");
				
				// add the name of the attribute of the parent element
				xmlSetProp(enum_node, BAD_CAST "attribute", xml_node->name);
				
				// add to parent element holding the attribute
				xmlAddChild(xml_node->parent, enum_node);
			}
			
			// loop all enumeration items
			for (i = 0; i < enum_number; i++)
			{
				if (!(chosen_value = xmlGetProp(xpath_obj->nodesetval->nodeTab[i], "value")))
				{
					ERROR("Could not get value attribute", true);
					goto cleanup;			
				}
				
				// create enumeration item
				enum_item = xmlNewNode(NULL, "item");
				
				// add annotations
				get_annotations(xpath_obj->nodesetval->nodeTab[i], enum_item, com);
				
				// add template namespace
				create_template_ns(com, enum_item);
				
				// create new text node
				text_node = xmlNewText(chosen_value);
				
				// add the text node
				xmlAddChild(enum_item, text_node);
				
				// add the enumeration item
				xmlAddChild(enum_node, enum_item);
				
				break;
			}	
		}
		else
		{
			if (!(chosen_value = xmlGetProp(xpath_obj->nodesetval->nodeTab[chosen_index], "value")))
			{
				ERROR("Could not get value attribute", true);
				goto cleanup;			
			}
			
			if (is_unique(*unique_head, chosen_value, xml_node))
			{
				// create new text node
				text_node = xmlNewText(chosen_value);
				xmlAddChild(xml_node, text_node);
			}
			else
			{
				xmlFree(chosen_value);
				chosen_value = NULL;
			}		
		}
		
		// set refuse
		refuse = NOT_UNIQUE;
		
	} while (!(text_node));
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, ENUMERATION, text_node);

	// add to undo list
	add_undo(undo_coll, unique_head, com, &create_restriction, schema_restriction, xml_node, text_node);
	
	cleanup:
		xmlXPathFreeObject(xpath_obj);
		free_answer(answer);
		xmlFree(chosen_value);
}

static void create_pattern_facet(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_restriction, xmlNodePtr xml_node)
{
	xmlXPathObjectPtr	xpath_obj = NULL;

	if (!(xpath_obj = get_xpath_result(XPATH_PATTERN, com->schema->xpath_ctxt, schema_restriction)))
	{
		return;
	}

	xmlChar						*value,
										*regex = NULL,
										*chosen_value = NULL;
	
	int								i,
										answer_len,
										matches;
										
	Refuse_Type				refuse = NO_REFUSE;
	
	Request						*request = NULL;
	
	Answer						*answer = NULL;
	
	xmlNodePtr				text_node;
	
	List_Node					*pattern_head = NULL;

	if (xpath_obj->nodesetval->nodeNr == 0)
	{
		goto cleanup;
	}	

	// get all regex
	for (i = 0; i < xpath_obj->nodesetval->nodeNr; i++)
	{
		if (!(value = xmlGetProp(xpath_obj->nodesetval->nodeTab[i], "value")))
		{
			ERROR("Could not get regex from value attribute", true);
			goto cleanup;
		}
	
		regex = concat_string(regex, value);
		
		if (i < (xpath_obj->nodesetval->nodeNr - 1))
		{
			regex = concat_string(regex, "|");
		}
		
		xmlFree(value);
	}
	
	while (!(chosen_value))
	{
		// create request
		if (!(request = create_data_request(STRING_REQ, PATTERN, refuse, xmlStrdup(xml_node->name), xmlStrdup(regex), xmlNodeGetContent(xml_node)))) goto cleanup;
		
		// get user data
		if(!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_restriction, schema_restriction, xml_node))) goto cleanup;
		
		// reset request
		request = NULL;
		
		// check if answer is null
		if (answer->data == NULL)
		{
			WARNING("Text submitted is null: The value assigned is the pattern regex. The resulting XML file will NOT be valid!", true);
			
			// create pattern node
			xmlNodePtr pattern_node = xmlNewNode(NULL, BAD_CAST "pattern");				
			
			// add template namespace
			create_template_ns(com, pattern_node);
			
			// add annotations
			get_annotations(xpath_obj->nodesetval->nodeTab[0], pattern_node, com);
			
			// assign regex itself as chosen value
			chosen_value = xmlStrdup(regex);
			
			// create new text node
			text_node = xmlNewText(chosen_value);
			
			// add text node
			xmlAddChild(pattern_node, text_node);
			
			// delete all old text
			remove_all_child_nodes(xml_node);
			
			// add pattern node
			xmlAddChild(xml_node, pattern_node);
		}
		else
		{
			// check uniqueness
			if (!(is_unique(*unique_head, answer->data, xml_node)))
			{
				refuse = NOT_UNIQUE;
			}
			else
			{
				// get length of string given by user
				answer_len = strlen(answer->data);
			
				// search pattern in string
				matches = searchInText(regex, answer->data, answer_len, &pattern_head);

				// check if error occurred
				if (matches < 0)
				{		
					ERROR("Error matching pattern", true);
					goto cleanup;
				}
			
				if (matches == 1)
				{
					Token *token = pattern_head->data;
					if (token->begin == 0 && token->end == answer_len)
					{
						// assign chosen value, loop will be interrupted
						chosen_value = xmlStrdup(answer->data);
						
						// delete all old text
						remove_all_child_nodes(xml_node);
	
						// create new text node
						text_node = xmlNewText(chosen_value);
						xmlAddChild(xml_node, text_node);
					}
				}
				
				// indicate no match for next iteration
				refuse = NO_MATCH;								
			}		
		}
		
		// reset old answer
		free_answer(answer);
		answer = NULL;

		// reset list
		free_list(&pattern_head, destroyToken);
		pattern_head = NULL;
	}
	
	// add to undo list
	add_undo(undo_coll, unique_head, com, &create_restriction, schema_restriction, xml_node, text_node);
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, PATTERN_CONTENT, text_node);
	
	cleanup:
		xmlXPathFreeObject(xpath_obj);
		free_list(&pattern_head, destroyToken);
		free_answer(answer);
		xmlFree(chosen_value);
		xmlFree(regex);		
}

static void create_bounds_facet(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_restriction, xmlNodePtr xml_element)
{
	xmlXPathObjectPtr	xpath_obj = NULL;

	if (!(xpath_obj = get_xpath_result(XPATH_BOUNDS, com->schema->xpath_ctxt, schema_restriction)))
	{
		return;
	}
	
	int 							i;
	
	xmlNodePtr				facet,
										text_node;
	
	xmlChar						*value = NULL,
										*desc_text = NULL,
										*builtin_type = NULL,
										*text = NULL;

	Request						*request = NULL;
	
	Answer						*answer = NULL;
	
	bool							correct;
	
	Action_Type				action_type;
	
	Refuse_Type				refuse = NO_REFUSE;
	
	Check_function		check_function;
	
	if (xpath_obj->nodesetval->nodeNr == 0)
	{
		goto cleanup;
	}
	
	// get builtin_type
	if (!(builtin_type = get_builtin_type(schema_restriction, com->schema))) goto cleanup;
	
	// initialize bounds
	Bounds bounds[2];
	
	// initialize to empty
	Bounds min = { .limit = MINIMUM, .type = EMPTY, .value = NULL }; 
	bounds[0] = min;
	Bounds max = { .limit = MAXIMUM, .type = EMPTY, .value = NULL };
	bounds[1] = max;

	for (i = 0; i < xpath_obj->nodesetval->nodeNr; i++)
	{
		facet = xpath_obj->nodesetval->nodeTab[i];
		value = xmlGetProp(facet, "value");
		
		// check facets
		if (EQUALS(facet->name, "minExclusive"))
		{
			bounds[0].type = EXCLUSIVE;
			bounds[0].value = value;
		}
		
		if (EQUALS(facet->name, "maxExclusive"))
		{
			bounds[1].type = EXCLUSIVE;
			bounds[1].value = value;
		}

		if (EQUALS(facet->name, "minInclusive"))
		{
			bounds[0].type = INCLUSIVE;
			bounds[0].value = value;
		}
		
		if (EQUALS(facet->name, "maxInclusive"))
		{
			bounds[1].type = INCLUSIVE;
			bounds[1].value = value;
		}
	}

	prepare_builtin_type(builtin_type, &action_type, &check_function, &desc_text);
	
	desc_text = concat_string(desc_text, " (");
	
	for (i = 0; i < 2; i++)
	{
		if (bounds[i].type != EMPTY)
		{
			if (bounds[i].type == EXCLUSIVE) desc_text = concat_string(desc_text, "exclusive");
			if (bounds[i].type == INCLUSIVE) desc_text = concat_string(desc_text, "inclusive");
			
			if (bounds[i].limit == MINIMUM) desc_text = concat_string(desc_text, " minimum: ");
			if (bounds[i].limit == MAXIMUM) desc_text = concat_string(desc_text, " maximum: ");
		}
		desc_text = concat_string(desc_text, bounds[i].value);
		if (i == 0 && bounds[1].type != EMPTY) desc_text = concat_string(desc_text, ", ");
	}
	desc_text = concat_string(desc_text, ")");
	
	do
	{
		correct = false;
		
		free_request(request);
		request = NULL;
		free_answer(answer);
		answer = NULL;
		
		// create request
		if (!(request = create_data_request(STRING_REQ, action_type, refuse, xmlStrdup(xml_element->name), xmlStrdup(desc_text), NULL))) goto cleanup;
		
		// get user data
		if(!(answer = make_create_request_with_check(com, request, undo_coll, unique_head, &create_restriction, schema_restriction, xml_element))) goto cleanup;
		
		// check if answer is null
		if (answer->data == NULL)
		{
			WARNING("Text submitted is null: The value assigned is the pattern regex. The resulting XML file will NOT be valid!", true);
			text = desc_text;
			correct = true;
			continue;
		}
		
		// set text
		text = answer->data;

		// check uniqueness
		if (!(is_unique(*unique_head, text, xml_element)))
		{
			refuse = NOT_UNIQUE;
			continue;		
		}

		// check built-in type
		if (!((*check_function)(text)))
		{
			refuse = INCORR;
			continue;
		}
		
		// check bounds
		if (!(check_bounds(text, &(bounds[0]), &(bounds[1]), action_type)))
		{
			refuse = OUT_OF_BOUNDS;
			continue;			
		}
		
		correct = true;
			
	} while (!(correct));
	
	// create new text node
	text_node = xmlNewText(text);
	xmlAddChild(xml_element, text_node);
	
	// add to undo list
	add_undo(undo_coll, unique_head, com, &create_restriction, schema_restriction, xml_element, text_node);
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, BOUNDS_CONTENT, text_node);
	
	cleanup:
		xmlXPathFreeObject(xpath_obj);
		xmlFree(builtin_type);
		xmlFree(desc_text);
		free_request(request);
		free_answer(answer);
}

static void create_restriction(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_restriction, xmlNodePtr xml_element)
{
	xmlXPathObjectPtr	xpath_obj = NULL;
	
	int								i;
	
	xmlNodePtr				base_type,
										schema_type,
										type_child;
	
	xmlChar						*builtin_type = NULL;
	
	// get base type
	if (!(base_type = get_schema_type(com->schema, schema_restriction)))
	{
		ERROR("Could not get base type", true);
		return;
	}
	
	// process as complex type
	if (EQUALS(base_type->name, "complexType"))
	{
		create(undo_coll, unique_head, com, schema_restriction, xml_element, create_complex_type);
	}
	else // or treat as built-in or simple type
	{		
		// process attribute
		if ((xpath_obj = get_xpath_result(XPATH_ATTR, com->schema->xpath_ctxt, schema_restriction)) && xpath_obj->nodesetval->nodeNr > 0)
		{
			create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[0], xml_element, create_attribute);
			
			// free xpath object
			xmlXPathFreeObject(xpath_obj);
		}
		
		// process enumeration
		create_enumeration_facet(undo_coll, unique_head, com, schema_restriction, xml_element);
		
		// process pattern
		create_pattern_facet(undo_coll, unique_head, com, schema_restriction, xml_element);
		
		// process bound_facet
		create_bounds_facet(undo_coll, unique_head, com, schema_restriction, xml_element);		
	}
	
	// TODO check other types too...

	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_restriction, xml_element, com);
	
	// free
	xmlFree(builtin_type);
}

static void create_complex_content(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_content, xmlNodePtr xml_element)
{
	xmlNodePtr	child;
	
	Request			*request;
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, COMPLEX_CONTENT, NULL);
	
	for (child = schema_content->children; child; child = child->next)
	{
		if (EQUALS(child->name, "extension"))
		{
			
			create(undo_coll, unique_head, com, child, xml_element, create_extension);
		}
		
		if (EQUALS(child->name, "restriction"))
		{
			create(undo_coll, unique_head, com, child, xml_element, create_restriction);
		}
		
		if (EQUALS(child->name, "group"))
		{
			create(undo_coll, unique_head, com, child, xml_element, create_group);
		}
	}

	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_content, xml_element, com);
}

static void create_simple_content(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_content, xmlNodePtr xml_element)
{
	// check if node is not deleted in undo
	if (!(check_continue(com, undo_coll, xml_element))) return;
	
	xmlNodePtr	child;

	Request			*request;
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, SIMPLE_CONTENT, NULL);
	
	for (child = schema_content->children; child; child = child->next)
	{
		if (EQUALS(child->name, "extension"))
		{
			create(undo_coll, unique_head, com, child, xml_element, create_extension);
		}
		
		if (EQUALS(child->name, "restriction"))
		{
			create(undo_coll, unique_head, com, child, xml_element, create_restriction);
		}
		
		if (EQUALS(child->name, "group"))
		{
			create(undo_coll, unique_head, com, child, xml_element, create_group);
		}
	}

	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_content, xml_element, com);
}

static void create_complex_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_type, xmlNodePtr xml_element)
{
	xmlXPathObjectPtr	xpath_obj;
	
	Request						*request;
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, COMPLEX_TYPE, NULL);

	// check attribute
	if ((xpath_obj = get_xpath_result(XPATH_ATTR, com->schema->xpath_ctxt, schema_type)) && xpath_obj->nodesetval->nodeNr > 0)
	{		
		int i;
		for (i = 0; i < xpath_obj->nodesetval->nodeNr; i++)
		{
			create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[i], xml_element, create_attribute);
		}
	}
	
	// check sequence
	if ((xpath_obj = get_xpath_result(XPATH_SEQ, com->schema->xpath_ctxt, schema_type)) && xpath_obj->nodesetval->nodeNr > 0)
	{
		create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[0], xml_element, create_sequence);
	}
	
	// check choice
	if ((xpath_obj = get_xpath_result(XPATH_CHOICE, com->schema->xpath_ctxt, schema_type)) && xpath_obj->nodesetval->nodeNr > 0)
	{		
		create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[0], xml_element, create_choice);
	}
	
	// check complex content
	if ((xpath_obj = get_xpath_result(XPATH_COMP_CONT, com->schema->xpath_ctxt, schema_type)) && xpath_obj->nodesetval->nodeNr > 0)
	{
		create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[0], xml_element, create_complex_content);
	}
	
	// check simple content
	if ((xpath_obj = get_xpath_result(XPATH_SIMPLE_CONT, com->schema->xpath_ctxt, schema_type)) && xpath_obj->nodesetval->nodeNr > 0)
	{
		create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[0], xml_element, create_simple_content);
	}
	
	// check group
	if ((xpath_obj = get_xpath_result(XPATH_GROUP, com->schema->xpath_ctxt, schema_type)) && xpath_obj->nodesetval->nodeNr > 0)
	{
		create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[0], xml_element, create_group);
	}	
	
	// TODO check other stuff too...
	
	xmlXPathFreeObject(xpath_obj);
	
	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_type, xml_element, com);
}

static void create_simple_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_type, xmlNodePtr xml_element)
{
	xmlXPathObjectPtr	xpath_obj;
	
	Request			*request;	
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, SIMPLE_TYPE, NULL);	

	// check restriction
	if ((xpath_obj = get_xpath_result(XPATH_REST, com->schema->xpath_ctxt, schema_type)) && xpath_obj->nodesetval->nodeNr > 0)
	{
		create(undo_coll, unique_head, com, xpath_obj->nodesetval->nodeTab[0], xml_element, create_restriction);
	}
	
	// TODO add list, union
	
	xmlXPathFreeObject(xpath_obj);

	// add annotations in case of template
	if (com->type == TEMPLATE) get_annotations(schema_type, xml_element, com);
}

static void create_builtin_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_element)
{
	xmlChar					*type = NULL,
									*text = NULL,
									*desc_text = NULL;
							
	xmlNodePtr			text_node = NULL;
							
	Request					*text_request = NULL;
	
	Answer					*text_answer = NULL;
	
	Action_Type			action_type;
	
	Refuse_Type			refuse;
	
	bool 						is_correct;
	
	Check_function	check_function;
	
	// check type
	if (!(type = xmlGetProp(schema_element, "type")))
	{
		if (!(type = xmlGetProp(schema_element, "base")))
		{
			ERROR("Could not get type attribute", true);
			goto cleanup;			
		}
	}
	
	prepare_builtin_type(type, &action_type, &check_function, &desc_text);

	do
	{
		// free text answer for reuse			
		if (text_answer)
		{
			free_answer(text_answer);
			text_answer = NULL;
		}
		
		if (!(text_request = create_data_request(STRING_REQ, action_type, refuse, xmlStrdup(xml_element->name), xmlStrdup(desc_text), xmlNodeGetContent(xml_element)))) goto cleanup;

		// get user data
		if (!(text_answer = make_create_request_with_check(com, text_request, undo_coll, unique_head, &create_builtin_type, schema_element, xml_element))) goto cleanup;
		
		// answer string is text
		text = text_answer->data;
		
		// output testing warning
		if (!(text))
		{
			WARNING("Text submitted is null: Parent node will be empty. The resulting XML file might NOT be valid!", true);
			
			// proceed and do not check uniqueness
			break;
		}
		
		// check type
		if (check_function && !((*check_function)(text))) 
		{
			is_correct = false;
			refuse = INCORR;
		}
		else
		{
			is_correct = true;
			refuse = NOT_UNIQUE;
		}
	} while (!(is_unique(*unique_head, text, xml_element)) || !(is_correct));
	
	// delete all old text
	remove_all_child_nodes(xml_element);	
	
	// create the text node
	text_node = xmlNewText(text);
	xmlAddChild(xml_element, text_node);	

	// add to undo list
	add_undo(undo_coll, unique_head, com, &create_builtin_type, schema_element, xml_element, text_node);
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, SIMPLE_TYPE, text_node);
	
	cleanup:
		xmlFree(type);
		xmlFree(desc_text);
		free_answer(text_answer);
}

static void create_schema_type(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_type, xmlNodePtr xml_element)
{	
	if (EQUALS(schema_type->name, "simpleType"))
	{
		create(undo_coll, unique_head, com, schema_type, xml_element, create_simple_type);
	}
	else if (EQUALS(schema_type->name, "complexType"))
	{
		create(undo_coll, unique_head, com, schema_type, xml_element, create_complex_type);
	}
	else
	{
		create(undo_coll, unique_head, com, schema_type, xml_element, create_builtin_type);
	}
}

static void create_content(Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, xmlNodePtr schema_element, xmlNodePtr xml_element)
{
	// increment depth
	com->cur_depth += 1;

	xmlNodePtr	schema_type,
							children,
							tmp;
	
	Request			*request = NULL;
	
	xmlChar			*ref = NULL;
	
	// confirm creation
	confirm_execution(com, CREATE_CONF, CONTENT, NULL);
	
	// check if attribute
	if (EQUALS(schema_element->name, "attribute"))
	{		
		// delete existing content
		children = xml_element->children;
		
		while (children)
		{
			tmp = children;
			children = children->next;
			
			xmlUnlinkNode(tmp);
			xmlFreeNode(tmp);
		}
		
		// get reference if exists
		if ((ref = xmlGetProp(schema_element, "ref")))
		{
			schema_element = get_referenced_node(ref, com->schema);
			xmlFree(ref);
		}
	}
	else
	{
		// get unique definition node and add to unique list
		get_unique(unique_head, com->schema, schema_element, com->xml, xml_element);			
	}

	if(!(schema_type = get_schema_type(com->schema, schema_element)))
	{
		ERROR("Could not get schema type", true);
		return;
	}
	
	// create the schema type
	create(undo_coll, unique_head, com, schema_type, xml_element, create_schema_type);

	// decrement depth
	com->cur_depth -= 1;
}

static void create_by_schema(Communicator *com)
{	
	xmlNodePtr			schema_root_element,
									xml_root_element;
	
	xmlChar					*root_tag_name;
	
	List_Node				*unique_head = NULL;
	
	Undo_Collection	*undo_coll = NULL;
	
	Request					*request = NULL;
	
	Answer					*answer = NULL;
	
	Answer_Type			type = UNDO_ANSWER;
	
	// libxml: This initialises the library and checks potential ABI mismatches between the version it was compiled for and the actual shared library used.
	LIBXML_TEST_VERSION
	
	// get the schema root element
	if (!(schema_root_element = get_schema_root_element(com->schema)))
	{
		ERROR("Could not get schema root element", true);
		goto cleanup;
	}
  
  // get the XML root element
  if (!(xml_root_element = xmlDocGetRootElement(com->xml->doc)))
  {
  	if (!(root_tag_name = xmlGetProp(schema_root_element, "name")))
  	{
  		ERROR("Could not get schema root element name", true);
  		goto cleanup;
  	}
  	
  	// if no root element, create one
  	xml_root_element = xmlNewNode(NULL, root_tag_name);
  	xmlDocSetRootElement(com->xml->doc, xml_root_element);
  	
  	xmlFree(root_tag_name);
  }
  
  // create the undo collection
  undo_coll = create_undo_collection();
  
  // create the xml document
  create_content(undo_coll, &unique_head, com, schema_root_element, xml_root_element);
  
  // do not ask for final undo if interruption is requested
  if (com->interrupt) goto cleanup;
  
  while (type == UNDO_ANSWER)
  {
		// make completion request
		answer = make_completion_request(com, undo_coll, &unique_head);
		
		// set type for check
		type = answer->type;
		
		free_answer(answer);
  }
  
  cleanup:
  	// notify completion
  	confirm_execution(com, COMPLETE_CONF, CREATE_FINISHED, NULL);
  	
  	// cleanup
  	if (unique_head) free_list(&unique_head, free_unique);
		if (undo_coll) free_undo_coll(undo_coll);
		xmlCleanupParser();
}

/* CREATE FUNCTIONS END */

/* EDIT FUNCTIONS */

static void edit_by_schema(Communicator *com)
{
	Request						*request = NULL;
	
	Answer						*answer = NULL;
	
	xmlXPathObjectPtr	xpath_obj = NULL;

	Answer_Type				type = NO_ANSWER;
	
	xmlNodePtr				node = NULL,
										schema_node = NULL;

	int								min,
										max,
										count;

	xmlChar						*use = NULL;

	// libxml: This initialises the library and checks potential ABI mismatches between the version it was compiled for and the actual shared library used.
	LIBXML_TEST_VERSION
	
	// keep user asking if wants to search
	while (type != QUIT_ANSWER)
	{		
		if (!(answer))
		{			
			// offer search but no results
			request = create_search_request(NULL);
			answer = make_search_request(com, request);
		}
		
		if (answer)
		{
			switch (answer->type)
			{
				case SEARCH_ANSWER: // in case the user sent data for search
					
					// perform Xpath search
					xpath_obj = get_xpath_result(answer->data, com->xml->xpath_ctxt, NULL);
				
					// remove answer as no longer needed
					free_answer(answer);
				
					// check result
					if (xpath_obj)
					{
						request = create_search_request(xpath_obj->nodesetval);
				
						xmlXPathFreeObject(xpath_obj);		
					}
					else
					{
						request = create_search_request(NULL);
					}
				
					// send results to be displayed and await answer
					answer = make_search_request(com, request);
					
					break;
				
				case DELETE_ANSWER: // in case the user requests the deletion of a node
					
					node = answer->data;
				
					// free answer
					free_answer(answer);
				
					switch (node->type)
					{
						case XML_ELEMENT_NODE:

							// get the schema node
							if (schema_node = get_schema_node(com->schema, node))
							{
								// get the bounds from schema
								get_bounds(&min, &max, schema_node);
							
								// get node occurrences
								xpath_obj = get_xpath_result(node->name, com->xml->xpath_ctxt, node->parent);
								count = xpath_obj->nodesetval->nodeNr;
								xmlXPathFreeObject(xpath_obj);
							
								// check if minimum is already reached
								if (count <= min)
								{
									confirm_execution(com, EDIT_CONF, DELETE_FORBIDDEN, NULL);
								}
								else
								{
									// delete the node
									xmlUnlinkNode(node);
									xmlFreeNode(node);
								
									// create confirmation that deleted
									confirm_execution(com, EDIT_CONF, DELETE_SUCCESS, NULL);							
								}
							}
							else 
							{
								confirm_execution(com, EDIT_CONF, DELETE_ERROR, NULL);
							}

							break;
					
						case XML_ATTRIBUTE_NODE:
						
							if ((use = xmlGetProp(schema_node, "use")) && EQUALS(use, "required"))
							{
								// create notification that deletion not allowed
								request = create_request(EDIT_CONF, DELETE_FORBIDDEN);
							}
							else
							{
								// delete the node
								xmlUnlinkNode(node);
								xmlFreeNode(node);
								
								// create confirmation that deleted
								confirm_execution(com, EDIT_CONF, DELETE_SUCCESS, NULL);
							}
						
							xmlFree(use);
						
							break;
					
						default:
						
							// notify user that node connot be deleted
							confirm_execution(com, EDIT_CONF, DELETE_ERROR, NULL);
						
							break;
					}
				
					// send answer null to indicate that calling thread is to be prompted if to make new search
					answer = NULL;
					
					break;
				
				case EDIT_ANSWER: // in case the user requests to change a node
					
					node = answer->data;
				
					// free answer
					free_answer(answer);
				
					switch (node->type)
					{
						case XML_ELEMENT_NODE:
						case XML_ATTRIBUTE_NODE:
						
							// get corresponding schema node
							if (schema_node = get_schema_node(com->schema, node))
							{
								// start creation of content from node
								Undo_Collection	*undo_coll = NULL;
								List_Node				*unique_head = NULL;
								undo_coll = create_undo_collection();
							
								create_content(undo_coll, &unique_head, com, schema_node, node);
							
								// check if editing (creation) was interrupted
								if (com->interrupt)
								{
									goto cleanup;
								}
								else
								{
									// notify user that node has successfully been edited
									confirm_execution(com, EDIT_CONF, EDIT_SUCCESS, NULL);							
								}

								// free unique and undo lists
								if (unique_head) free_list(&unique_head, free_unique);
								if (undo_coll) free_undo_coll(undo_coll);
							}
							else
							{
								// notify user that node connot be edited
								confirm_execution(com, EDIT_CONF, EDIT_ERROR, NULL);
							}
						
							break;
					
						default:
					
							// notify user that node connot be edited
							confirm_execution(com, EDIT_CONF, EDIT_FORBIDDEN, NULL);
						
							break;			
					}
					
					// send answer null to indicate that calling thread is to be prompted if to make new search
					answer = NULL;
					
					break;
				
				case QUIT_ANSWER: // in case the user requests to quit editing
					
					free_answer(answer);
				
					type = QUIT_ANSWER;
					
					break;
			}
		}
	}
	
	cleanup:
		// notify completion
		confirm_execution(com, COMPLETE_CONF, CREATE_FINISHED, NULL);

		xmlCleanupParser();
}

/* EDIT FUNCTIONS END */

/* COMMUNICATION FUNCTIONS */

/* REQUEST FUNCTIONS */

bool has_request(Communicator *com)
{
	if (com->request) return true;
	else return false;
}

static Request *create_request(Request_Type type, Action_Type action)
{
	Request	*request;
	
	// allocate memory for the new request
	if (!(request = malloc(sizeof(Request))))
	{
		ERROR("Could not allocate memory for request", true);
		return NULL;
	}
	
	// set type
	request->type = type;
	
	// set action
	request->action = action;
	
	// set refuse
	request->refuse = NO_REFUSE;
	
	// set data to NULL
	request->data = NULL;		
}

static Request *create_data_request(Request_Type type, Action_Type action, Refuse_Type refuse, xmlChar *node_name, xmlChar *node_type, xmlChar *content)
{
	Request						*request;
	
	Node_Description	*node_desc;
	
	// make an empty request of type data
	if (!(request = create_request(type, action)))
	{
		return NULL;
	}
	
	// set refuse
	request->refuse = refuse;
	
	// create a node description
	if (!(node_desc = malloc(sizeof(Node_Description))))
	{
		ERROR("Could not allocate memory for node description", true);
		free_request(request);
		return NULL;		
	}
	
	// add data to node description
	node_desc->name = node_name;
	node_desc->type = node_type;
	node_desc->content = content;
	
	// assign it to request
	request->data = node_desc;
	
	return request;	
}

static Request *create_choice_request(Action_Type action, Refuse_Type refuse, xmlChar *node_name)
{
	Request						*request;
	
	Choice_Collection	*coll;
	
	// make an empty request of type choice
	if (!(request = create_request(CHOOSE_REQ, action)))
	{
		return NULL;
	}
	
	// set refuse type
	request->refuse = NO_REFUSE;
	
	// add an empty choice collection to data
	if (!(coll = malloc(sizeof(Choice_Collection))))
	{
		ERROR("Could not allocate memory for choice collection", true);
		free_request(request);
		return NULL;
	}
	
	// add parent node name
	coll->name = node_name;
	
	// initialize members
	coll->count = 0;
	coll->choices = NULL;
	
	// add choice collection
	request->data = coll;
	
	return request;
}

static void add_choice(Request *request, xmlChar *choice)
{
	Choice_Collection	*coll = request->data;
	
	size_t size = coll->count;
	
	if (!(coll->choices = realloc(coll->choices, (sizeof(xmlChar *) * (size + 1)))))
	{
		ERROR("Could not extend memory for choice array", true);
		return;
	}
	
	coll->choices[size] = choice;
	
	coll->count++;
}

static Request *create_search_request(xmlNodeSetPtr nodeset)
{
	Request	*request;
	
	Result	*result;
	
	int			i;
	
	if (nodeset && nodeset && nodeset->nodeNr > 0)
	{
		// make an empty request of type search
		if (!(request = create_request(SEARCH_REQ, SEARCH_RESULT)))
		{
			return NULL;
		}
		
		if (!(result = malloc(sizeof(Result))))
		{
			free_request(request);
			ERROR("Could not allocate memory for result", true);
			return NULL;			
		}
		
		result->count = nodeset->nodeNr;
		
		// add the nodes found	
		if (!(result->nodes = malloc(sizeof(xmlNodePtr) * nodeset->nodeNr)))
		{
			free_request(request);
			ERROR("Could not allocate memory for nodes array", true);
			return NULL;		
		}
		
		for (i = 0; i < nodeset->nodeNr; i++)
		{
			result->nodes[i] = nodeset->nodeTab[i];
		}
		
		request->data = result;
	}
	else
	{
		// make an empty request of type search
		if (!(request = create_request(SEARCH_REQ, SEARCH_EMPTY)))
		{
			return NULL;
		}		
	}

	return request;
}

void confirm_execution(Communicator *com, Request_Type type, Action_Type action, xmlNodePtr xml_node)
{
	if (com->type == COMMUNICATE)
	{
		Request	*request;
	
		// let through if request could not be created
		if (!(request = create_request(type, action))) 
		{
			ERROR("Request could not be created.", true);
			return;
		}
	
		// add the XML node as data
		request->data = xml_node;
	
		// add the request to the communicator
		com->request = request;
	
		// wait that onfirmation has been received by checking if request is set null by the calling thread
		while (com->request)
		{

		}	
	}
}

static Request *create_confirmation(Action_Type action, xmlNodePtr node)
{
	Request	*request;
	
	if (!(request = create_request(CREATE_CONF, action)))
	{
		return NULL;
	}
	
	// add the XML node as data
	request->data = node;
	
	return request;
}

static void free_request(Request *request)
{	
	Node_Description	*node_desc;
	
	Choice_Collection	*coll;
	
	int								i;
	
	// check if it exists at all
	if (request)
	{
		// in case there are additional data
		if (request->data)
		{
			switch (request->type)
			{
				case CONFIRM_REQ:
				case STRING_REQ:
					
					node_desc = request->data;
						
					xmlFree(node_desc->name);
					
					xmlFree(node_desc->type);
					
					xmlFree(node_desc->content);
					
					free(node_desc);
					
					break;
				
				case CHOOSE_REQ:
					
					coll = request->data;

					// free each choice
					for (i = 0; i < coll->count; i++)
					{
						xmlFree(coll->choices[i]);
					}

					// free node name
					xmlFree(coll->name);
					
					free(coll);
					
					break;
					
				case SEARCH_REQ:
					
					if (request->action == SEARCH_RESULT)
					{
						Result *result = request->data;
				
						// free the array containing those pointers (but not the pointers themselves)
						free(result->nodes);
				
						free(result);
					}
					
					break;
			}
		}
		
		// free the request object itself
		free(request);
	}
}

static Answer *make_template_answer(Request *request, Answer *answer)
{
	// select correct answer according to request type
	switch (request->type)
	{
		case CONFIRM_REQ: // main thread must accept or refuse
		case STRING_REQ: // main thread must return string data
			// decide what to answer according to the request action
			switch (request->action)
			{
				case CREATE_NODE: // main thread must accept or refuse creation
					answer = create_answer(DATA_ANSWER);
					answer->acceptance = ACCEPT_ONCE;
					break;
				case DEFAULT: // main thread must accept or refuse default values
					answer = create_answer(DATA_ANSWER);
					answer->acceptance = ACCEPT;
					break;
				case REPLACE_NODE: // main thread must accept or refuse node replacement
					answer = create_answer(DATA_ANSWER);
					answer->acceptance = REFUSE;
					break;
				// main thread must return string data
				default:
					answer = create_answer(DATA_ANSWER);
					answer->data = NULL; // submit null to signal that value must not be checked for uniqueness or pattern conformity
					break;
			}
			break;
		
		case CHOOSE_REQ: // main thread must choose from a list of options
			answer = create_answer(DATA_ANSWER);
			answer->chosen_index = -1; // submit null to signal that value must not be checked for uniqueness or pattern conformity
			break;
	}
	
	return answer;
}

static Answer *make_create_request_with_check(Communicator *com, Request *request, Undo_Collection *undo_coll, List_Node **unique_head, Create_function create_function, xmlNodePtr schema_element, xmlNodePtr xml_node)
{	
	Answer	*answer_obj;
	
	switch (com->type)
	{
		case COMMUNICATE:
			// add the request to the communicator
			com->request = request;

			// wait until the calling thread has answered
			while (!(com->answer))
			{

			}

			// free and reset request as not needed any longer
			free_request(com->request);
			com->request = NULL;

			answer_obj = com->answer;
			com->answer = NULL;

			// check if to abort or not (= abort further creation or editing) 
			if (check_answer(answer_obj, undo_coll, unique_head, com, create_function, schema_element, xml_node))
			{
				// return answer (must be freed by caller)
				return answer_obj;
			}
			else
			{
				// free and reset answer as not needed any longer
				free_answer(answer_obj);

				return NULL;
			}
		
		case TEMPLATE:
			
			// get the answer
			answer_obj = make_template_answer(request, answer_obj);
			
			// free and reset request as not needed any longer
			free_request(com->request);
			com->request = NULL;
			return answer_obj;		
	}
}

static Answer *make_completion_request(Communicator *com, Undo_Collection *undo_coll, List_Node **unique_head)
{
	Request	*request;
	
	Answer	*answer;
	
	// let through if request could not be created
	if (!(request = create_request(COMPLETE_CONF, CREATE_SUCCESS))) 
	{
		ERROR("Request could not be created.", true);
		return NULL;
	}
	
	// add the request to the communicator
	com->request = request;
	
	// wait until the calling thread has answered
	while (!(com->answer))
	{
		
	}
	
	// store answer internally
	answer = com->answer;
	
	// reset answer for further answers
	com->answer = NULL;
		
	// check answer and make undo
	check_answer(answer, undo_coll, unique_head, com, NULL, NULL, NULL);
	
	// return answer (must be freed by caller)
	return answer;
}

static Answer *make_search_request(Communicator *com, Request *request)
{
	Answer	*tmp;
	
	// add the request to the communicator
	com->request = request;

	// wait until the calling thread has answered
	while (!(com->answer))
	{
		
	}

	// free and reset request as not needed any longer
	free_request(com->request);
	com->request = NULL;
	
	// assign answer pointer temporarily
	tmp = com->answer;
	
	// reset answer for further answers
	com->answer = NULL;
	
	// return answer (must be freed by caller)
	return tmp;	
}

/* REQUEST FUNCTIONS END */

/* ANSWER FUNCTIONS */

void confirm_received(Communicator *com)
{
	// set communicator request to NULL to signal that request has been received
	com->request = NULL;
}

static Answer *create_answer(Answer_Type type)
{
	Answer	*answer;
	
	// add the answer
	if (!(answer = malloc(sizeof(Answer))))
	{
		ERROR("Could not create new Answer", true);
		return false;
	}
	
	// set type
	answer->type = type;
	
	// initialize members
	answer->data = NULL;

	answer->chosen_index = -1; 

	answer->acceptance = REFUSE;
	
	return answer;
}

bool quit(Communicator *com)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(QUIT_ANSWER)))
	{
		return false;
	}
	
	// add answer to communicator
	com->answer = answer;
	
	// wait until a completion confirmation is issued in worker thread
	while (!(com->request) || !(com->request->type == COMPLETE_CONF && com->request->action == CREATE_FINISHED))
	{

	}
	
	// confirm that confirmation has been received so that worker thread completes execution and worker thread is terminated
	confirm_received(com);
	
	return true;
}

void accept(Communicator *com)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(DATA_ANSWER)))
	{
		return;
	}
	
	answer->acceptance = ACCEPT;
	
	// add answer to communicator
	com->answer = answer;	
}

void accept_once(Communicator *com)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(DATA_ANSWER)))
	{
		return;
	}
	
	answer->acceptance = ACCEPT_ONCE;
	
	// add answer to communicator
	com->answer = answer;	
}

void refuse(Communicator *com)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(DATA_ANSWER)))
	{
		return;
	}
	
	answer->acceptance = REFUSE;

	// add answer to communicator
	com->answer = answer;
}

void answer(Communicator *com, xmlChar *data)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(DATA_ANSWER)))
	{
		return;
	}
	
	answer->data = data;

	// add answer to communicator
	com->answer = answer;
}

void send_xpath(Communicator *com, xmlChar *xpath_exp)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(SEARCH_ANSWER)))
	{
		return;
	}
	
	answer->data = xpath_exp;

	// add answer to communicator
	com->answer = answer;
}

void choose(Communicator *com, int chosen_index)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(DATA_ANSWER)))
	{
		return;
	}
	
	answer->chosen_index = chosen_index;

	// add answer to communicator
	com->answer = answer;
}

void request_editing(Communicator *com, xmlNodePtr node)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(EDIT_ANSWER)))
	{
		return;
	}
	
	answer->data = node;

	// add answer to communicator
	com->answer = answer;
}

void request_deletion(Communicator *com, xmlNodePtr node)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(DELETE_ANSWER)))
	{
		return;
	}
	
	answer->data = node;

	// add answer to communicator
	com->answer = answer;
}

void request_undo(Communicator *com)
{
	Answer	*answer;
	
	// signal that request has been received by user
	confirm_received(com);
	
	if (!(answer = create_answer(UNDO_ANSWER)))
	{
		return;
	}

	// add answer to communicator
	com->answer = answer;
}

static bool check_answer(Answer *answer, Undo_Collection *undo_coll, List_Node **unique_head, Communicator *com, Create_function create_function, xmlNodePtr schema_node, xmlNodePtr xml_node)
{
	switch (answer->type)
	{
		case DATA_ANSWER:
			return true;
		case UNDO_ANSWER:
			process_undo(undo_coll, unique_head, com, create_function, schema_node, xml_node);
			return false;
		case QUIT_ANSWER:
			com->interrupt = true;
			return false;
		case DELETE_ANSWER:
		case EDIT_ANSWER:
		case SEARCH_ANSWER:
			com->interrupt = true;
			ERROR("This type of answer is node allowed, check code", true);
			return false;
	}
}

static void free_answer(Answer *answer)
{
	// check if it exists at all
	if (answer)
	{
		// free answer data if not node
		if (answer->type != DELETE_ANSWER && answer->type != EDIT_ANSWER) xmlFree(answer->data);
	
		// free the answer object itself
		free(answer);
	}
}

/* ANSWER FUNCTIONS END */

Communicator *create_communicator(Schema_Obj *schema, XML_Obj *xml)
{
	Communicator	*com;
	
	if (!(com = malloc(sizeof(Communicator))))
	{
		ERROR("Could not create new Communicator", true);
		return NULL;
	}
	
	// set to default
	com->request = NULL;
  com->answer = NULL;
  com->interrupt = false;
  com->cur_depth = 0;
  
  // add the schema and the xml for processing
  com->schema = schema;
  com->xml = xml;
  
  return com;	
}

void start_creation(Communicator *com, Creation_Type type)
{
	// assign creation type to communicator
	com->type = type;
	
	// declare worker thread
	pthread_t	thread;
	
	// start the worker thread
  pthread_create(&thread, NULL, start_internal_creation, com);
}

static void *start_internal_creation(void *thread_val)
{
	Communicator	*com = thread_val;
	
	// initialize error message container
	errors = NULL;
	
	// start actual creation
	create_by_schema(com);
	
	// free error message container
	xmlFree(errors);
	
	return NULL;
}

void start_editing(Communicator *com)
{
	// assign communicate type to communicator
	com->type = COMMUNICATE;
	
	// declare worker thread
	pthread_t	thread;
	
	// start the worker thread
  pthread_create(&thread, NULL, start_internal_editing, com);	
}

static void *start_internal_editing(void *thread_val)
{
	Communicator	*com = thread_val;

	// initialize error message container
	errors = NULL;
	
	edit_by_schema(com);

	// free error message container
	xmlFree(errors);

	return NULL;
}

/* COMMUNICATION FUNCTIONS END */

#else

xmlChar *create_string(xmlChar *format, ...)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

xmlChar *concat_string(xmlChar *old_str, const xmlChar *new_str)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

xmlXPathObjectPtr get_xpath_result(const xmlChar *xpath_exp, xmlXPathContextPtr xpath_ctxt, xmlNodePtr ctxt_node)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);	
}

void save_to_file(XML_Obj *xml, xmlChar *file_path, xmlChar *encoding)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

Schema_Obj *get_schema(xmlChar *schema_path, xmlChar *encoding)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void free_schema(void *schema)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

XML_Obj *get_xml(Schema_Obj *schema, xmlChar *xml_path, xmlChar *encoding)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

XML_Obj *create_xml(Schema_Obj *schema, xmlChar *version)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void free_xml(void *xml)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

bool has_request(Communicator *com)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

bool quit(Communicator *com)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void accept(Communicator *com)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void refuse(Communicator *com)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void answer(Communicator *com, xmlChar *data)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void send_xpath(Communicator *com, xmlChar *xpath_exp)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void choose(Communicator *com, int chosen_index)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void request_editing(Communicator *com, xmlNodePtr node)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void request_deletion(Communicator *com, xmlNodePtr node)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void request_undo(Communicator *com)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

Communicator *create_communicator(Schema_Obj *schema, XML_Obj *xml)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void *start_creation(void *thread_val)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}

void *start_editing(void *thread_val)
{
	fprintf(stderr, "LibXML support not compiled in\n");
	exit(1);
}
#endif

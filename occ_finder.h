#ifndef OCC_FINDER
#define OCC_FINDER

#include "list.h"

typedef struct Token {
  int begin;
  int end;
} Token;

/**
 * Searches multiple occurrences of a given regex pattern and saves it to a doubly linked list
 * @param regex a string containing the regular expression for search
 * @param text a string containing the text to search in
 * @param textLen the length of the text as int
 * @param head a pointer to the beginning of a list where the results are stored
 * @param tail a pointer to the end of a list where the results are stored
 * @return a negative int if an error occurred, otherwise the number of results found 
 */
int searchInText(char *regex, char *text, int textLen, List_Node **head);

/**
 * Destroys a token object.
 * @param data the token object
 */
void destroyToken(void *data);

#endif

#include "main.h"

#define ENC_UTF8 "UTF-8"

int main(int argc, char *argv[])
{ 
  char *glade_path = create_string("%.*s/window_main.glade", strrchr(argv[0], '/') - argv[0], argv[0]);
  
  gtk_init(&argc, &argv);

  builder = gtk_builder_new();
  gtk_builder_add_from_file(builder, glade_path, NULL);
  
  free(glade_path);

  window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
  gtk_builder_connect_signals(builder, NULL);

  gtk_widget_show(window);                
  gtk_main();

  return 0;
}

/* KEYBOARD EVENT FUNCTIONS */

bool trigger_grid_button(const gchar *value)
{
  GtkWidget							*schema_notebook,
  											*xml_notebook;

  Schema_Collection 		*s_coll;
  
  XML_Create_Collection	*c_coll;
  
  XML_Edit_Collection		*e_coll;
  
  List_Node							*cur;
  
	GList									*children,
												*iter;

	schema_notebook = GTK_WIDGET(gtk_builder_get_object(builder, "notebook"));
	xml_notebook = get_current_tab(GTK_NOTEBOOK(schema_notebook));
	s_coll = get_current_schema_collection(xml_notebook);
	
	for (cur = s_coll->create_list_head; cur; cur = cur->next)
	{
		c_coll = cur->data;
		
		if (get_current_tab(GTK_NOTEBOOK(xml_notebook)) == GTK_WIDGET(c_coll->paned))
		{
			children = gtk_container_get_children(GTK_CONTAINER(c_coll->grid));
			
			for (iter = children; iter; iter = g_list_next(iter))
			{
				if (EQUALS(gtk_widget_get_name(GTK_WIDGET(iter->data)), value) && gtk_widget_is_sensitive(GTK_WIDGET(iter->data)))
				{
					g_signal_emit_by_name(iter->data, "clicked");
					return true;
				}
			}
			
			g_list_free(children);
			break;
		}
	}
	
	for (cur = s_coll->edit_list_head; cur; cur = cur->next)
	{
		e_coll = cur->data;
		
		if (get_current_tab(GTK_NOTEBOOK(xml_notebook)) == GTK_WIDGET(e_coll->paned))
		{
			if (EQUALS(gtk_widget_get_name(GTK_WIDGET(e_coll->search_button)), value) && gtk_widget_is_sensitive(GTK_WIDGET(e_coll->search_button)))
			{
				g_signal_emit_by_name(e_coll->search_button, "clicked");
				return true;
			}
			
			children = gtk_container_get_children(GTK_CONTAINER(e_coll->grid));
			
			for (iter = children; iter; iter = g_list_next(iter))
			{
				if (EQUALS(gtk_widget_get_name(GTK_WIDGET(iter->data)), value) && gtk_widget_is_sensitive(GTK_WIDGET(iter->data)))
				{
					g_signal_emit_by_name(iter->data, "clicked");
					return true;
				}
			}
			
			g_list_free(children);
			break;
		}
	}
	
	return false;
}

gint on_window_main_key_press_event(GtkWidget* widget, GdkEventKey* event, gpointer data)
{
	switch (event->keyval)
  {
  	case GDK_KEY_s:
  		if (event->state & GDK_CONTROL_MASK)
  		{
  			on_open_xsd_button_clicked();
  			return true;
  		}
  		else if (trigger_grid_button("s"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_x:
  		if ((event->state & GDK_CONTROL_MASK) && schema_list_head)
  		{
  			on_create_xml_button_clicked();
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_e:
  		if ((event->state & GDK_CONTROL_MASK) && schema_list_head)
  		{
  			on_edit_xml_button_clicked();
  			return true;
  		}
  		else
  		{
  			return false;
  		} 	
  	case GDK_KEY_Return:
  		if (trigger_grid_button("y"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
		case GDK_KEY_BackSpace:
  		if (trigger_grid_button("n"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_b:
  		if (trigger_grid_button("b"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_q:
  		if ((event->state & GDK_CONTROL_MASK) && trigger_grid_button("q"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_z:
  		if ((event->state & GDK_CONTROL_MASK) && trigger_grid_button("g"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}  	
  	case GDK_KEY_1:
  		if (trigger_grid_button("0"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_2:
  		if (trigger_grid_button("1"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_3:
  		if (trigger_grid_button("2"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_4:
  		if (trigger_grid_button("3"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_5:
  		if (trigger_grid_button("4"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_6:
  		if (trigger_grid_button("5"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_7:
  		if (trigger_grid_button("6"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_8:
  		if (trigger_grid_button("7"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	case GDK_KEY_9:
  		if (trigger_grid_button("8"))
  		{
  			return true;
  		}
  		else
  		{
  			return false;
  		}
  	default:
			return false;
  }
}

/* KEYBOARD EVENT FUNCTIONS END */

/* UTILITIES */

char *get_file_name_only(char *file_path)
{
	char	*file_name;
	
	if (!(file_name = strrchr(file_path, '/')))
	{
		return file_path;
	}
	else
	{
		return ++file_name;
	}
}

void set_toolbar_sensitive(gboolean sensitive)
{
	GtkWidget *button;
	
	button = GTK_WIDGET(gtk_builder_get_object(builder, "create_xml_button"));
	gtk_widget_set_sensitive(button, sensitive);
	
	button = GTK_WIDGET(gtk_builder_get_object(builder, "edit_xml_button"));
	gtk_widget_set_sensitive(button, sensitive);	
}

void clear_container(GtkContainer *container)
{
	GList	*children,
				*iter;
	
	children = gtk_container_get_children(container);
	
	for (iter = children; iter != NULL; iter = g_list_next(iter))
	{
		gtk_widget_destroy(GTK_WIDGET(iter->data));
	}
	
	g_list_free(children);	
}

void style_text_area(GtkTextView *textarea)
{
	GtkCssProvider	*provider;
	
	GtkStyleContext *context;
	
	provider = gtk_css_provider_new();
	
	gtk_css_provider_load_from_data(provider,
					                        "textview {font: 15px sans-serif;}",
					                        -1,
					                        NULL);
					                        
	context = gtk_widget_get_style_context(GTK_WIDGET(textarea));
	
	gtk_style_context_add_provider(context,
					                       GTK_STYLE_PROVIDER(provider),
					                       GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	
	gtk_text_view_set_left_margin(textarea, 5);
	gtk_text_view_set_top_margin(textarea, 5);
	gtk_text_view_set_wrap_mode(textarea, GTK_WRAP_CHAR);
}

/* UTILITIES END */

/* DIALOGS */

void error_dialog(GtkWidget *parent, char *msg)
{
	GtkWidget	*err_dialog;	
	
	err_dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
				                           		GTK_DIALOG_DESTROY_WITH_PARENT,
				                           		GTK_MESSAGE_ERROR,
				                           		GTK_BUTTONS_CLOSE,
				                           		msg);
	
	gtk_dialog_run(GTK_DIALOG(err_dialog));
	gtk_widget_destroy(err_dialog);
}

bool confirm_dialog(GtkWidget *parent, char *msg)
{
	GtkWidget	*conf_dialog;
	
	gint			answer;

	bool			confirm = false;
	
	conf_dialog = gtk_message_dialog_new(GTK_WINDOW(parent),
																			 GTK_DIALOG_DESTROY_WITH_PARENT,
																			 GTK_MESSAGE_QUESTION,
																			 GTK_BUTTONS_YES_NO,
																			 msg);

	answer = gtk_dialog_run(GTK_DIALOG(conf_dialog));

	if (answer == GTK_RESPONSE_YES) confirm = true;
	
	if (answer == GTK_RESPONSE_NO) confirm = false;
	
	gtk_widget_destroy(conf_dialog);
	
	return confirm;
}

char *file_dialog(GtkFileChooserAction action, const gchar *title, GtkWindow *parent, const gchar *filter_pattern, const char *given_file_name)
{
	GtkWidget				*dialog;
	
	gchar						*button_text;
	
	GtkFileFilter 	*filter;
	
	gint						answer;
	
	GtkFileChooser	*chooser;
	
	char						*file_name = NULL;
	
	// choose the right button text
	switch (action)
	{
		case GTK_FILE_CHOOSER_ACTION_OPEN:
			button_text = "_Open";
			break;
		case GTK_FILE_CHOOSER_ACTION_SAVE:
			button_text = "_Save";
			break;
		default:
			button_text = "_OK";
			break;
	}
	
	dialog = gtk_file_chooser_dialog_new(title,
																			 parent,
																			 action,
																			 "_Cancel",
																			 GTK_RESPONSE_CANCEL,
																			 button_text,
																			 GTK_RESPONSE_ACCEPT,
	                                     NULL);

	// set the filter
	filter = gtk_file_filter_new();
	gtk_file_filter_add_pattern(filter, filter_pattern);
	gtk_file_chooser_add_filter(GTK_FILE_CHOOSER(dialog),
                              filter);
                              
  // set given file name if exists
  if (given_file_name) gtk_file_chooser_set_filename(GTK_FILE_CHOOSER(dialog), given_file_name); 
                              
  // run the file chooser
  answer = gtk_dialog_run(GTK_DIALOG(dialog));
  
  // check if user chose
  if (answer == GTK_RESPONSE_ACCEPT)
  {
  	chooser = GTK_FILE_CHOOSER(dialog);
  	file_name = gtk_file_chooser_get_filename(chooser);
  }
  
  gtk_widget_destroy(dialog);
  
  return file_name;
}

/* DIALOGS END */

/* CREATE FUNCTIONS */

Schema_Collection *create_schema_collection(char *xsd_file_path)
{
	Schema_Collection	*coll = NULL;
	
	if (!(coll = malloc(sizeof(Schema_Collection))))
  {
  	return NULL;
  }
  
	if (!(coll->schema = get_schema(xsd_file_path, ENC_UTF8)))
	{
		free(coll); // simple free because no data
		
		error_dialog(window, get_error_message());
		
		return NULL;
	}
	
	coll->tab_label = NULL;
	
	coll->notebook = NULL;
	
	coll->create_list_head = NULL;
	
	coll->edit_list_head = NULL;
	
	return coll;
}

XML_Create_Collection *create_xml_create_collection(Schema_Obj *schema)
{
	XML_Create_Collection	*c_coll = NULL;
	
	if (!(c_coll = malloc(sizeof(XML_Create_Collection))))
  {
  	return NULL;
  }
  
	if (!(c_coll->xml = create_xml(schema, "1.0")))
	{
		free(c_coll); // simple free because no data
		return NULL;
	}
	
	c_coll->com = create_communicator(schema, c_coll->xml);
	
	// set last row to null
	c_coll->last_row = NULL;
	
	// set last row depth
	c_coll->last_row_depth = 0;
	
	return c_coll;
}

XML_Edit_Collection *create_xml_edit_collection(Schema_Obj *schema, char *xml_file_path)
{
	XML_Edit_Collection	*e_coll = NULL;
	
	if (!(e_coll = malloc(sizeof(XML_Edit_Collection))))
  {
  	return NULL;
  }
  
  if (!(e_coll->file_path = strdup(xml_file_path)))
  {
  	free(e_coll); // simple free because no data
		return NULL;
  }
  
	if (!(e_coll->xml = get_xml(schema, xml_file_path, ENC_UTF8)))
	{
		free(e_coll->file_path);
		free(e_coll);
		
		error_dialog(window, get_error_message());
		
		return NULL;
	}
	
	e_coll->com = create_communicator(schema, e_coll->xml);
	
	// initialize the list for the results
	e_coll->result_head = NULL;
	
	// initialize the last search
	e_coll->last_xpath_exp = NULL;
	
	// initialize the last selected node
	e_coll->last_selected_node = NULL;

	// set last row to null
	e_coll->last_row = NULL;

	// set last row depth
	e_coll->last_row_depth = 0;
	
	return e_coll;
}

GtkWidget *create_tab_label(char *title, Close_function close, void *collection)
{
	GtkWidget	*box,
						*label,
						*button;
	
	box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 3);	
	
	label = gtk_label_new(title);
	button = gtk_button_new_from_icon_name("window-close", GTK_ICON_SIZE_BUTTON);
	
	// make the button operable
	g_signal_connect_swapped(button, "clicked", (GCallback) close, collection);
	
	gtk_box_pack_start(GTK_BOX(box), label, TRUE, TRUE, 3);
	gtk_box_pack_end(GTK_BOX(box), button, TRUE, TRUE, 3);
	
	return box;
}

GtkTreeViewColumn	*create_column(gchar *title, int index)
{
	GtkCellRenderer		*renderer;
	
	GtkTreeViewColumn	*column;
	
	renderer = gtk_cell_renderer_text_new();
	
	g_object_set(renderer, "editable", TRUE, NULL);
	
	column = gtk_tree_view_column_new_with_attributes(title,
					                                          renderer,
					                                          "text", index,
					                                          NULL);
	
	return column;	
}

/* CREATE FUNCTIONS END */

/* FREE FUNCTIONS */

void free_schema_collection(void *data)
{
	Schema_Collection	*s_coll = data;
	
	List_Node					*cur_xml = NULL;
	
	// free the schema object
	free_schema(s_coll->schema);
	
	// free the list of nodes in creation
	free_list(&(s_coll->create_list_head), free_xml_create_collection);
	
	// free the list of nodes in editing
	free_list(&(s_coll->edit_list_head), free_xml_edit_collection);
	
	// free the schema collection itself
	free(s_coll);
}

void free_xml_create_collection(void *data)
{
	XML_Create_Collection	*c_coll = data;
	
	// free the xml object
	free_xml(c_coll->xml);
	
	// free the communicator
	free(c_coll->com);
	
	// free the collection itself too
	free(c_coll);
}

void free_xml_edit_collection(void *data)
{
	XML_Edit_Collection	*e_coll = data;
	
	// free path
	free(e_coll->file_path);
	
	// free the xml object
	free_xml(e_coll->xml);
	
	// free the communicator
	free(e_coll->com);
	
	// free the result tree list
	free_list(&(e_coll->result_head), free);
	
	// free last xpath expression
	free(e_coll->last_xpath_exp);
	
	// free the collection itself too
	free(e_coll);		
}

/* FREE FUNCTIONS END */

/* GET FUNCTIONS */

GtkWidget *get_current_tab(GtkNotebook *notebook)
{
	gint	cur_page_nr;
	
	cur_page_nr = gtk_notebook_get_current_page(notebook);
	
	if (cur_page_nr < 0)
	{
		return NULL;
	}
	
	return gtk_notebook_get_nth_page(GTK_NOTEBOOK(notebook), cur_page_nr);
}

Schema_Collection *get_current_schema_collection(GtkWidget *cur_page)
{
	List_Node					*cur;
	
	Schema_Collection	*s_coll;
	
	// loop schema collection list	
	for (cur = schema_list_head; cur; cur = cur->next)
	{
		s_coll = cur->data;
		if (cur_page == GTK_WIDGET(s_coll->notebook)) return s_coll;
	}
	
	return NULL;
}

/* GET FUNCTIONS END */

/* COMMUNICATION FUNCTIONS */

void handle_communicator(void *coll)
{
	XML_Collection			*x_coll = coll;
	
	XML_Edit_Collection	*e_coll;
	
	Communicator				*com;						

	int									i;
	
	GtkTreeIter					child_iter,
											parent_iter;
	
	gchar								*name,
											*action,
											*type_name,
											*msg,
											*desc_text = NULL;

	GtkWidget						*result_tree,
											*box,
											*scrolled_window,
											*button_grid,
											*button,
											*textarea;

	GtkTextBuffer				*buffer;

	GtkTreeStore				*result_store;

	GtkCellRenderer 		*renderer;

	GtkTreeViewColumn		*column;
	
	Search_Result				*search_result;
	
	GtkTreeSelection		*selection;
	
	GtkTreePath					*path;
	
	Node_Description		*node_desc;
	
	Choice_Collection		*choice_coll;
	
	Result							*result;
	
	xmlNodePtr					xml_node;
	
	xmlChar							*content = NULL;
	
	// assign x_coll and communicator
	com = x_coll->com;
	
	// clean the grid
	clear_container(GTK_CONTAINER(x_coll->grid));
	
	// wait for request
	while (!(has_request(com)))
	{
		// but stay responsive
		while (gtk_events_pending()) gtk_main_iteration();
	}
	
	// show according dialog
	switch (com->request->type)
	{
		case STRING_REQ:
			
			// get the node description
			node_desc = com->request->data;
			
			switch (com->request->refuse)
			{
				case NOT_UNIQUE:
					desc_text = g_strdup_printf("Not unique! Please enter again %s for '%s':", node_desc->type, node_desc->name);
					break;
				case INCORR:
					desc_text = g_strdup_printf("Input is of wrong type! Please enter again %s for '%s':", node_desc->type, node_desc->name);
					break;
				case NO_MATCH:
					desc_text = g_strdup_printf("No match! Please enter again string matching pattern %s for '%s':", node_desc->type, node_desc->name);
					break;
				case OUT_OF_BOUNDS:
					desc_text = g_strdup_printf("Out of bounds! Please enter again %s for '%s':", node_desc->type, node_desc->name);
					break;
				case NO_REFUSE:
				default:
					switch (com->request->action)
					{
						case STRING:
						case FILE_PATH:
						case DECIMAL:
						case INTEGER:
						case DATE:
						case TIME:
						case DATETIME:
							desc_text = g_strdup_printf("Please enter %s for '%s':", node_desc->type, node_desc->name);
							break;
						case PATTERN:
							desc_text = g_strdup_printf("Please enter string matching pattern %s for '%s':", node_desc->type, node_desc->name);
							break;						
					}
			}
			
			switch (com->request->action)
			{
				case STRING:
				case PATTERN:
				case DECIMAL:
				case INTEGER:
				case DATE:
				case TIME:
				case DATETIME:
				
					gtk_grid_attach(x_coll->grid, gtk_label_new(desc_text), 0, 0, 2, 1);
				
					g_free(desc_text);
			
					textarea = gtk_text_view_new();
				
					// add text if available
					if (node_desc->content)
					{
						buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textarea));
						gtk_text_buffer_set_text(buffer, node_desc->content, -1);
					}

					style_text_area(GTK_TEXT_VIEW(textarea));
				
					gtk_grid_attach(x_coll->grid, textarea, 0, 1, 2, 1);
					gtk_widget_grab_focus(textarea); // set focused
			
					button = gtk_button_new_with_label("OK");
					gtk_grid_attach(x_coll->grid, button, 0, 2, 1, 1);
					gtk_widget_set_name(button, "y");
					g_signal_connect(button, "clicked", (GCallback) set_answer, coll);
			
					button = gtk_button_new_with_label("Go back");
					gtk_grid_attach(x_coll->grid, button, 1, 2, 1, 1);
					gtk_widget_set_name(button, "g");
					g_signal_connect_swapped(button, "clicked", (GCallback) set_undo, coll);
					
					break;
				
				case FILE_PATH:
					
					gtk_grid_attach(x_coll->grid, gtk_label_new(desc_text), 0, 0, 3, 1);
				
					g_free(desc_text);

					textarea = gtk_text_view_new();
				
					// add text if available
					if (node_desc->content)
					{
						buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textarea));
						gtk_text_buffer_set_text(buffer, node_desc->content, -1);
					}

					style_text_area(GTK_TEXT_VIEW(textarea));

					gtk_grid_attach(x_coll->grid, textarea, 0, 1, 3, 1);
					gtk_widget_grab_focus(textarea); // set focused
			
					button = gtk_button_new_with_label("Browse");
					gtk_grid_attach(x_coll->grid, button, 0, 2, 1, 1);
					gtk_widget_set_name(button, "b");
					g_signal_connect_swapped(button, "clicked", (GCallback) prompt_file_path, textarea);
			
					button = gtk_button_new_with_label("OK");
					gtk_grid_attach(x_coll->grid, button, 1, 2, 1, 1);
					gtk_widget_set_name(button, "y");
					g_signal_connect(button, "clicked", (GCallback) set_answer, coll);
			
					button = gtk_button_new_with_label("Go back");
					gtk_grid_attach(x_coll->grid, button, 2, 2, 1, 1);
					gtk_widget_set_name(button, "g");
					g_signal_connect_swapped(button, "clicked", (GCallback) set_undo, coll);
					
					break;
			}
			
			break;
			
		case CONFIRM_REQ:
			
			// get the node description
			node_desc = com->request->data;
			
			switch (com->request->action)
			{
				case CREATE_NODE:
					desc_text = g_strdup_printf("Create %s '%s'?", node_desc->type, node_desc->name);
					break;
				case REPLACE_NODE:
					desc_text = g_strdup_printf("Replace %s '%s'?", node_desc->type, node_desc->name);
					break;
				case DEFAULT:
					desc_text = g_strdup_printf("Use %s as default for %s of '%s'?", node_desc->content, node_desc->type, node_desc->name);
					break;
			}
			
			gtk_grid_attach(x_coll->grid, gtk_label_new(desc_text), 0, 0, 2, 1);
			
			g_free(desc_text);
			
			button = gtk_button_new_with_label("Yes");
			gtk_grid_attach(x_coll->grid, button, 0, 1, 1, 1);
			gtk_widget_set_name(button, "y");
			g_signal_connect(button, "clicked", (GCallback) set_answer, coll);
		
			button = gtk_button_new_with_label("No");			
			gtk_grid_attach(x_coll->grid, button, 1, 1, 1, 1);
			gtk_widget_set_name(button, "n");
			g_signal_connect(button, "clicked", (GCallback) set_answer, coll);
		
			button = gtk_button_new_with_label("Go back");
			gtk_grid_attach(x_coll->grid, button, 0, 2, 2, 1);
			gtk_widget_set_name(button, "g");
			g_signal_connect_swapped(button, "clicked", (GCallback) set_undo, coll);			
			
			break;
			
		case CHOOSE_REQ:
			
			// get the choice collection
			choice_coll = com->request->data;
			
			if (com->request->action == CHOOSE_NODE)
			{
				desc_text = g_strdup_printf("Choose one of the following child nodes of '%s':", choice_coll->name);
			}
			else if (com->request->action == CHOOSE_ENUMERATION)
			{
				desc_text = g_strdup_printf("Choose one of the following contents for '%s':", choice_coll->name);
			}
			
			gtk_grid_attach(x_coll->grid, gtk_label_new(desc_text), 0, 0, 1, 1);
			
			g_free(desc_text);
			
			for (i = 0; i < choice_coll->count; i++)
			{				
				// create button with label
				name = g_strdup_printf("%i) %s", (i + 1), choice_coll->choices[i]);				
				button = gtk_button_new_with_label(name);
				g_free(name);
				
				// give the index as name
				name = g_strdup_printf("%i", i);
				gtk_widget_set_name(button, name);
				g_free(name);
			
				gtk_grid_attach(x_coll->grid, button, 0, i + 1, 1, 1);
				g_signal_connect(button, "clicked", (GCallback) set_answer, coll);
			}
			
			button = gtk_button_new_with_label("Go back");
			gtk_grid_attach(x_coll->grid, button, 0, i + 1, 1, 1);
			gtk_widget_set_name(button, "g");
			g_signal_connect_swapped(button, "clicked", (GCallback) set_undo, coll);
			
			break;
		
		case SEARCH_REQ:
			
			e_coll = coll;
			
			// empty the result tree list
			free_list(&(e_coll->result_head), free);
			e_coll->result_head = NULL;
			
			// get the new result
			result = com->request->data;
			
			if (result && result->count > 0)
			{				
				for (i = 0; i < result->count; i++)
				{
					// create search result object
					if (!(search_result = malloc(sizeof(Search_Result))))
					{
						error_dialog(window, "Error: Could not allocate memory for search result.");
						save_clean(coll);
						return;
					}
					
					// create the box
					box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 2);
					
					// create the tree store for the results					
					result_store = gtk_tree_store_new(N_TREE_COLUMNS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_POINTER);
					
					// fill it with the data
					fill_result_store(result_store, result->nodes[i], NULL);
					
					// create the tree view
					result_tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(result_store));
				
					renderer = gtk_cell_renderer_text_new();
					column = gtk_tree_view_column_new_with_attributes("Node name",
										                                        renderer,
										                                        "text", NAME_TREE,
										                                        NULL);
										                                        
					gtk_tree_view_append_column(GTK_TREE_VIEW(result_tree), column);
				
					renderer = gtk_cell_renderer_text_new();
					g_object_set(renderer, "editable", TRUE, NULL); // the data column cells are editable 
					column = gtk_tree_view_column_new_with_attributes("Data",
										                                        renderer,
										                                        "text", CONTENT_TREE,
										                                        NULL);
										                                        
					gtk_tree_view_append_column(GTK_TREE_VIEW(result_tree), column);
					
					// create the surrounding scrolled window
					scrolled_window = gtk_scrolled_window_new(NULL, NULL);
					gtk_container_add(GTK_CONTAINER(scrolled_window), result_tree);
					
					// add it to the box
					gtk_box_pack_start(GTK_BOX(box), scrolled_window, TRUE, TRUE, 0);
					
					// add it to the search result object
					search_result->treeview = GTK_TREE_VIEW(result_tree);
					
					// add the delete and edit buttons
					button_grid = gtk_grid_new();
					gtk_grid_set_column_homogeneous(GTK_GRID(button_grid), TRUE);
					gtk_grid_set_row_homogeneous(GTK_GRID(button_grid), TRUE);
					
					button = gtk_button_new_with_label("Edit selected");
					g_signal_connect(button, "clicked", (GCallback) set_edit, coll);
					gtk_grid_attach(GTK_GRID(button_grid), button, 0, 0, 1, 1);
					
					search_result->edit_button = GTK_BUTTON(button);
					
					button = gtk_button_new_with_label("Delete selected");
					g_signal_connect(button, "clicked", (GCallback) set_edit, coll);
					gtk_grid_attach(GTK_GRID(button_grid), button, 1, 0, 1, 1);
					
					search_result->delete_button = GTK_BUTTON(button);
					
					// add reference to edit collection
					search_result->e_coll = e_coll;
					
					// add it to the box
					gtk_box_pack_start(GTK_BOX(box), button_grid, FALSE, FALSE, 0);
					
					// add to grid
					gtk_grid_attach(e_coll->grid, box, 0, i, 1, 1);					
					
					// expand all
					gtk_tree_view_expand_all(search_result->treeview);
					
					// add to result list
					push_list(&(e_coll->result_head), search_result);
				}
			}

			break;
		
		case EDIT_CONF:
			
			switch (com->request->action)
			{
				case EDIT_SUCCESS:
					msg = "Node has successfully been edited";
					break;
				case EDIT_ERROR:
					msg = "An error occurred while editing";
					break;
				case EDIT_FORBIDDEN:
					msg = "Node is not allowed to be edited";
					break;
				case DELETE_SUCCESS:
					msg = "Node has successfully been deleted";
					break;
				case DELETE_ERROR:
					msg = "An error occurred while deleting";
					break;
				case DELETE_FORBIDDEN:
					msg = "Node is not allowed to be deleted because it is mandatory according to schema";
					break;
			}
			
			// show edit status as dialog
			error_dialog(window, msg);
			
			e_coll = coll;
			
			// enable search button again after editing
			gtk_widget_set_sensitive(GTK_WIDGET(e_coll->search_button), TRUE);
			gtk_widget_set_sensitive(GTK_WIDGET(e_coll->search_entry), TRUE);

			// search again the previous search
			set_previous_search(e_coll);
			
			break;		

		case CREATE_CONF:
			
			switch (com->request->action)
			{
				case ATTRIBUTE:
					type_name = "Attribute";
					break;
				case ELEMENT:
					type_name = "Element";
					break;
				case CONTENT:
					type_name = "Content";
					break;
				case PATTERN_CONTENT:
					type_name = "Pattern";
					break;
				case SIMPLE_TYPE:
					type_name = "Text content";
					break;
				case CHOICE:
					type_name = "Element choice";
					break;
				case ENUMERATION:
					type_name = "Content choice";
					break;
				case BOUNDS_CONTENT:
					type_name = "Value within range";
					break;
				default:
					type_name = NULL;
					break;
			}
			
			if (type_name)
			{							
				// append to overview tree according to depth HIER WEITER checkn, ob noch bugs da sind...
				if (x_coll->last_row && gtk_tree_row_reference_valid(x_coll->last_row))
				{
					int depth_diff = x_coll->last_row_depth - com->cur_depth;
					
					GtkTreePath *last_path = gtk_tree_row_reference_get_path(x_coll->last_row);
					
					gtk_tree_model_get_iter(GTK_TREE_MODEL(x_coll->store), &parent_iter, last_path);
					
					if (depth_diff < 0) // if depth is higher (= child)
					{
						gtk_tree_store_append(x_coll->store, &child_iter, &parent_iter);				
					}
					else if (depth_diff == 0) // if depth is the same (= sibling)
					{						
						GtkTreeIter iter;
						
						if (gtk_tree_model_iter_parent(GTK_TREE_MODEL(x_coll->store), &iter, &parent_iter))
						{
							gtk_tree_store_append(x_coll->store, &child_iter, &iter);
						}
						else
						{
							gtk_tree_store_append(x_coll->store, &child_iter, NULL);
						}			
					}
					else // if depth is lower (= ancestor)
					{
						int i;
						GtkTreeIter old_iter = parent_iter,
												new_iter;
						bool valid = true;
						
						for (i = 0; i <= depth_diff; i++) // go through ancestors
						{
							if (valid = gtk_tree_model_iter_parent(GTK_TREE_MODEL(x_coll->store), &new_iter, &old_iter))
							{
								old_iter = new_iter;
							}
							else
							{
								break;
							}
						}
						
						if (valid)
						{
							gtk_tree_store_append(x_coll->store, &child_iter, &old_iter);
						}
						else
						{
							gtk_tree_store_append(x_coll->store, &child_iter, NULL);
						}
					}
					
					gtk_tree_path_free(last_path);
				}
				else
				{
					gtk_tree_store_append(x_coll->store, &child_iter, NULL);
				}
						
				if (xml_node = com->request->data)
				{
					if (xml_node->type == XML_TEXT_NODE)
					{
						content = xmlNodeGetContent(xml_node);
						
						gtk_tree_store_set(x_coll->store, &child_iter, TYPE_LIST, type_name, NAME_LIST, content, -1); // show content
						
						xmlFree(content);
					}
					else
					{
						gtk_tree_store_set(x_coll->store, &child_iter, TYPE_LIST, type_name, NAME_LIST, xml_node->name, -1); // show name
					}	
				}
				else
				{
					gtk_tree_store_set(x_coll->store, &child_iter, TYPE_LIST, type_name, -1);
				}
				
				gtk_tree_view_expand_all(GTK_TREE_VIEW(x_coll->treeview));
				
				// set newest selected
				selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(x_coll->treeview));
				gtk_tree_selection_select_iter(selection, &child_iter);
				
				// scroll to selected
				path = gtk_tree_model_get_path(GTK_TREE_MODEL(x_coll->store), &child_iter);
				gtk_tree_view_scroll_to_cell(GTK_TREE_VIEW(x_coll->treeview), path, NULL, FALSE, 0, 0);
				
				// save last row and its depth
				x_coll->last_row = gtk_tree_row_reference_new(GTK_TREE_MODEL(x_coll->store), path);
				x_coll->last_row_depth = com->cur_depth;
			}
			
			set_answer(NULL, coll);
			
			break;
			
		case COMPLETE_CONF:
			
			gtk_grid_attach(x_coll->grid, gtk_label_new("Successfully created"), 0, 0, 2, 1);
			
			button = gtk_button_new_with_label("Save to file");
			gtk_grid_attach(x_coll->grid, button, 0, 1, 1, 1);
			gtk_widget_set_name(button, "s");
			g_signal_connect_swapped(button, "clicked", (GCallback) save_clean, coll);
			
			button = gtk_button_new_with_label("Go back");
			gtk_grid_attach(x_coll->grid, button, 1, 1, 1, 1);
			gtk_widget_set_name(button, "g");
			g_signal_connect_swapped(button, "clicked", (GCallback) set_undo, coll);

			break;

		default:
			
			error_dialog(window, "Error: The communication between user and program is faulty, creation aborted.");
			
			save_clean(coll);
			
			break;
	}
	
	// show the updated grid
	gtk_widget_show_all(GTK_WIDGET(x_coll->grid));
}

void fill_result_store(GtkTreeStore	*store, xmlNodePtr node, GtkTreeIter *parent_iter)
{
	GtkTreeIter	child_iter,
							prop_iter;
	
	xmlChar			*text_content;

	xmlNodePtr	child;
	
	xmlAttrPtr	property;
	
	List_Node		*pattern_head = NULL;
	
	int					matches;
	
	if (node->type == XML_ELEMENT_NODE)
	{
		// append new row and add data
		gtk_tree_store_append(store, &child_iter, parent_iter);
		gtk_tree_store_set(store, &child_iter, NAME_TREE, node->name, POINTER_TREE, node, -1);
		
		// also the attributes
		for (property = node->properties; property; property = property->next)
		{
			text_content = xmlNodeListGetString(node->doc, property->children, 1);
			
			gtk_tree_store_append(store, &prop_iter, &child_iter);
			gtk_tree_store_set(store, &prop_iter, NAME_TREE, property->name, CONTENT_TREE, text_content, POINTER_TREE, property, -1);
			
			xmlFree(text_content);
		}		
	}

	if (node->type == XML_TEXT_NODE)
	{
		text_content = xmlNodeGetContent(node);
		
		// check if content is in any form meaningful
		matches = searchInText("[\\S]+", text_content, xmlStrlen(text_content), &pattern_head);
		free_list(&pattern_head, destroyToken);
		
		// append new row and add data if at least 1 none white space char is available
		if (matches > 0)
		{
			gtk_tree_store_append(store, &child_iter, parent_iter);
			gtk_tree_store_set(store, &child_iter, NAME_TREE, node->name, CONTENT_TREE, text_content, POINTER_TREE, node, -1);
		} 

		xmlFree(text_content);
	}
	
	for (child = node->children; child; child = child->next)
	{
		fill_result_store(store, child, &child_iter);
	}
}

xmlNodePtr get_selected_node(GtkTreeView *treeview)
{
	GtkTreeSelection	*selection;

	GList							*selected_list;

	GtkTreeModel			*model;
	
	GtkTreePath				*path;

	xmlNodePtr				selected_node;
	
	GtkTreeIter				iter;
	
	selection = gtk_tree_view_get_selection(treeview);
	
	model = gtk_tree_view_get_model(treeview);
	
	if (!(selected_list = gtk_tree_selection_get_selected_rows(selection, &model)))
	{
		return NULL;
	}
	
	path = selected_list->data;
	
	// get the node pointer from the selected row
	if (!(gtk_tree_model_get_iter(model, &iter, path)))
	{
		error_dialog(window, "Error: Could not get the selected node.");
	}
	else
	{
		gtk_tree_model_get(model, &iter, POINTER_TREE, &selected_node, -1);
	}
	
	g_list_free_full(selected_list, (GDestroyNotify) gtk_tree_path_free);
	
	return selected_node;
}

void set_search(GtkWidget *button, gpointer callback_data)
{
	XML_Edit_Collection	*e_coll;

	// get edit collection
	e_coll = callback_data;
	
	// clear last xpath expression
	free(e_coll->last_xpath_exp);
	
	// save last xpath expression
	e_coll->last_xpath_exp = strdup(gtk_entry_get_text(e_coll->search_entry));
	
	// send data to schema manager
	send_xpath(e_coll->com, strdup(e_coll->last_xpath_exp));
	
	// resume the communication
	handle_communicator(callback_data);	
}

int select_edited(GtkTreeModel *model, GtkTreePath *path, GtkTreeIter *iter, gpointer data)
{
	void 							*node_ptr;

	Search_Result 		*search_result;

	GtkTreeSelection	*selection;
	
	// get search result
	search_result = data;
	
	// get node pointer
	gtk_tree_model_get(model, iter, POINTER_TREE, &node_ptr, -1);
	
	// check if node is edited node
	if (search_result->e_coll->last_selected_node == node_ptr)
	{
		// get selection
		selection = gtk_tree_view_get_selection(search_result->treeview);
		
		// set edited one selected
		gtk_tree_selection_select_iter(selection, iter);
		
		// scroll to selected
		gtk_tree_view_scroll_to_cell(search_result->treeview, path, NULL, FALSE, 0, 0);
		
		return TRUE;
	}
	
	return FALSE;
}

void set_previous_search(gpointer callback_data)
{
	XML_Edit_Collection	*e_coll;
	
	List_Node						*cur;

	Search_Result				*search_result;

	GtkTreeView					*treeview;

	GtkTreeModel				*model;
	
	e_coll = callback_data;
	
	// confirm that confirmation received
	confirm_received(e_coll->com);
	
	// wait for search request from sub thread
	while (!(has_request(e_coll->com)))
	{
	
	}	
	
	// send data to schema manager
	send_xpath(e_coll->com, strdup(e_coll->last_xpath_exp));
	
	// resume the communication
	handle_communicator(callback_data);
	
	// select the nodes modified or parent of deleted
	if (e_coll->last_selected_node)
	{	
		// loop search results
		for (cur = e_coll->result_head; cur; cur = cur->next)
		{
			// get the model
			search_result = cur->data;			
			treeview = search_result->treeview;
			model = gtk_tree_view_get_model(treeview);
			
			gtk_tree_model_foreach(model, select_edited, search_result);
		}
		
		// set back to null
		e_coll->last_selected_node = NULL;
	}
}

void set_edit(GtkWidget *button, gpointer callback_data)
{
	XML_Edit_Collection	*e_coll;
	
	Communicator				*com;
	
	List_Node						*cur;

	Search_Result				*search_result;

	xmlNodePtr					selected_node;
	
	// get edit collection
	e_coll = callback_data;
	
	// get communicator
	com = e_coll->com;
	
	// clear list
	gtk_tree_store_clear(e_coll->store);
	
	for (cur = e_coll->result_head; cur; cur = cur->next)
	{
		search_result = cur->data;
		
		// check if to delete
		if (search_result->delete_button == GTK_BUTTON(button))
		{
			if (!(selected_node = get_selected_node(search_result->treeview)))
			{
				error_dialog(window, "No node selected. Select the node to delete first!");
			}
			else
			{
				// save parent node as last selected node
				e_coll->last_selected_node = selected_node->parent;
				
				request_deletion(com, selected_node);
			}
			
			break;
		}
		
		// check if to edit
		if (search_result->edit_button == GTK_BUTTON(button))
		{
			if (!(selected_node = get_selected_node(search_result->treeview)))
			{
				error_dialog(window, "No node selected. Select the node to edit first!");
			}
			else
			{
				// save node as last selected node
				e_coll->last_selected_node = selected_node;
				
				request_editing(com, selected_node);
				
				// disable search button while editing
				gtk_widget_set_sensitive(GTK_WIDGET(e_coll->search_button), FALSE);
				gtk_widget_set_sensitive(GTK_WIDGET(e_coll->search_entry), FALSE);
			}
			
			break;
		}
	}
	
	// resume the communication
	handle_communicator(callback_data);	
}

void set_answer(GtkWidget *button, gpointer callback_data)
{
	XML_Collection			*x_coll = callback_data;
	
	Communicator				*com = x_coll->com;
	
	GtkWidget						*textarea;

	GtkTextBuffer				*buffer;
	
	GtkTextIter					start, end;
	
	gchar								*string;
	
	switch (com->request->type)
	{
		case CONFIRM_REQ:
		case STRING_REQ:
			
			switch (com->request->action)
			{
				
				case CREATE_NODE:
				case REPLACE_NODE:
				case DEFAULT:
					
					if (EQUALS(gtk_button_get_label(GTK_BUTTON(button)), "Yes")) accept(com);
					else refuse(com);
			
					break;
				
				case STRING:
				case FILE_PATH:
				case PATTERN:
				case DECIMAL:
				case INTEGER:
				case DATE:
				case TIME:
				case DATETIME:
					
					textarea = gtk_grid_get_child_at(x_coll->grid, 0, 1);
					
					buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textarea));
					
					gtk_text_buffer_get_bounds(buffer, &start, &end);
					
					answer(com, strdup(gtk_text_buffer_get_text(buffer, &start, &end, FALSE)));
					
					break;		
			}
			
			break;
			
		case CHOOSE_REQ:
			
			choose(com, atoi(gtk_widget_get_name(GTK_WIDGET(button))));
			
			break;
		
		case CREATE_CONF:
		
			confirm_received(com);
			
			break;
	}
	
	// resume the communication
	handle_communicator(callback_data);
}

void set_undo(gpointer callback_data)
{
	XML_Collection	*x_coll = callback_data;
	
	GtkTreeIter			iter;
	
	// let the user know that undo is proceeding
	gtk_tree_store_append(x_coll->store, &iter, NULL);
	gtk_tree_store_set(x_coll->store, &iter,
				             TYPE_LIST, "Undo",
				             // NAME_CONTENT, "",
					           -1);
	
	request_undo(x_coll->com);
	
	handle_communicator(callback_data);
}

void prompt_file_path(gpointer callback_data)
{
	GtkWidget			*textarea = callback_data;
	
	GtkTextBuffer	*buffer;
	
	gchar					*file_path = NULL;
	
	if (file_path = file_dialog(GTK_FILE_CHOOSER_ACTION_OPEN, "Choose a file", GTK_WINDOW(window), "*", NULL))
	{
		buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textarea));
		
		gtk_text_buffer_set_text(buffer, file_path, -1);
		
		g_free(file_path);
	}
}

void save_clean(gpointer callback_data)
{
	XML_Collection				*x_coll = callback_data;
	
	List_Node							*cur_schema,
												*cur_xml;
	
	Schema_Collection			*s_coll;
	
	XML_Create_Collection	*c_coll = NULL;
	
	XML_Edit_Collection		*e_coll = NULL;
	
	gchar									*xml_file_path = NULL;

	gint									page_num;
	
	if (!(quit(x_coll->com)))
	{
		error_dialog(window, "There was an error terminating the creation process. Cleanup will be faulty.");
	}
	
	// find the collection in the schema
	for (cur_schema = schema_list_head; cur_schema; cur_schema = cur_schema->next)
	{
		s_coll = cur_schema->data;
		
		// look in the create list
		for (cur_xml = s_coll->create_list_head; cur_xml; cur_xml = cur_xml->next)
		{
			c_coll = cur_xml->data;
			
			if (c_coll == callback_data)
			{
				e_coll = NULL;
				goto proceed;
			}
		}
		
		// look in the edit list
		for (cur_xml = s_coll->edit_list_head; cur_xml; cur_xml = cur_xml->next)
		{
			e_coll = cur_xml->data;
			
			if (e_coll == callback_data)
			{
				c_coll = NULL;
				goto proceed;
			}
		}
	}
	
	proceed:
		
		if (c_coll)
		{			
			if (xml_file_path = file_dialog(GTK_FILE_CHOOSER_ACTION_SAVE, "Save new XML file", GTK_WINDOW(window), "*.xml", NULL))
			{
				// save to file if user gives path
				save_to_file(c_coll->com->xml, BAD_CAST xml_file_path, ENC_UTF8, true);
	
				g_free(xml_file_path);
			}
			
			// find the paned
			page_num = gtk_notebook_page_num(GTK_NOTEBOOK(s_coll->notebook), GTK_WIDGET(c_coll->paned));
			
			// remove it
			gtk_notebook_remove_page(GTK_NOTEBOOK(s_coll->notebook), page_num);
			
			// remove the collection
			remove_node_by_data(&(s_coll->create_list_head), c_coll, free_xml_create_collection);
		}
		
		if (e_coll)
		{
			if (xml_file_path = file_dialog(GTK_FILE_CHOOSER_ACTION_SAVE, "Save edited XML file", GTK_WINDOW(window), "*.xml", e_coll->file_path))
			{
				// save to file if user gives path
				save_to_file(e_coll->com->xml, BAD_CAST xml_file_path, ENC_UTF8, true);
	
				free(xml_file_path);
			}
			
			// find the paned			
			page_num = gtk_notebook_page_num(GTK_NOTEBOOK(s_coll->notebook), GTK_WIDGET(e_coll->paned));
			
			// remove it
			gtk_notebook_remove_page(GTK_NOTEBOOK(s_coll->notebook), page_num);
			
			// remove the collection
			remove_node_by_data(&(s_coll->edit_list_head), e_coll, free_xml_edit_collection);									
		}
}

/* COMMUNICATION FUNCTIONS END */

/* CLOSE FUNCTIONS */

void close_schema(void *s_coll_data)
{
	Schema_Collection	*s_coll = s_coll_data;
	
	List_Node					*cur_xml = NULL,
										*cur_schema = NULL;

	GtkWidget					*schema_notebook;

	gint							page_num;
	
	GList							*children,
										*iter;

	const gchar				*label_name = NULL;
	
	gchar							*msg = NULL;						
	
	// get the label name of the schema to be closed
	children = gtk_container_get_children(GTK_CONTAINER(s_coll->tab_label));
	
	for (iter = children; iter != NULL; iter = g_list_next(iter))
	{
		if (GTK_IS_LABEL(GTK_WIDGET(iter->data)))
		{
			label_name = gtk_label_get_text(GTK_LABEL(GTK_WIDGET(iter->data)));
		}
	}
	
	g_list_free(children);
	
	msg = g_strdup_printf("Close schema '%s'? This will interrupt all ongoing XML creations.", label_name);
	
	if (confirm_dialog(window, msg))
	{		
		// end all creations under this schema
		for (cur_xml = s_coll->create_list_head; cur_xml; cur_xml = cur_xml->next)
		{
			save_clean(cur_xml->data);
		}
		
		// end all editings under this schema
		for (cur_xml = s_coll->edit_list_head; cur_xml; cur_xml = cur_xml->next)
		{			
			save_clean(cur_xml->data);
		}
		
		// get current schema from list
		for (cur_schema = schema_list_head; cur_schema; cur_schema = cur_schema->next)
		{
			if (cur_schema->data == s_coll)
			{
				// remove it from the notebook
				schema_notebook = GTK_WIDGET(gtk_builder_get_object(builder, "notebook"));
				
				if ((page_num = gtk_notebook_page_num(GTK_NOTEBOOK(schema_notebook), s_coll->notebook)) >= 0)
				{
					gtk_notebook_remove_page(GTK_NOTEBOOK(schema_notebook), page_num);
				}
				
				// remove schema collection from list
				remove_node(&(schema_list_head), cur_schema, free_schema_collection);
				
				break;
			}
		}
	}
	
	g_free(msg);
	
	// set unsensitive in case schema list is empty
	if (!(schema_list_head)) set_toolbar_sensitive(false);
} 

void close_xml_create(void *x_coll_data)
{
	XML_Collection	*x_coll = x_coll_data;
	
	if (confirm_dialog(window, "Stop creating the XML file?"))
	{
		save_clean(x_coll);
	}
}

void close_xml_edit(void *x_coll_data)
{
	XML_Collection	*x_coll = x_coll_data;
	
	if (confirm_dialog(window, "Stop editing the XML file?"))
	{
		save_clean(x_coll);
	}
}

/* CLOSE FUNCTIONS END */

/* GUI CALLBACKS */

void on_window_main_destroy(void)
{
	List_Node	*cur;
	
	Schema_Collection	*s_coll;
	
	// free schemas and xmls
	for (cur = schema_list_head; cur; cur = cur->next)
	{
		s_coll = cur->data;
		
		close_schema(s_coll);
	}
	
	// check if all schemas are freed
	if (!(schema_list_head))
	{
		// free builder object
		g_object_unref(builder);
		  
		// end application
		gtk_main_quit();		
	}
}

void on_edit_xml_button_clicked(void)
{
	GtkWidget							*schema_notebook,
												*xml_notebook,
												*tab_label,
												*paned,
												*vertical_paned,
												*tree,
												*scrolled_window,
												*grid,
												*search_grid,
												*entry,
												*button;

	GtkTreeStore					*store;
	
	GtkTreeViewColumn			*column;

	Schema_Collection			*s_coll;
	
	XML_Edit_Collection		*e_coll;
	
	char									*xml_file_path;
	
	pthread_t							thread;
	
	if (!(xml_file_path = file_dialog(GTK_FILE_CHOOSER_ACTION_OPEN, "Open XML file", GTK_WINDOW(window), "*.xml", NULL)))
	{
		return;
	}
	
	// get the main schema notebook
	schema_notebook = GTK_WIDGET(gtk_builder_get_object(builder, "notebook"));
	
	// get the current schema tab 
	if (!(xml_notebook = get_current_tab(GTK_NOTEBOOK(schema_notebook))))
	{
		error_dialog(window, "Error: No schema selected or schema tab could not be found!");
		return;
	}
	
	// get the corresponding schema collection
	s_coll = get_current_schema_collection(xml_notebook);
	
	// get the XML collection from an existing XML file
	if (!(e_coll = create_xml_edit_collection(s_coll->schema, xml_file_path)))
	{
		error_dialog(window, "Error: Could not load XML. Check if valid or if you have access permission!");
		free(xml_file_path);
		return;
	}

	// make a new tab label for xml
	tab_label = create_tab_label(get_file_name_only(xml_file_path), close_xml_edit, e_coll);

	// free path as no longer needed
	free(xml_file_path);
	
	// show tab label
	gtk_widget_show_all(tab_label);
	
	// add it to the collection
	e_coll->tab_label = tab_label;
	
	// create the paned
	paned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
	e_coll->paned = GTK_PANED(paned);
	
	// create the list store and tree
	store = gtk_tree_store_new(N_LIST_COLUMNS, G_TYPE_STRING, G_TYPE_STRING); // 2 columns: the node type and the name or content
	tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));
	
	// add treeview to collection
	e_coll->treeview = GTK_TREE_VIEW(tree);
	
	// create the columns
	column = create_column("Node type", TYPE_LIST);
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column);

	column = create_column("Name or content", NAME_LIST);
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column);
	
	// add the list store to the collection
	e_coll->store = store;
	
	// put tree into scrolled window
	scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(scrolled_window), tree);
	
	// add scrolled window to collection
	e_coll->scrolled = GTK_SCROLLED_WINDOW(scrolled_window);
	
	// add it to the paned
	gtk_paned_add1(GTK_PANED(paned), scrolled_window);
	
	// create the vertical paned
	vertical_paned = gtk_paned_new(GTK_ORIENTATION_VERTICAL);
	
	// add it to the collection
	e_coll->vertical_paned = GTK_PANED(vertical_paned);
	
	// create search grid
	search_grid = gtk_grid_new();
	gtk_grid_set_row_homogeneous(GTK_GRID(search_grid), TRUE);
	gtk_grid_set_column_homogeneous(GTK_GRID(search_grid), TRUE);
	
	// add info label	
	gtk_grid_attach(GTK_GRID(search_grid), gtk_label_new("Search nodes via Xpath"), 0, 0, 4, 1);
	
	// add text field
	entry = gtk_entry_new();
	gtk_grid_attach(GTK_GRID(search_grid), entry, 0, 1, 3, 1);
	gtk_widget_grab_focus(entry); // set focused
	
	// add it to collection
	e_coll->search_entry = GTK_ENTRY(entry);		
	
	// add search button
	button = gtk_button_new_with_label("Search");
	gtk_grid_attach(GTK_GRID(search_grid), button, 3, 1, 1, 1);
	gtk_widget_set_name(button, "q");
	g_signal_connect(button, "clicked", (GCallback) set_search, e_coll);
	e_coll->search_button = GTK_BUTTON(button);
	
	// add it to the vertical paned up
	gtk_paned_add1(GTK_PANED(vertical_paned), search_grid);	
	
	// create the grid
	grid = gtk_grid_new();
	gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
	gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
	
	// add grid to the collection
	e_coll->grid = GTK_GRID(grid);
	
	// put grid into scrolled window
	scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(scrolled_window), grid);
	
	// add it to the vertical paned down
	gtk_paned_add2(GTK_PANED(vertical_paned), scrolled_window);
	
	// add it to the paned					
	gtk_paned_add2(GTK_PANED(paned), vertical_paned);	
	
	// add the XML tab to the XML notebook
	gtk_notebook_append_page(GTK_NOTEBOOK(s_coll->notebook), GTK_WIDGET(e_coll->paned), e_coll->tab_label);
	
	// set paned divider position
	gtk_paned_set_position(GTK_PANED(paned), 200);
	
	// show everything
	gtk_widget_show_all(window);
	
	// add to list
	push_list(&(s_coll->edit_list_head), e_coll);
	
	// start the thread for the creation
	start_editing(e_coll->com);
	
	// save "/*" (search everything) as last xpath expression
	e_coll->last_xpath_exp = strdup("/*");
	
	// send search to schema manager
	send_xpath(e_coll->com, strdup(e_coll->last_xpath_exp));
	
	// start the communication
	handle_communicator(e_coll);	
}

void on_create_xml_button_clicked(void)
{
	GtkWidget								*schema_notebook,
													*xml_notebook,
													*tab_label,
													*paned,
													*tree,
													*scrolled_window,
													*grid;
						
	Schema_Collection				*s_coll;
	
	XML_Create_Collection		*c_coll;
	
	GtkTreeStore						*store;
	
	GtkTreeViewColumn				*column;
	
	pthread_t								thread;
	
	// get the main schema notebook
	schema_notebook = GTK_WIDGET(gtk_builder_get_object(builder, "notebook"));
	
	// get the current schema tab 
	if (!(xml_notebook = get_current_tab(GTK_NOTEBOOK(schema_notebook))))
	{
		error_dialog(window, "Error: No schema selected or schema tab could not be found!");
		return;
	}
	
	// get the corresponding schema collection
	s_coll = get_current_schema_collection(xml_notebook);
	
	// create a new xml collection
	c_coll = create_xml_create_collection(s_coll->schema);
	
	// make a new tab label for xml
	tab_label = create_tab_label("New XML File", close_xml_create, c_coll);
	
	// show it
	gtk_widget_show_all(tab_label);
	
	// add it to the collection
	c_coll->tab_label = tab_label;
	
	// create the paned
	paned = gtk_paned_new(GTK_ORIENTATION_HORIZONTAL);
	c_coll->paned = GTK_PANED(paned);
	
	// create the list store and tree
	store = gtk_tree_store_new(N_LIST_COLUMNS, G_TYPE_STRING, G_TYPE_STRING); // 2 columns: the node type and the name or content
	tree = gtk_tree_view_new_with_model(GTK_TREE_MODEL(store));

	// add treeview to collection
	c_coll->treeview = GTK_TREE_VIEW(tree);
	
	// create the columns
	column = create_column("Node type", TYPE_LIST);
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column);

	column = create_column("Name or content", NAME_LIST);
	gtk_tree_view_append_column(GTK_TREE_VIEW(tree), column);
	
	// add the list store to the collection
	c_coll->store = store;
	
	// put tree into scrolled window
	scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(scrolled_window), tree);
	
	// add scrolled window to collection
	c_coll->scrolled = GTK_SCROLLED_WINDOW(scrolled_window);
	
	// add it to the paned
	gtk_paned_add1(GTK_PANED(paned), scrolled_window);		
	
	// create the grid
	grid = gtk_grid_new();
	gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
	gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
	
	// add grid to the collection
	c_coll->grid = GTK_GRID(grid);
	
	// put grid into scrolled window
	scrolled_window = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(scrolled_window), grid);	
	
	// add it to the paned					
	gtk_paned_add2(GTK_PANED(paned), scrolled_window);	
	
	// add the XML tab to the XML notebook
	gtk_notebook_append_page(GTK_NOTEBOOK(s_coll->notebook), GTK_WIDGET(c_coll->paned), c_coll->tab_label);
	
	// set paned divider position
	gtk_paned_set_position(GTK_PANED(paned), 200);
	
	// show everything
	gtk_widget_show_all(window);
	
	// add to list
	push_list(&(s_coll->create_list_head), c_coll);
	
	// start the thread for the creation
	start_creation(c_coll->com, COMMUNICATE);
	
	// start the communication
	handle_communicator(c_coll);
}

void on_open_xsd_button_clicked(void)
{
	char							*xsd_file_path = NULL,
										*xsd_file_name = NULL;
	
	Schema_Collection	*s_coll = NULL;
	
	GtkWidget					*notebook = NULL,
										*tab_label = NULL,
										*xml_notebook = NULL;
	
	// get xsd file chosen by user
	if (!(xsd_file_path = file_dialog(GTK_FILE_CHOOSER_ACTION_OPEN, "Select XSD file", GTK_WINDOW(window), "*.xsd", NULL)))
	{
		return;
	}
	
	if (!(s_coll = create_schema_collection(xsd_file_path)))
	{
		error_dialog(window, "Error: Schema could not be created. Please check if the XSD file is valid or if user has reading permissions!");
		
		free(xsd_file_path);
		return;
	}
	
	xsd_file_name = get_file_name_only(xsd_file_path);
	
	// get the main notebook
	notebook = GTK_WIDGET(gtk_builder_get_object(builder, "notebook"));
	
	// make a new tab label for xsd
	tab_label = create_tab_label(xsd_file_name, close_schema, s_coll);
	
	// show it
	gtk_widget_show_all(tab_label);
	
	// add it to the collection
	s_coll->tab_label = tab_label;
	
	// create new notebook for xmls
	s_coll->notebook = gtk_notebook_new();
	
	// add to notebook
	gtk_notebook_append_page(GTK_NOTEBOOK(notebook), s_coll->notebook, s_coll->tab_label);
	
	// make create and edit possible
	set_toolbar_sensitive(true);
	
	// refresh and show
	gtk_widget_show_all(window);
	
	// add to list
	push_list(&schema_list_head, s_coll);	
		  
  free(xsd_file_path);
}

/* GUI CALLBACKS END */

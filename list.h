#ifndef LIST_H
#define LIST_H

#include <stdlib.h>

/**
 * A generic list node with pointer to the next node.
 */
typedef struct _List_Node {
	void *data;
	struct _List_Node *next;	
} List_Node;

/**
 * Add a node to the beginning of the list.
 * @param head a pointer to the head of the list
 * @param data a pointer to the actual data
 * @return 0 if success, 1 in case of allocation error
 */
int push_list(List_Node **head, void *data);

/**
 * Remove a node from the list.
 * @param head a pointer to the head of the list
 * @param node the list node to be removed
 * qparam free_func a pointer to the function used to free the data
 */
void remove_node(List_Node **head, List_Node *node, void (*free_func)(void *data));

/**
 * Remove a node from the list by comparing the node data.
 * @param head a pointer to the head of the list
 * @param node_data the data of the list node to be removed
 * qparam free_func a pointer to the function used to free the data
 */
void remove_node_by_data(List_Node **head, void *node_data, void (*free_func)(void *data));

/**
 * Free the entire list.
 * @param head a pointer to the head of the list
 * @param free_func a pointer to the function used to free the data
 */
void free_list(List_Node **head, void (*free_func)(void *data));

#endif

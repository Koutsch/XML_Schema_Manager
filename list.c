#include "list.h"

int push_list(List_Node **head, void *data) {
	
	// allocate memory for the list node 
  List_Node *temp = malloc(sizeof(List_Node));
  
  // in case the allocation was successful
  if (temp) {
  	
  	// add the data to the list node
  	temp->data = data;
  	
  	// check if the head node exists
  	if (*head) {
  		
  		// add the head as next node to the new node
  		temp->next = *head;
  	
  	} else {
  		
  		// set the next node to be null
  		temp->next = NULL;
  	}
  	
  	// make the new node the head
  	*head = temp;
  	
  	return 0;
  }
  
  return 1;
}

void remove_node(List_Node **head, List_Node *node, void (*free_func)(void *data))
{
	List_Node *cur = *head, *tmp;
	
	// check first occurrence
	if (*head == node)
	{
		if (free_func) free_func(cur->data);
		
		free(cur);
		
		*head = (*head)->next;
		
		return;
	}
	
	while (cur->next && cur->next != node)
	{
		cur = cur->next;
	}
	
	// it is the node
	if (cur->next)
	{
		tmp = cur->next;
		
		cur->next = cur->next->next;
		
		if (free_func) free_func(tmp->data);
		
		free(tmp);
	}
}

void remove_node_by_data(List_Node **head, void *node_data, void (*free_func)(void *data))
{
	List_Node *cur = *head, *tmp;
	
	// check first occurrence
	if ((*head)->data == node_data)
	{		
		if (free_func) free_func(cur->data);
		
		free(cur);
		
		*head = (*head)->next;
		
		return;
	}
	
	while (cur->next && cur->next->data != node_data)
	{
		cur = cur->next;
	}
	
	// it is the node
	if (cur->next)
	{
		tmp = cur->next;
		
		cur->next = cur->next->next;
		
		if (free_func) free_func(tmp->data);
		
		free(tmp);
	}
}

void free_list(List_Node **head, void (*free_func)(void *data)) {

	List_Node *next, *to_delete;
	
	// assign the head as the node to be deleted
	to_delete = *head;
	
	// check if there is a node to be deleted
	while (to_delete) {
		
		// save the next node temporarily
		next = to_delete->next;
		
		// free the data via custom free function
		if (free_func) free_func(to_delete->data);
		
		// free the node to delete
		free(to_delete);
		
		// assign the next node as the one to be deleted
		to_delete = next;
	}
	
	// set the head to null to indicate that the list is deleted
	*head = NULL;
}

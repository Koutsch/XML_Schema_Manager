/** 
 * @file xml_schema_manager_time.h 
 *
 * @brief The XML Schema Manager TIme contains utilities to represent ISO 8601 date and time as used in XML Schema.
 *
 * @author Jakob Wiedner
 */

#ifndef XML_SCHEMA_MAN_TIME
#define XML_SCHEMA_MAN_TIME

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#ifdef E
	#define ERROR(fmt, ...) fprintf(stderr, "ERROR %s:%d:%s(): " fmt "\n", __FILE__, \
                                __LINE__, __func__, __VA_ARGS__); 
#else
	#define ERROR(fmt, ...)
#endif

typedef struct _Time {
	long long int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
	long long int nth_second;
	int tz_hour;
	int tz_minute;
} Time;

/**
 * Makes the time object loopable.
 * @param time the Time object
 * @param i the offset of the time object part desired
 * @return the time object part desired
 */
static long long int get_time_part(Time time, int i);

/**
 * Check if a string represents an ISO timezone format.
 * @param date_str the string to be checked
 * @return true if it is a timezone representation, false otherwise
 */
static bool is_timezone(char *timezone_str);

/**
 * Check if a string represents an ISO date format.
 * @param date_str the string to be checked
 * @return true if it is a date representation, false otherwise
 */
bool is_date(char *date_str);

/**
 * Check if a string represents an ISO time format.
 * @param date_str the string to be checked
 * @return true if it is a time representation, false otherwise
 */
bool is_time(char *time_str);

/**
 * Check if a string represents an ISO datetime format.
 * @param date_str the string to be checked
 * @return true if it is a datetime representation, false otherwise
 */
bool is_datetime(char *datetime_str);

/**
 * Exctract the nth second from a nth second string and add to the Time object.
 * @param t the Time object to add to
 * @param nth_second_str the string containing the nth second
 * @return the number of chars consumed
 */
static int get_nth_second(Time *t, char *nth_second_str);

/**
 * Exctract the timezone from a timezone string and add to the Time object.
 * @param t the Time object to add to
 * @param timezone_str the string containing the timezone
 */
static void get_timezone(Time *t, char *timezone_str);

/**
 * Exctract the date from a date string.
 * @param date_str the string containing the date
 * @return a Time object representing the date
 */
Time get_date(char *date_str);

/**
 * Exctract the time from a time string.
 * @param date_str the string containing the time
 * @return a Time object representing the time
 */
Time get_time(char *time_str);

/**
 * Exctract the time from a datetime string.
 * @param date_str the string containing the datetime
 * @return a Time object representing the datetime
 */
Time get_datetime(char *datetime_str);

/**
 * Compare two times.
 * @param time1 the first time to compare
 * @param time2 the second time to compare
 * @return 0 if both times are the same, a negative value if the first one is earlier, a positive value if the second one is earlier
 */
int compare_time(Time time1, Time time2);

#endif

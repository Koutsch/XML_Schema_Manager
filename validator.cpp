#include "validator.h"

using namespace std;
using namespace xercesc;

void ParserErrorHandler::reportParseException(const SAXParseException& ex)
{
  string exSystemId = XMLString::transcode(ex.getSystemId());

  char* msg = XMLString::transcode(ex.getMessage());
  string errorStr = "at line ";
  errorStr.append(to_string(ex.getLineNumber())).append(" column ").append(to_string(ex.getColumnNumber())).append(": ").append(msg).append("\n");
  
  XMLString::release(&msg);
  
  if (0 == _xsdSystemId.compare(exSystemId))
  {
  	xsdErrorStr.append(errorStr);
  }
  else if (0 == _xmlSystemId.compare(exSystemId))
  {
  	xmlErrorStr.append(errorStr);
  }	
}

ParserErrorHandler::ParserErrorHandler(string xsdSystemId, string xmlSystemId)
{
	_xsdSystemId = xsdSystemId;
	_xmlSystemId = xmlSystemId;	
}

void ParserErrorHandler::warning(const SAXParseException& ex)
{
	reportParseException(ex);
}

void ParserErrorHandler::error(const SAXParseException& ex)
{
	reportParseException(ex);
}

void ParserErrorHandler::fatalError(const SAXParseException& ex)
{
	reportParseException(ex);
}

void ParserErrorHandler::resetErrors()
{
	xsdErrorStr = "";
	xmlErrorStr = "";
}

string ParserErrorHandler::getXsdErrors()
{
	return xsdErrorStr;
}

string ParserErrorHandler::getXmlErrors()
{
	return xmlErrorStr;
}

void Validator::validate(string xsdFilePath, InputSource &xmlInput)
{			
	resetErrors();
	
	XercesDOMParser domParser;
	if (domParser.loadGrammar(xsdFilePath.c_str(), Grammar::SchemaGrammarType) == NULL)
	{
	  return;
	}
	
	string xmlSystemId = XMLString::transcode(xmlInput.getSystemId());
	ParserErrorHandler parserErrorHandler(xsdFilePath.c_str(), xmlSystemId);
	
	domParser.setErrorHandler(&parserErrorHandler);
	domParser.setValidationScheme(XercesDOMParser::Val_Always);
	domParser.setDoNamespaces(true);
	domParser.setDoSchema(true);
	domParser.setValidationSchemaFullChecking(true);
	domParser.setExternalNoNamespaceSchemaLocation(xsdFilePath.c_str());
	
	domParser.parse(xmlInput);
	
	if(domParser.getErrorCount() != 0)
	{
		xsdErrorStr = parserErrorHandler.getXsdErrors();
		xmlErrorStr = parserErrorHandler.getXmlErrors();
	}
}

void Validator::resetErrors()
{
	xsdErrorStr = "";
	xmlErrorStr = "";
}

Validator::Validator()
{
	resetErrors();
}

char *Validator::validateXSD(const char *xsdFilePath)
{
	string filePath = xsdFilePath;

	XMLPlatformUtils::Initialize();
	{			
		string dummyXml = "<a></a>";	
		MemBufInputSource dummyXml_buf((const XMLByte*) dummyXml.c_str(), dummyXml.size(), "dummyXml");
		validate(filePath, dummyXml_buf);
	}
	XMLPlatformUtils::Terminate();			
	
	if (xsdErrorStr.length() == 0) return NULL;
	
	char* output = (char *) malloc(sizeof(char) * xsdErrorStr.length() + 1);
	copy(xsdErrorStr.begin(), xsdErrorStr.end(), output);
	output[xsdErrorStr.length()] = '\0';
	
	return output;
}

char *Validator::validateXML(const char *xsdFilePath, const char *xmlFilePath)
{
	string _xsdFilePath = xsdFilePath;

	XMLPlatformUtils::Initialize();
	{			
		XMLCh* _xmlFilePath = XMLString::transcode(xmlFilePath);
		LocalFileInputSource localXml_buf(_xmlFilePath);
		XMLString::release(&_xmlFilePath);
		
		validate(_xsdFilePath, localXml_buf);
	}
	XMLPlatformUtils::Terminate();
	
	if (xmlErrorStr.length() == 0) return NULL;
	
	char* output = (char *) malloc(sizeof(char) * xmlErrorStr.length() + 1);
	copy(xmlErrorStr.begin(), xmlErrorStr.end(), output);
	output[xmlErrorStr.length()] = '\0';			
	
	return output;
}

char *validate_xsd(char *xsd_file_path)
{
	Validator validator;
	
	char *err_str = validator.validateXSD(xsd_file_path);
	
	return err_str;
}

char *validate_xml(char *xsd_file_path, char *xml_file_path)
{
	Validator validator;
	
	char *err_str = validator.validateXML(xsd_file_path, xml_file_path);
	
	return err_str;
}

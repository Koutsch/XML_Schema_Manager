CC ?= gcc
CXX ?= g++

OBJ = validator xml_schema_manager occ_finder list xml_schema_manager_time
OBJS = $(addsuffix .o,$(OBJ))

all:
	make compile

compile:
	make main.o template_creator.o $(OBJS)
	make xsm_gui
	make xtc_cmd

xsm_gui: main.o $(OBJS)
	$(CXX) `xml2-config --cflags --libs` `pkg-config --cflags --libs gtk+-3.0` -o xsm_gui main.o $(OBJS) -lpthread -export-dynamic -lpcre  -L/usr/lib -lxerces-c
	@echo "XSM GUI Object created..."

xtc_cmd: template_creator.o $(OBJS)
	$(CXX) `xml2-config --cflags --libs` -o xtc_cmd template_creator.o $(OBJS) -lpthread -export-dynamic -lpcre  -L/usr/lib -lxerces-c
	@echo "XTC CMD Object created..."	

main.o: main.c
	$(CC) -D_REENTRANT -D E -D W `xml2-config --cflags --libs` `pkg-config --cflags --libs gtk+-3.0` -c -o main.o main.c -lpthread -export-dynamic

template_creator.o: template_creator.c
	$(CC) -D_REENTRANT -D E -D W `xml2-config --cflags --libs` -c -o template_creator.o template_creator.c -lpthread -export-dynamic
	
xml_schema_manager.o: xml_schema_manager.c
	$(CC) -D_REENTRANT -D E -D W `xml2-config --cflags --libs` -c -o xml_schema_manager.o xml_schema_manager.c -lpthread -export-dynamic
	
occ_finder.o: occ_finder.c
	$(CC) -c -o occ_finder.o occ_finder.c -lpcre

list.o: list.c
	$(CC) -c -o list.o list.c

xml_schema_manager_time.o: xml_schema_manager_time.c
	$(CC) -c -o xml_schema_manager_time.o xml_schema_manager_time.c
	
validator.o: validator.cpp
	$(CXX) -std=c++0x -c -o validator.o validator.cpp -L/usr/lib -lxerces-c

clean:
	rm -rf main.o template_creator.o $(OBJS) xsm_gui *.dSYM

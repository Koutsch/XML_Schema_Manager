#ifdef __cplusplus

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/ErrorHandler.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <iostream>
#include <string>

class ParserErrorHandler : public xercesc::ErrorHandler
{
	private:
		std::string	xsdErrorStr = "",
								xmlErrorStr = "",
								_xsdSystemId,
								_xmlSystemId;
		
		void reportParseException(const xercesc::SAXParseException& ex);
	
	public:
		ParserErrorHandler(std::string xsdSystemId, std::string xmlSystemId);
		
		void warning(const xercesc::SAXParseException& ex);
		
		void error(const xercesc::SAXParseException& ex);
		
		void fatalError(const xercesc::SAXParseException& ex);
		
		void resetErrors();
		
		std::string getXsdErrors();
		
		std::string getXmlErrors();
};

class Validator
{
	private:
		std::string	xsdErrorStr = "",
								xmlErrorStr = "";
	
		void validate(std::string xsdFilePath, xercesc::InputSource &xmlInput);
		
		void resetErrors();
	
	public:
		Validator();
		
		char *validateXSD(const char *xsdFilePath);
		
		char *validateXML(const char *xsdFilePath, const char *xmlFilePath);
};

#endif

#ifdef __cplusplus

extern "C"
{

#endif

char *validate_xsd(char *xsd_file_path);
char *validate_xml(char *xsd_file_path, char *xml_file_path);

#ifdef __cplusplus

}

#endif


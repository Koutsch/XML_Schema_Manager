/**
 * To compile this file using gcc you can type
 * gcc gcc list.c cconc.c -lpcre FILE_NAME.c
 */

#include <pcre.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "occ_finder.h"

#define SUBSTRVEC_LEN 30

int searchInText(char *regex, char *text, int textLen, List_Node **head) {
	
	// check if regex is valid
	if (regex == NULL || *regex == '\0') {
		fprintf(stderr, "Invalid regex. Search terminated with error code 1\n");	
		return -1;
	}
  
  // the error message string
  const char *pcreErrorStr;
  // the offset of the error
  int pcreErrorOffset;
  // the extra for studying
  pcre_extra *pcreExtra;
  // the index of starting the search (needed for multiple results)
  long int startAt = 0;
  // the pattern search return value
  int pcreExecRet;
  // the array for substring matches
  int subStrVec[SUBSTRVEC_LEN];
  // a pointer to the substring found
  const char *psubStrMatchStr;
  // push to list count (number of elements)
  int pushCount = 0;
  
  // compile regex with UTF8 capabilities
  pcre *compRegex = pcre_compile(regex, PCRE_UCP | PCRE_UTF8, &pcreErrorStr, &pcreErrorOffset, NULL);
  
  // check if the compilation was successful
  if (compRegex == NULL) {
    printf("ERROR: Could not compile '%s': %s Search terminated with error code 2\n", regex, pcreErrorStr);
    return -2;
  }
  
  // optimize the regex by studying it
  pcreExtra = pcre_study(compRegex, 0, &pcreErrorStr);
  
  // check if regex could be studied
  if(pcreErrorStr != NULL) {
    printf("ERROR: Could not study '%s': %s  Search terminated with error code 3\n", regex, pcreErrorStr);
    return -3;
  }
  
  // go through the text and find matches
  do {
  
    // do the pattern search
    pcreExecRet = pcre_exec(compRegex,
                            pcreExtra,
                            text, 
                            textLen,   // length of string
                            startAt,        // Start looking at this point
                            PCRE_NO_UTF8_CHECK, // OPTIONS: utf8 validity is not checked
                            subStrVec,      // the array for substring matches
                            SUBSTRVEC_LEN); // Length of subStrVec
                            
    // Report potential errors in the pcre_exec call
    if (pcreExecRet < 0) { // Something bad happened..
      switch(pcreExecRet) {
      case PCRE_ERROR_NOMATCH:
      	// fprintf(stderr, "String did not match the pattern or end was reached\n");
      	break;
      case PCRE_ERROR_NULL:
      	fprintf(stderr, "PCRE null. Search terminated with error code 4\n");
      	return -4;
      case PCRE_ERROR_BADOPTION:
      	fprintf(stderr, "A bad option was passed. Search terminated with error code 5\n");
      	return -5;
      case PCRE_ERROR_BADMAGIC:
      	fprintf(stderr, "Magic number bad (compiled re corrupt?). Search terminated with error code 6\n");
      	return -6;
      case PCRE_ERROR_UNKNOWN_NODE:
      	fprintf(stderr, "Something kooky in the compiled regex. Search terminated with error code 7\n");
      	return -7;
      case PCRE_ERROR_NOMEMORY:
      	fprintf(stderr, "Ran out of memory. Search terminated with error code 8\n");
      	return -8;
      default:
      	fprintf(stderr, "Unknown error. Search terminated with error code 9\n");
      	return -9;
      } /* end switch */
    } else {
      // Check if there is enough space for the matches
      if(pcreExecRet == 0) {
        printf("Too many substrings were found to fit in subStrVec!\n");
        // Set rc to the max number of substring matches possible.
        pcreExecRet = SUBSTRVEC_LEN / 3;
      }
      
      int i;
      for (i = 0; i < pcreExecRet; i++) {
        
        // create new token
        Token *token = (Token *) malloc(sizeof(Token));
        
        // check if Token was created
        if (token == 0) {
        	fprintf(stderr, "Result cannot be saved. Search terminated with error code 10\n");
        	return -10;
        }
        
        // add the position of the match as new token
        token->begin = subStrVec[i*2];
        token->end = subStrVec[i*2+1];
        
        // add the token to the list
        push_list(head, token);
        pushCount++;
        
        // change startAt to match
        startAt = subStrVec[i*2+1];
        
        // check if result found is length 0
        if (subStrVec[i*2] == subStrVec[i*2+1]) {
        	startAt++;
        }
      }
      
    	// check if startAt is still in range
      if (startAt > textLen) {
      	break;
      }
    }
    
  } while (pcreExecRet > 0); // as long as the end is not reached
  
  // Free up the regular expression.
  pcre_free(compRegex);
      
  // Free up the EXTRA PCRE value (may be NULL at this point)
  if(pcreExtra != NULL) {
    #ifdef PCRE_CONFIG_JIT
      pcre_free_study(pcreExtra);
    #else
      pcre_free(pcreExtra);
    #endif
  }
  
  // return success
  return pushCount;
}

void destroyToken(void *data) {
  Token *token = (Token *) data;
  free(token);	
}

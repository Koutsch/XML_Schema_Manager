#include <gtk/gtk.h>
#include <errno.h>
#include <string.h>

#include "xml_schema_manager.h"

enum
{
	TYPE_LIST = 0,
	NAME_LIST,
	N_LIST_COLUMNS
};

enum
{
	NAME_TREE = 0,
	CONTENT_TREE,
	POINTER_TREE,
	N_TREE_COLUMNS
};

typedef struct _Schema_Collection
{
	Schema_Obj *schema;
	GtkWidget *tab_label;
	GtkWidget *notebook;
	List_Node *create_list_head;
	List_Node *edit_list_head;
} Schema_Collection;

typedef struct _XML_Collection
{
	XML_Obj *xml;
	Communicator *com;
	GtkWidget *tab_label;
	GtkGrid *grid;
	GtkTreeView *treeview;
	GtkTreeStore *store;
	GtkScrolledWindow *scrolled;
	GtkTreeRowReference *last_row;
	int last_row_depth;
	
} XML_Collection;

typedef struct _XML_Create_Collection
{
	XML_Obj *xml;
	Communicator *com;
	GtkWidget *tab_label;
	GtkGrid *grid;
	GtkTreeView *treeview;
	GtkTreeStore *store;
	GtkScrolledWindow *scrolled;
	GtkTreeRowReference *last_row;
	int last_row_depth;
	GtkPaned *paned;
} XML_Create_Collection;

typedef struct _XML_Edit_Collection
{
	XML_Obj *xml;
	Communicator *com;
	GtkWidget *tab_label;
	GtkGrid *grid;
	GtkTreeView *treeview;
	GtkTreeStore *store;
	GtkScrolledWindow *scrolled;
	GtkTreeRowReference *last_row;
	int last_row_depth;
	GtkPaned *paned;
	char *file_path;
	GtkPaned *vertical_paned;
	GtkEntry *search_entry;
	char *last_xpath_exp;
	xmlNodePtr last_selected_node;
	GtkButton *search_button;
	List_Node *result_head;
} XML_Edit_Collection;

typedef struct _Search_Result
{
	GtkTreeView *treeview;
	GtkButton *delete_button;
	GtkButton *edit_button;
	XML_Edit_Collection *e_coll;
} Search_Result;

typedef void (*Close_function)(void *collection);

GtkBuilder *builder;

GtkWidget *window;

List_Node *schema_list_head = NULL;

/* FUNCTIONS */

bool trigger_grid_button(const gchar *value);

gint on_window_main_key_press_event(GtkWidget* widget, GdkEventKey* event, gpointer data);

char *get_file_name_only(char *file_path);

void set_toolbar_sensitive(gboolean sensitive);

void clear_container(GtkContainer *container);

void style_text_area(GtkTextView *textarea);

void error_dialog(GtkWidget *parent, char *msg);

bool confirm_dialog(GtkWidget *parent, char *msg);

char *file_dialog(GtkFileChooserAction action, const gchar *title, GtkWindow *parent, const gchar *filter_pattern, const char *given_file_name);

Schema_Collection *create_schema_collection(char *xsd_file_path);

/**
 * Create a XML create collection for creating a new XML file.
 * @param schema the schema object necessary for creating an empty XML file from schema
 * @return a new XML create collection
 */
XML_Create_Collection *create_xml_create_collection(Schema_Obj *schema);

/**
 * Create a XML edit collection for editing an existing XML file.
 * @param schema the schema object necessary for validating the XML file before creating the collection
 * @param xml_file_path the path to the XML file
 * @return a new XML edit collection
 */
XML_Edit_Collection *create_xml_edit_collection(Schema_Obj *schema, char *xml_file_path);

GtkWidget *create_tab_label(char *title, Close_function close, void *collection);

GtkTreeViewColumn	*create_column(gchar *title, int index);

void free_schema_collection(void *data);

void free_xml_create_collection(void *data);

void free_xml_edit_collection(void *data);

GtkWidget *get_current_tab(GtkNotebook *notebook);

Schema_Collection *get_current_schema_collection(GtkWidget *cur_page);

void handle_communicator(void *coll);

void fill_result_store(GtkTreeStore	*store, xmlNodePtr node, GtkTreeIter *parent_iter);

void set_search(GtkWidget *button, gpointer callback_data);

void set_previous_search(gpointer callback_data);

void set_edit(GtkWidget *button, gpointer callback_data);

void set_answer(GtkWidget *button, gpointer callback_data);

void set_undo(gpointer callback_data);

void prompt_file_path(gpointer callback_data);

void save_clean(gpointer callback_data);

void close_schema(void *s_coll_data);

void close_xml_create(void *x_coll_data);

void close_xml_edit(void *x_coll_data);

/**
 * Function called when window is closed.
 */
void on_window_main_destroy(void);

/**
 * Function called to edit an XML file.
 */
void on_edit_xml_button_clicked(void);

/**
 * Function called to create an XML file.
 */
void on_create_xml_button_clicked(void);

/**
 * Function called to open an XSD file.
 */
void on_open_xsd_button_clicked(void);
